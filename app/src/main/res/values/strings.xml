<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    
    <string name="app_name" translatable="false">Persistence</string>
    <string name="copyright" translatable="false">©2016 Actinarium</string>

    <!-- Common -->
    <string name="save">Save</string>
    <string name="ok">OK</string>
    <string name="cancel">Cancel</string>
    <string name="discard">Discard</string>
    <string name="confirm">Confirm</string>
    <string name="undo">Undo</string>
    <string name="delete">Delete</string>
    <string name="custom">Custom</string>
    <string name="skip">Skip</string>
    <string name="back">Back</string>
    <string name="next">Next</string>
    <string name="done">Done</string>
    <string name="more_options">More options</string>
    <string name="keep_editing">Keep editing</string>
    <string name="accessibility_discard_close">Discard and close</string>

    <!-- Quantity string (e.g. 25 minutes), do not translate -->
    <string name="qty" translatable="false"><xliff:g id="total_goal" example="25">%d</xliff:g> <xliff:g id="units" example="minutes">%s</xliff:g></string>
    <string name="em_dash_prefix" translatable="false">— %s</string>
    <string name="vulgar_fraction" translatable="false">%d/%d</string>

    <string name="today">today</string>
    <string name="yesterday">yesterday</string>
    <string name="tomorrow">tomorrow</string>
    <string name="a_week_ago">a week ago</string>
    <string name="long_ago">long ago</string>

    <plurals name="x_days">
        <item quantity="one">%d day</item>
        <item quantity="other">%d days</item>
    </plurals>

    <!-- todo: refactor these for better i18n -->
    <plurals name="x_days_ago">
        <item quantity="one">@string/yesterday</item>
        <item quantity="other">%d days ago</item>
    </plurals>
    <plurals name="in_x_days">
        <item quantity="one">@string/tomorrow</item>
        <item quantity="other">in %d days</item>
    </plurals>
    <plurals name="x_weeks_ago">
        <item quantity="one">@string/a_week_ago</item>
        <item quantity="other">%d weeks ago</item>
    </plurals>

    <!-- ######################## HOME SCREEN & SET PROGRESS FLOW ######################## -->

    <string name="title_today">Today</string>
    <string name="action_add_challenge">Add challenge</string>
    <string name="action_edit_challenge">Edit challenge</string>
    <string name="action_support_developer">Support developer</string>
    <string name="action_settings">Settings</string>
    <string name="action_help_about">Help &amp; about</string>

    <!-- Challenges header -->
    <string name="goals_met">goals met</string>
    <string name="completed_today">completed</string>
    <string name="start_quote">Don’t wait for the ‘perfect’&#160;time. It&#160;will never&#160;come. Start&#160;today.</string>

    <!-- Welcome card -->
    <string name="get_started_title">Get started</string>
    <string name="get_started_message_1">Add the activities you want to turn into habits. In Persistence, these are called <b>challenges.</b></string>
    <string name="card_add_a_challenge">Add a challenge</string>

    <!-- No challenges card -->
    <string name="no_challenges_title">It’s quiet in here</string>
    <string name="no_challenges_message">You have no active challenges. Why not take up something new?</string>

    <!-- First challenge wizard dialog -->
    <string name="dialog_run_wizard_title">Help creating first challenge?</string>
    <string name="dialog_run_wizard_message">Seems like you’re new. Start the wizard to guide you through creating your first challenge?</string>
    <string name="dialog_run_wizard_yes">Start the wizard</string>
    <string name="dialog_run_wizard_no">No, I can manage it</string>

    <!-- Challenge cards -->
    <string name="start_now">Start now</string>
    <string name="add_progress">Add progress</string>
    <string name="give_up">Give up</string>

    <!-- Challenge statuses -->
    <string name="status_just_started">Just started</string>
    <string name="status_great_start">Great start!</string>
    <string name="status_on_track">On track</string>
    <string name="status_back_on_track">Back on track</string>
    <string name="status_in_the_zone">In the zone</string>
    <string name="status_hiccups">Hiccups</string>
    <string name="status_not_good">Not looking good…</string>
    <string name="status_critical">Going down in flames</string>


    <!-- Add progress dialog -->
    <string name="progress_for_day">Progress for <xliff:g id="day" example="Wed, Jul 22">%s</xliff:g></string>
    <!-- Next two lines are like "Goal: 25 minutes" and "Goal: 25 minutes (10 + 15 debt)" -->
    <string name="goal_no_debt">Goal: <xliff:g id="total_goal" example="25">%d</xliff:g> <xliff:g id="units" example="minutes">%s</xliff:g></string>
    <string name="goal_with_debt">Goal: <xliff:g id="total_goal" example="25">%d</xliff:g> <xliff:g id="units" example="minutes">%s</xliff:g> (<xliff:g id="daily_goal" example="10">%d</xliff:g> + <xliff:g id="debt" example="15">%d</xliff:g> debt)</string>
    <string name="reserve_locked">Use reserve (locked)</string>
    <string name="reserve_available">Use reserve (<xliff:g id="reserve_count" example="10">%d</xliff:g> available)</string>
    <!-- Hint should look like this: "To unlock reserve, exceed your goals by 25 more minutes in total.", followed by a link "Read more" -->
    <string name="reserve_locked_hint">To unlock reserve, exceed your goals by <xliff:g id="total_goal" example="25">%d</xliff:g> more <xliff:g id="units" example="minutes">%s</xliff:g> in total.
        <![CDATA[<a href="#read-more">Read more</a>]]></string>


    <!-- Set progress screen / Progress labels -->
    <string name="set_progress">Set progress</string>
    <string name="qty_completed"><xliff:g id="units" example="minutes">%s</xliff:g> completed</string>
    <string name="qty_compensated"><xliff:g id="units" example="minutes">%s</xliff:g> compensated</string>
    <string name="qty_down"><xliff:g id="units" example="minutes">%s</xliff:g> down</string>
    <string name="qty_to_go"><xliff:g id="units" example="minutes">%s</xliff:g> to go</string>
    <string name="qty_debt"><xliff:g id="units" example="minutes">%s</xliff:g> in debt</string>

    <string name="no_debt_status">Good job!</string>
    <string name="debt_status">growing <xliff:g id="multiplier" example="x1.5">%s</xliff:g> daily</string>
    <string name="debt_hiatus_status">not growing</string>

    <string name="qty_reserve">in reserve</string>
    <string name="reserve_collected_status">+<xliff:g id="num_collected" example="1">%s</xliff:g> today</string>
    <string name="reserve_used_status">−<xliff:g id="num_used" example="1">%s</xliff:g> today</string>

    <string name="available_reserve"><xliff:g id="reserve_balance" example="25">%d</xliff:g> <xliff:g id="units" example="minutes">%s</xliff:g> available</string>
    <string name="add_note">Add note</string>


    <!-- ######################## CREATE / EDIT / START CHALLENGE FLOW ######################## -->

    <string name="new_challenge">New challenge</string>
    <string name="edit_challenge">Edit challenge</string>
    <string name="new_challenge_name_prompt">What do you want to do?</string>
    <string-array name="new_challenge_suggestions">
        <item>Read more books</item>
        <item>Go outside every day</item>
        <item>Practice drawing</item>
        <item>Work out daily</item>
        <item>Practice guitar</item>
        <item>Learn a language</item>
        <item>Eat more fruit</item>
    </string-array>
    <string name="new_challenge_description_prompt">Why is this important for you?</string>
    <!-- todo: account for localization, write explanation -->
    <string name="measure_progress_in">Measure progress in</string>
    <string name="set_daily_goals">Set daily goals</string>
    <string name="accessibility_delete_rule">Delete rule</string>
    <string name="add_rule">Add rule</string>
    <string name="edit_special_rule">Edit rule</string>
    <string name="applies_on">Applies on</string>
    <string name="every_week">every week</string>
    <string name="between">between</string>
    <string name="and">and</string>
    <string name="set_up_reminders">Set up reminders</string>
    <string name="add_reminder_first">Tap here to add a reminder</string>
    <string name="add_reminder">Add another reminder</string>
    <string-array name="reminder_types_long">
        <item>Always remind</item>
        <item>Remind if goal is not met yet</item>
        <item>Remind if no progress at all</item>
    </string-array>
    <string-array name="reminder_types_short">
        <item>always</item>
        <item>if goal not met</item>
        <item>if no progress</item>
    </string-array>
    <string name="starting">Starting</string>
    <string-array name="starting_when">
        <item>right now</item>
        <item>later</item>
    </string-array>
    <string name="reminder_already_set">Reminder with such time already exists</string>
    <string name="invalid_title">Provide challenge title</string>
    <string name="invalid_rule">Goals can’t be zero</string>
    <string name="already_saving">Saving challenge already…</string>


    <!-- Add custom unit dialog -->
    <string name="add_custom_unit">Add custom unit</string>
    <!-- todo: account for localization -->
    <string name="custom_unit_hint">Plural, e.g. miles</string>

    <string-array name="challenge_default_units">
        <item>minutes</item>
        <item>meters</item>
        <item>pages</item>
        <item>pcs</item>
    </string-array>


    <!-- Rule conditions and pretty print building blocks. Must be lowercase -->
    <string name="rule_every_day">every day</string>
    <string name="rule_every_other_day">every other day</string>
    <string name="rule_on">on <xliff:g id="day_expr_*" example="Sat and Sun">%s</xliff:g></string>
    <string name="rule_every">every <xliff:g id="day_expr_*" example="Sat and Sun">%s</xliff:g></string>
    <string name="rule_from_to">from <xliff:g id="time_from" example="Jan 25">%s</xliff:g> to <xliff:g id="time_to" example="Jan 26">%s</xliff:g></string>
    <string name="rule_between_and">between <xliff:g id="time_from" example="Jan 25">%s</xliff:g> and <xliff:g id="time_to" example="Jan 26">%s</xliff:g></string>

    <string name="day_expr_two"><xliff:g id="weekday_group_1" example="Mon">%s</xliff:g> and <xliff:g id="weekday_group_2" example="Wed–Fri">%s</xliff:g></string>
    <string name="day_expr_three"><xliff:g id="weekday_group_1" example="Mon">%s</xliff:g>, <xliff:g id="weekday_group_2" example="Wed">%s</xliff:g>, and <xliff:g id="weekday_group_3" example="Fri">%s</xliff:g></string>
    <string name="day_expr_four"><xliff:g id="weekday_group_1" example="Mon">%s</xliff:g>, <xliff:g id="weekday_group_2" example="Tue">%s</xliff:g>, <xliff:g id="weekday_group_3" example="Thu">%s</xliff:g>, and <xliff:g id="weekday_group_4" example="Fri">%s</xliff:g></string>


    <!-- Incomplete form alert dialog -->
    <string name="dialog_incomplete_challenge_title_no_reason">Save with no reason?</string>
    <string name="dialog_incomplete_challenge_title_no_reminders">Save without reminders?</string>
    <string name="dialog_incomplete_challenge_title_no_reason_and_reminders">Save without reason or reminders?</string>
    <string name="dialog_incomplete_challenge_message_no_reason">Thinking why this challenge is important can greatly inspire you for success.</string>
    <string name="dialog_incomplete_challenge_message_no_reminders">Set at least one reminder so that you don’t forget to log your daily progress.</string>
    <string name="dialog_incomplete_challenge_message_no_reason_and_reminders_joiner" translatable="false"><xliff:g id="message_reason_1" example="Describing why this challenge is...">%s</xliff:g>\n\n<xliff:g id="message_reason_2" example="Set at least one reminder so that...">%s</xliff:g></string>


    <!-- Discard creating challenge dialog -->
    <string name="dialog_discard_challenge_message">Discard this challenge?</string>
    <string name="dialog_discard_changes_message">Discard changes?</string>
    <string name="empty_challenge_discarded">Empty challenge discarded</string>


    <!-- Start challenge now dialog -->
    <string name="dialog_start_challenge_title">Start the challenge?</string>
    <string name="dialog_start_challenge_message">This will make the units (<xliff:g id="units" example="minutes">%s</xliff:g>) permanent
        and the goals will be locked for 7 days.</string>


    <!-- ######################## VIEW CHALLENGE DETAILS FLOW ######################## -->

    <string name="start_challenge">Start challenge</string>
    <string name="start_over">Start over</string>
    <!-- Delete - for those challenges that weren't started yet; Stop - for active ones -->
    <string name="action_delete_challenge">Delete challenge</string>
    <string name="action_stop_challenge">Stop challenge</string>

    <string name="since_the_start">Since the start</string>
    <string name="qty_expected">Aimed to do</string>
    <string name="qty_actual">Actually done</string>

    <string name="goals">Goals</string>
    <string name="reminders">Reminders</string>
    <string name="no_reminders">No reminders</string>

    <string name="added_when">Added <xliff:g id="when" example="3 days ago">%s</xliff:g></string>
    <string name="added_when_long">Added on <xliff:g id="day" example="Jan 25">%s</xliff:g> (<xliff:g id="when" example="3 days ago">%s</xliff:g>)</string>
    <string name="stopped_on_lasted">Stopped on <xliff:g id="day" example="Jan 25">%s</xliff:g> · Lasted <xliff:g id="how_long" example="3 days">%s</xliff:g></string>


    <!-- Delete challenge dialog -->
    <string name="dialog_delete_challenge_title">Delete the challenge?</string>
    <string name="dialog_delete_challenge_message">You’re about to delete this challenge without even starting. This operation cannot be undone.</string>

    <!-- Challenge history screen -->
    <string name="challenge_history">Challenge history</string>
    <string name="sort_date_asc">Oldest to newest</string>
    <string name="sort_date_desc">Newest to oldest</string>


    <!-- ######################## ONBOARDING AND CHALLENGE WIZARD FLOW ######################## -->

    <!-- Welcome page -->
    <string name="ob_welcome">Welcome!</string>
    <string name="ob_welcome_text_1">Thank you for installing Persistence.
        \nWe hope it helps you become more productive in the things you want to do.</string>
    <string name="ob_welcome_text_2">If you’re new to the Persistence technique, read this introduction to learn how it works.</string>

    <!-- What is Persistence page -->
    <string name="ob_what_is_persistence">What is Persistence?</string>
    <string name="ob_persistence_quote">Raw&#160;persistence may&#160;be the&#160;only&#160;option other&#160;than giving&#160;up&#160;entirely.</string>
    <string name="ob_persistence_quote_attr">— Antichamber</string>
    <string name="ob_what_is_persistence_text_1">Persistence is a simple self-motivation technique for getting things done.
        It takes an activity you’re struggling with and helps you turn it into a habit.</string>
    <string name="ob_what_is_persistence_text_2">The technique is explained in 5 basic steps:</string>
    <string-array name="ob_persistence_rules">
        <item>Pick an activity you want to do <b>every day.</b></item>
        <item>Decide on the <b>units</b> to measure progress in. These can be anything, the most universal being time spent in <i>minutes</i>.</item>
        <item>Set <b>daily goals.</b> You may have different goals on different days, none can be 0. You can’t adjust them more than once in 7 days.</item>
        <item>If you don’t meet the goal, the difference goes to <b>debt</b>, which is then multiplied <b>1.5x</b> and added to the goal on the next day.</item>
        <item>But exceed the goal, and the <b>half</b> of excess will go to <b>reserve</b>, which you can use to make up for underachievement later.</item>
    </string-array>
    <string name="ob_what_is_persistence_text_3">Obviously, this will work only for activities that:
    </string>
    <string-array name="ob_persistence_challenge_reqs">
        <item>require or imply <b>everyday commitment,</b></item>
        <item>whose progress is <b>measurable</b> and can be presented with a number, e.g. <i>5&#160;miles</i>, <i>30&#160;minutes</i> etc.,</item>
        <item>and the bigger number, the better.</item>
    </string-array>
    <string name="ob_what_is_persistence_text_4">Examples:</string>
    <string-array name="ob_persistence_challenge_examples">
        <item><b>Write a book — good:</b> you can devote some time to it every day, and measure the progress in sentences written or minutes spent.</item>
        <item><b>Go to gym twice a week — bad:</b> it’s not an everyday activity.</item>
        <item><b>Take out trash — bad:</b> simple yes/no activities don’t qualify. If you skip a day, taking out trash 3 times next day won’t make things better.</item>
        <item><b>Floss twice a day — bad:</b> more isn’t necessarily better here. Skipping a week and then flossing 104 times will neither do you good nor teach you a lesson.</item>
        <item>But you can substitute those with something that fits, such as <b>Be&#160;active</b>, <b>Do&#160;chores</b>, or <b>Make&#160;time for&#160;health</b>.</item>
    </string-array>


    <string name="ob_analytics_title">Your feedback matters</string>
    <string name="ob_analytics_text_1">Persistence is a new and experimental technique. To know if it’s any good, we need your feedback.</string>
    <string name="ob_analytics_text_2">You can allow Persistence to send us some <b>anonymous</b> usage data so that we know how to improve the experience for you. It’s safe, easy, and very helpful. By the way, most apps on your device do this already.</string>
    <string name="ob_analytics_text_3">For the details see <a href="#privacy">Privacy Policy</a>. Should you choose to opt out, please drop us a line in our <a href="https://plus.google.com/u/0/communities/100266323983247554451">Google+ community</a>.</string>

    <!-- Notion of a day page -->
    <string name="ob_calling_it_a_day">Calling it a day</string>
    <string name="ob_new_day_start_text_1">Here’s one thing you should be aware of.</string>
    <string name="ob_new_day_start_text_2">In Persistence you can adjust the hour that separates today from tomorrow.
        Select the time in the morning when you’re usually asleep, and Persistence will treat your past-midnight work as the previous day — perfect for night owls!</string>
    <string name="ob_new_day_start_text_3">This setting applies to all challenges.</string>

    <!-- Your first challenge page -->
    <string name="ob_first_challenge">Your first challenge</string>
    <string name="ob_first_challenge_text_1">OK, let’s create your first challenge.
        For this one, pick something simple (or even dummy) so that you get the grasp of things.
        Don’t worry, you can edit or delete this challenge later.</string>
    <string name="ob_first_challenge_title_question">What will your first challenge be?</string>
    <string-array name="ob_first_challenge_suggestions">
        <item>Read more books</item>
        <item>Or go outside every day?</item>
        <item>Practice drawing?</item>
        <item>Or work out daily?</item>
        <item>Will it be playing guitar?</item>
        <item>Or learning a language?</item>
        <item>Meditation is an option</item>
        <item>So is eating more fruit</item>
        <item>A project so awesome,</item>
        <item>everyone would love it!</item>
        <item>I’m out of ideas</item>
        <item>Now it’s your turn</item>
    </string-array>
    <string name="ob_first_challenge_reason_text">When this is decided, think carefully why this challenge is important for you, and why you simply <i>must</i> push forward.
        Although this is optional, thinking of <i>the why</i> can greatly inspire you for success.</string>
    <string name="ob_first_challenge_reason_hint">I need this to become a better me</string>
    <string name="ob_first_challenge_advice">While you can have as many challenges as you wish, it’s a good idea to roll with just one for the first few days, to see how it works for you.</string>

    <string name="ob_commitment">Setting a goal</string>
    <string name="ob_commitment_text_1">Everything in Persistence depends on how you set your goals.</string>
    <string name="ob_commitment_text_2">Set different goals for different days by adding <b>rules.</b>
        Rules are added on the top and override the ones below. Then on each day the first rule to match is applied.</string>
    <string name="ob_commitment_text_3">For the first week we recommend the equivalent of 25 min on workdays and 60 min on weekends, then adjust if required.</string>

    <string name="ob_reminders">Reminders</string>
    <!-- For copy, @string/dialog_incomplete_challenge_message_no_reminders is reused todo: rename identifier -->

    <string name="ob_starting">Start the challenge</string>
    <!-- %s is a placeholder for another string resource @string/dialog_start_challenge_message -->
    <string name="ob_starting_text_1">You can start the challenge right away. <xliff:g id="dialog_start_challenge_message" example="This will make the units…">%s</xliff:g> Choose it if you’re determined to meet your goal today already.</string>
    <string name="ob_starting_text_2">Or you can choose to start it later and freely edit it meanwhile.</string>


    <string name="ob_lets_do_this">Let’s do this!</string>


    <!-- ######################## STOP CHALLENGE FLOW ######################## -->

    <string name="stop_challenge">Stop challenge</string>

    <string name="stop_challenge_message_1">You’re about to stop the <![CDATA[<font face="sans-serif-medium">]]><xliff:g id="challenge_title" example="Practice drawing">%s</xliff:g><![CDATA[</font>]]> challenge.</string>

    <string name="stop_challenge_message_2b">You started it just <xliff:g id="when" example="2 days ago">%s</xliff:g></string>
    <string name="stop_challenge_message_2b1">but didn’t make any progress.</string>
    <string name="stop_challenge_message_2b2">and already accomplished something.</string>
    <string name="stop_challenge_message_2b3">and you seem doing well.</string>

    <string name="stop_challenge_message_2c">You started it on <xliff:g id="when" example="Jan 31">%s</xliff:g>
        and already persisted for <xliff:g id="how_long" example="25 days">%s</xliff:g>.</string>
    <string name="stop_challenge_message_2c1">Since the start you aimed to complete <xliff:g id="goal" example="40">%d</xliff:g> <xliff:g id="units" example="pages">%s</xliff:g>
        but didn’t make any progress</string>
    <string name="stop_challenge_message_2c2">Since the start you aimed to complete <xliff:g id="goal" example="40">%d</xliff:g> <xliff:g id="units" example="pages">%s</xliff:g>
        but did only <xliff:g id="actual" example="20">%d</xliff:g></string>
    <string name="stop_challenge_message_2c3">Since the start you logged <xliff:g id="actual" example="80">%d</xliff:g> <xliff:g id="units" example="pages">%s</xliff:g>,
        which is about the same as expected.</string>
    <string name="stop_challenge_message_2c4">Since the start you logged <xliff:g id="actual" example="80">%d</xliff:g> <xliff:g id="units" example="pages">%s</xliff:g>,
        exceeding your goals by <xliff:g id="number" example="23">%d</xliff:g> percent!</string>

    <string name="stop_challenge_message_3_00">Besides, you’ll be able to edit the goals <xliff:g id="when" example="tomorrow">%s</xliff:g>.</string>
    <string name="stop_challenge_message_3_01">You can still edit the goals for this challenge.</string>
    <string name="stop_challenge_message_3_10">You can still use the reserve, and the goals will be unlocked <xliff:g id="when" example="in 3 days">%s</xliff:g>.</string>
    <string name="stop_challenge_message_3_11">You can still use the reserve or edit the goals.</string>

    <string name="stop_challenge_message_4">Before you stop it, remember why you started.</string>
    <string name="stop_challenge_message_4opt"><![CDATA[<font face="sans-serif-medium">]]><xliff:g id="reason" example="I want to become an artist">%s</xliff:g><![CDATA[</font>]]> — that’s what you said.</string>

    <string name="stop_challenge_reason_title">Reason to quit</string>
    <string name="stop_challenge_reason_message">Before the challenge is stopped, tell us why you’re abandoning it.</string>
    <string-array name="stop_challenge_reasons">
        <item>Other</item>
        <item>It successfully became my habit</item>
        <item>No time for this at the moment</item>
        <item>It’s not important anymore</item>
        <item>I thought it was important — it’s not</item>
        <item>I overestimated myself and set the&#160;goals too&#160;high</item>
        <item>I was lazy and didn’t do my best</item>
        <item>I already have no problem doing this</item>
    </string-array>
    <string name="other_reason_hint">Please specify</string>
    <string name="reason_invalid">Select a reason</string>

    <string name="challenge_stopped_title">Done</string>
    <string name="challenge_stopped_message_1">Challenge <![CDATA[<font face="sans-serif-medium">]]><xliff:g id="challenge_title" example="Practice drawing">%s</xliff:g><![CDATA[</font>]]> is now stopped.</string>
    <string name="challenge_stopped_message_2a">Good luck with your next challenges!</string>
    <string name="challenge_stopped_message_2b">Should you change your mind, you can still resume it until tomorrow, or start over at any time.</string>

    <string name="challenge_stopped_donate_success_1st_time">Enjoyed Persistence? Please consider <a href="#">supporting the developer</a>.</string>
    <string name="challenge_stopped_donate_fail_1st_time">Want to punish yourself? How about <a href="#">sending some money the developer’s way</a>?</string>
    <string name="challenge_stopped_donate_fail_returning">Want to punish yourself for this failure? How about <a href="#">making another donation</a>?</string>


    <!-- ######################## EVERYTHING ELSE ######################## -->

    <!-- Settings -->
    <string name="new_day_starts_at">New day starts at</string>
    <string name="reset_custom_units">Remove custom units</string>
    <string name="reset_custom_units_description">Reset the list of units on create challenge page. Existing challenges won’t be affected.</string>
    <string name="reset_custom_units_snackbar">List of units reset</string>
    <string name="allow_analytics">Help make Persistence better</string>
    <string name="allow_analytics_description">Allow sending anonymous usage data to the developer. See Privacy Policy for the details.</string>

    <!-- Support developer -->

    <!-- Help & about-->
    <string name="action_view_on_play_store">View in Google Play Store</string>
    <string name="action_run_onboarding_again">Rerun welcome</string>
    <string name="action_run_wizard_again">Rerun wizard</string>
    <string name="action_privacy_policy">Privacy Policy</string>
    <string name="action_open_source_licenses">Open source licenses</string>

    <string name="version_info">Version <xliff:g id="version_number" example="1.0.0">%s</xliff:g></string>

    <string name="getting_help_title">Getting help</string>
    <string name="getting_help_text_1">All information about Persistence app and technique was presented in the welcome screen and the first challenge wizard. Did you skip it, or need a refresher?</string>
    <string name="getting_help_text_2">Help topics will be added in the future. For now if you have questions, or wish to discuss the app or the technique, or share feedback, feel free to join
        <a href="https://plus.google.com/u/0/communities/100266323983247554451">the official Google+ community</a>.</string>

    <string name="open_source_title">Open source notice</string>
    <string name="open_source_text">This application directly uses and/or derives from these third party open source dependencies:</string>
    <string-array name="oos_libraries_list" translatable="false">
        <item><b>Android Open Source Project</b> — Apache 2.0 License</item>
        <item><b>Android Support Library</b> — Apache 2.0 License</item>
        <item><b>Stetho</b> — BSD License (3-clause) with patent grant</item>
        <item><b>Material icons (Google)</b> — Creative Commons Attribution 4.0 International</item>
        <item><b>materialdesignicons.com Community icons</b> — SIL Open Font License 1.1</item>
    </string-array>

    <!-- Notifications -->
    <string name="single_subtitle">Ready to add some progress?</string>
    <plurals name="stacked_header">
        <item quantity="one"><xliff:g id="number" example="1">%d</xliff:g> challenge needs your attention</item>
        <item quantity="other"><xliff:g id="number" example="5">%d</xliff:g> challenges need your attention</item>
    </plurals>
    <!-- Overflow text, displayed when notification is expanded after the first 5 items, e.g. "+2 more" -->
    <string name="stacked_overflow">+<xliff:g id="number_of_overflow_reminders" example="2">%d</xliff:g> more</string>
    <string name="snooze_30_min">Remind in 30 minutes</string>

    <!-- Challenge rule easter eggs -->
    <string-array name="challenge_easter_eggs">
        <item>That’s many rules you’ve got there</item>
        <item>Seriously, stop</item>
        <item>You do realize you’d better be doing your challenge already than wasting your time on this, right?</item>
        <item translatable="false">…</item>
        <item>100 rules! Such commitment! I’m not even mad, this is amazing.</item>
        <item>You can stop now. No more messages ahead, promise.</item>
    </string-array>

</resources>
