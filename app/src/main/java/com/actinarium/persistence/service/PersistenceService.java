package com.actinarium.persistence.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.actinarium.persistence.common.ApplicationContext;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.DataUtils;
import com.actinarium.persistence.data.PersistenceContract;
import com.actinarium.persistence.data.PersistenceDbHelper;
import com.actinarium.persistence.data.PersistenceDbOps;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.dto.ScheduledReminder;
import com.actinarium.persistence.ui.settings.PrefUtils;

import java.util.UUID;

/**
 * Intent service class that handles asynchronous data manipulation
 */
public class PersistenceService extends IntentService {

    private static final String TAG = "PersistenceService";

    private static final String ACTION_CREATE_CHALLENGE = "com.actinarium.persistence.intent.action.CREATE_CHALLENGE";
    private static final String ACTION_UPDATE_CHALLENGE = "com.actinarium.persistence.intent.action.UPDATE_CHALLENGE";
    private static final String ACTION_START_CHALLENGE = "com.actinarium.persistence.intent.action.START_CHALLENGE";
    private static final String ACTION_STOP_CHALLENGE = "com.actinarium.persistence.intent.action.STOP_CHALLENGE";
    private static final String ACTION_DELETE_CHALLENGE = "com.actinarium.persistence.intent.action.DELETE_CHALLENGE";
    private static final String ACTION_UPDATE_PROGRESS = "com.actinarium.persistence.intent.action.UPDATE_PROGRESS";

    private static final String EXTRA_CHALLENGE = "com.actinarium.persistence.intent.extra.CHALLENGE";
    private static final String EXTRA_CHALLENGE_ID = "com.actinarium.persistence.intent.extra.CHALLENGE_ID";
    private static final String EXTRA_PROGRESS = "com.actinarium.persistence.intent.extra.PROGRESS";
    private static final String EXTRA_SEND_EVENT = "com.actinarium.persistence.intent.extra.SEND_EVENT";

    /* Public API for invoking the service */

    /**
     * Post new challenge to the DB. It's your responsibility to set all required fields and flags, <b>except UUID,</b>
     * which will be assigned by this service, ensuring its uniqueness. If {@link Challenge#isActive()}, will also start
     * the challenge right away. If challenge is started, this method will also register reminders and ensure integrity
     * since {@link Challenge#startedOn} (not necessarily today). Uses {@link Challenge#toContentValuesOnCreate()} for
     * building the query.
     *
     * @param context   Context
     * @param challenge Challenge to create. Fields <code>id</code> and <code>uuid</code> are ignored and subsequently
     *                  assigned by this service call
     * @param sendEvent Set this to true to communicate operation status via {@link ApplicationContext}
     */
    public static void createChallenge(Context context, Challenge challenge, boolean sendEvent) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_CREATE_CHALLENGE);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        intent.putExtra(EXTRA_SEND_EVENT, sendEvent);
        context.startService(intent);
    }

    /**
     * Update challenge in DB. The challenge <b>must</b> have <code>id</code> field properly set. Uses {@link
     * Challenge#toContentValuesOnUpdate()} for building the query. If {@link Challenge#getRules()} is not
     * <code>null</code>, all progress entries starting from {@link Challenge#rulesSetOn} will be updated with new
     * rules. If {@link Challenge#getReminders()} is not <code>null</code>, scheduled reminders will be updated as well.
     * It's your responsibility to set all required fields and flags.
     *
     * @param context   Context
     * @param challenge Challenge to update
     */
    public static void updateChallenge(Context context, Challenge challenge) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_UPDATE_CHALLENGE);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startService(intent);
    }

    public static void startChallenge(Context context, Challenge challenge) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_START_CHALLENGE);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startService(intent);
    }

    public static void stopChallenge(Context context, Challenge challenge) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_STOP_CHALLENGE);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startService(intent);
    }

    public static void deleteChallenge(Context context, long challengeId) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_DELETE_CHALLENGE);
        intent.putExtra(EXTRA_CHALLENGE_ID, challengeId);
        context.startService(intent);
    }

    /**
     * Save progress to database
     *
     * @param context
     * @param progress Progress to insert into DB (possibly replacing existing progress record)
     */
    public static void updateProgress(Context context, Progress progress) {
        Intent intent = new Intent(context, PersistenceService.class);
        intent.setAction(ACTION_UPDATE_PROGRESS);
        intent.putExtra(EXTRA_PROGRESS, progress);
        context.startService(intent);
    }


    public PersistenceService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_PROGRESS.equals(action)) {
                handleUpdateProgress((Progress) intent.getParcelableExtra(EXTRA_PROGRESS));
            } else if (ACTION_UPDATE_CHALLENGE.equals(action)) {
                handleUpdateChallenge((Challenge) intent.getParcelableExtra(EXTRA_CHALLENGE));
            } else if (ACTION_CREATE_CHALLENGE.equals(action)) {
                handleCreateChallenge(
                        (Challenge) intent.getParcelableExtra(EXTRA_CHALLENGE),
                        intent.getBooleanExtra(EXTRA_SEND_EVENT, true)
                );
            } else if (ACTION_START_CHALLENGE.equals(action)) {
                handleStartChallenge((Challenge) intent.getParcelableExtra(EXTRA_CHALLENGE));
            } else if (ACTION_STOP_CHALLENGE.equals(action)) {
                handleStopChallenge((Challenge) intent.getParcelableExtra(EXTRA_CHALLENGE));
            } else if (ACTION_DELETE_CHALLENGE.equals(action)) {
                handleDeleteChallenge(intent.getLongExtra(EXTRA_CHALLENGE_ID, -1));
            }
        }
    }

    private void handleCreateChallenge(Challenge challenge, boolean sendEvent) {
        final PersistenceDbHelper helper = PersistenceDbHelper.getInstance(this);
        boolean success = false;
        int tries = 0;

        // Generate UUID, try to create the challenge. Max 3 attempts
        while (!success && tries < 3) {
            tries++;
            challenge.uuid = UUID.randomUUID().toString();
            success = PersistenceDbOps
                    .startTransaction(helper.getWritableDatabase())
                    .createChallenge(challenge)
                    .commit();
        }

        if (!success) {
            ApplicationContext.get().setChallengeSaved(null);
            return;
        }

        getContentResolver().notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);

        // todo: interim solution, implement adequately

        // If challenge is to be started right away
        if (challenge.isActive()) {
            handleStartChallenge(challenge);
        }

        // Report new saved challenge back to the form
        if (sendEvent) {
            ApplicationContext.get().setChallengeSaved(challenge);
        }
    }

    private void handleUpdateChallenge(Challenge challenge) {
        if (challenge.id == 0) {
            throw new IllegalArgumentException("The challenge must have ID field set.");
        }

        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getWritableDatabase();
        final PersistenceDbOps.Transaction transaction = PersistenceDbOps.startTransaction(db);

        // Update the challenge
        transaction.updateChallenge(challenge);

        // When updating the challenge, we must also determine if we need to update rules and reminders as well
        // It is agreed that null means no update required
        final boolean rulesChanged = challenge.getRules() != null;
        final boolean remindersChanged = challenge.getReminders() != null;

        if (remindersChanged) {
            final int currentMinuteOfDay = Utils.getCurrentMinuteOfDay();
            ScheduledReminder[] reminders = DataUtils.makeScheduledReminders(challenge.getReminders(), currentMinuteOfDay, challenge.id);
            transaction
                    .deleteRemindersForChallenge(challenge.id)
                    .insertReminders(reminders);
        }

        if (rulesChanged) {
            final int today = Utils.getCurrentEpochDay(this);
            Progress[] recordsToUpdate = PersistenceDbOps.getChallengeProgressSinceDay(db, today, challenge.id);
            DataUtils.recalculateRecordsCascade(recordsToUpdate, challenge.getRules());
            for (Progress progress : recordsToUpdate) {
                transaction.insertOrUpdateProgress(progress);
            }
        }

        boolean success = transaction.commit();

        // Now if the commit was successful
        if (success) {
            final ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);
            if (rulesChanged) {
                contentResolver.notifyChange(PersistenceContract.ProgressEntry.CONTENT_URI, null);
            }
            if (remindersChanged) {
                // todo: don't rely on current timestamp - possibly store the last processed reminder ts in prefs
                ReminderService.updateAlarm(this, System.currentTimeMillis());
            }
        }

        ApplicationContext.get().setChallengeSaved(null);
    }

    private void handleStartChallenge(Challenge challenge) {
        ScheduledReminder[] reminders = DataUtils.makeScheduledReminders(
                challenge.getReminders(), Utils.getCurrentMinuteOfDay(), challenge.id
        );
        final Progress firstDayRecord = DataUtils.makeEmptyRecord(
                challenge.id, challenge.startedOn, challenge.getRules(), null
        );

        boolean success = PersistenceDbOps
                .startTransaction(PersistenceDbHelper.getInstance(this).getWritableDatabase())
                .updateChallenge(challenge)
                .insertReminders(reminders)
                .insertOrUpdateProgress(firstDayRecord)
                .commit();

        if (success) {
            PrefUtils.trySetFirstChallengeStartDay(this, challenge.startedOn);
            final ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);
            contentResolver.notifyChange(PersistenceContract.ProgressEntry.CONTENT_URI, null);
            // todo: don't rely on current timestamp - possibly store the last processed reminder ts in prefs
            ReminderService.updateAlarm(this, System.currentTimeMillis());
        }
    }

    private void handleStopChallenge(Challenge challenge) {
        boolean success = PersistenceDbOps
                .startTransaction(PersistenceDbHelper.getInstance(this).getWritableDatabase())
                .updateChallenge(challenge)
                .deleteRemindersForChallenge(challenge.id)
                .commit();

        if (success) {
            final ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);
            contentResolver.notifyChange(PersistenceContract.ProgressEntry.CONTENT_URI, null);
            // todo: don't rely on current timestamp - possibly store the last processed reminder ts in prefs
            ReminderService.updateAlarm(this, System.currentTimeMillis());
        }
    }

    private void handleDeleteChallenge(long challengeId) {
        boolean success = PersistenceDbOps
                .startTransaction(PersistenceDbHelper.getInstance(this).getWritableDatabase())
                .deleteChallenge(challengeId)
                .commit();

        if (success) {
            final ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);
        }
    }

    private void handleUpdateProgress(Progress progress) {
        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getWritableDatabase();

        Progress[] recordsToUpdate = PersistenceDbOps.getChallengeProgressSinceDay(db, progress.day, progress.challengeId);
        if (recordsToUpdate.length == 0 || recordsToUpdate[0].day != progress.day) {
            Log.e(TAG, "Invalid progress set obtained for update, aborting update operation");
            return;
        }

        // First record is the one, which changes get applied in cascaded manner onto the rest of the array
        recordsToUpdate[0] = progress;
        DataUtils.recalculateRecordsCascade(recordsToUpdate);

        final PersistenceDbOps.Transaction transaction = PersistenceDbOps.startTransaction(db);
        for (Progress recordToUpdate : recordsToUpdate) {
            transaction.insertOrUpdateProgress(recordToUpdate);
        }
        boolean success = transaction.commit();

        if (success) {
            final ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange(PersistenceContract.ChallengeEntry.CONTENT_URI, null);
            contentResolver.notifyChange(PersistenceContract.ProgressEntry.CONTENT_URI, null);
        }
    }

}
