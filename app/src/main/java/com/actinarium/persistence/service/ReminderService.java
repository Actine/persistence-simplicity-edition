package com.actinarium.persistence.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.format.DateUtils;
import android.util.Log;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.PersistenceDbHelper;
import com.actinarium.persistence.data.PersistenceDbOps;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.dto.ScheduledReminder;
import com.actinarium.persistence.ui.home.HomeActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


/**
 * Intent service class that handles notifications functionality
 */
public class ReminderService extends IntentService {

    private static final String TAG = "ReminderService";

    private static final String ACTION_FIRE_ALARM = "com.actinarium.persistence.intent.action.FIRE_ALARM";
    private static final String ACTION_UPDATE_ALARM = "com.actinarium.persistence.intent.action.UPDATE_ALARM";
    private static final String ACTION_POSTPONE_REMINDERS = "com.actinarium.persistence.intent.action.POSTPONE_REMINDERS";
    private static final String ACTION_DISMISS_REMINDERS = "com.actinarium.persistence.intent.action.DISMISS_REMINDERS";

    private static final String EXTRA_REMINDER_TIMESTAMP = "com.actinarium.persistence.intent.extra.REMINDER_TIMESTAMP";

    private static final int REQ_CODE_NOTIFICATION_REMINDER = 0;
    private static final int REQ_CODE_POSTPONE_OR_DISMISS = 1;
    private static final int NOTIFICATION_REMINDER_ID = 0;

    /**
     * Will immediately show notification for all pending reminders if any, and set up alarm for the next reminder. Call
     * this method from {@link ReminderAlarmReceiver}. Also run this method when it is suspected that alarm might be off
     * (e.g. after device reboot, app update, while testing etc). <b>Note:</b> starts the service with wake lock!
     *
     * @param context
     */
    public static void fireAlarm(Context context) {
        Intent intent = new Intent(context, ReminderService.class);
        intent.setAction(ACTION_FIRE_ALARM);
        // sigh, it's all static...
        WakefulBroadcastReceiver.startWakefulService(context, intent);
    }

    /**
     * Call this programmatically when you processed your primary reminder intent
     *
     * @param reminderTimestamp Timestamp to identify reminders that were processed
     */
    public static void dismissReminders(Context context, long reminderTimestamp) {
        // Cancel the notification
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_REMINDER_ID);

        // Send onDismiss action to this service
        Intent intent = new Intent(context, ReminderService.class);
        intent.setAction(ACTION_DISMISS_REMINDERS);
        intent.putExtra(EXTRA_REMINDER_TIMESTAMP, reminderTimestamp);
        context.startService(intent);
    }

    /**
     * Updates alarm: determines when the next reminder should fire. Disables the alarm if no reminders should fire in
     * the future
     *
     * @param context          Context
     * @param currentTimestamp Search for "next reminder" on or after this timestamp. You should always pass here the
     *                         last processed timestamp and not rely on {@link System#currentTimeMillis()} so that you
     *                         don't accidentally lose reminders that would fire just in these few msec between
     */
    public static void updateAlarm(Context context, long currentTimestamp) {
        Intent intent = new Intent(context, ReminderService.class);
        intent.setAction(ACTION_UPDATE_ALARM);
        intent.putExtra(EXTRA_REMINDER_TIMESTAMP, currentTimestamp);
        context.startService(intent);
    }


    public ReminderService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FIRE_ALARM.equals(action)) {
                handleFireAlarm();
                WakefulBroadcastReceiver.completeWakefulIntent(intent);
            } else if (ACTION_UPDATE_ALARM.equals(action)) {
                handleUpdateAlarm(intent.getLongExtra(EXTRA_REMINDER_TIMESTAMP, 0));
            } else if (ACTION_POSTPONE_REMINDERS.equals(action)) {
                handlePostponeReminders(intent.getLongExtra(EXTRA_REMINDER_TIMESTAMP, 0));
            } else if (ACTION_DISMISS_REMINDERS.equals(action)) {
                handleDismissReminders(intent.getLongExtra(EXTRA_REMINDER_TIMESTAMP, 0));
            }
        }
    }

    /**
     * Display notification for pending reminders if any. <b>Heads up!</b> Automatically reschedules alarm fire time
     */
    private void handleFireAlarm() {
        Notification notification;

        int currentEpochDay = Utils.getCurrentEpochDay(this);
        long currentTimestamp = System.currentTimeMillis();

        // Ensure that we have progress data to join, e.g. if notification is fired earlier than the user opens the app
        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getWritableDatabase();
        PersistenceDbOps.ensureIntegrity(db, currentEpochDay, null);
        ScheduledReminder[] reminders = PersistenceDbOps.getPendingReminders(db, currentTimestamp, true, currentEpochDay);

        // Filter irrelevant reminders (e.g. older ones if new ones for given challenge exist)
        reminders = filterReminders(reminders);

        final int length = reminders.length;
        if (length == 0) {
            Log.w(TAG, "Notification was triggered but there are no reminders to fire at the moment. Rescheduling anyway");
            handleUpdateAlarm(currentTimestamp);
            return;
        } else if (length == 1) {
            notification = makeSingleChallengeNotification(currentTimestamp, reminders[0]);
        } else {
            notification = makeMultipleChallengesNotification(currentTimestamp, reminders);
        }

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_REMINDER_ID, notification);

        // Reschedule alarm to run whenever the next notification is scheduled
        handleUpdateAlarm(currentTimestamp);
    }

    /**
     * Dismisses existing reminders (re-schedules them for same time tomorrow); schedules copies to fire in 30 min
     *
     * @param reminderTimestamp used to identify subset of reminders to re-schedule
     */
    private void handlePostponeReminders(long reminderTimestamp) {
        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getWritableDatabase();
        int thatDay = Utils.getEpochDay(reminderTimestamp, this);

        // Cancel the notification
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_REMINDER_ID);

        // Get reminders in question, and narrow them down to the relevant ones
        // Ensure that we have progress data to join, e.g. if postpone is fired earlier than the user opens the app
        PersistenceDbOps.ensureIntegrity(db, thatDay, null);
        ScheduledReminder[] reminders = PersistenceDbOps.getPendingReminders(db, reminderTimestamp, false, thatDay);
        ScheduledReminder[] remindersToSnooze = filterReminders(reminders);

        // Perform copy on the to-snooze reminders, meanwhile setting snooze flag and new time for them
        final long newTimestamp = System.currentTimeMillis() + DateUtils.MINUTE_IN_MILLIS * ScheduledReminder.SNOOZE_MINUTES;
        for (int i = 0, len = remindersToSnooze.length; i < len; i++) {
            final ScheduledReminder newReminder = new ScheduledReminder(remindersToSnooze[i], newTimestamp);
            newReminder.flags |= ScheduledReminder.FLAG_IS_SNOOZED;
            remindersToSnooze[i] = newReminder;
        }

        boolean status = PersistenceDbOps
                .startTransaction(db)
                .insertReminders(remindersToSnooze)
                .commit();
        if (!status) {
            Log.e(TAG, "Could not insert snooze reminders");
        }

        // Finally, request alarm update
        handleUpdateAlarm(reminderTimestamp);
    }

    private void handleDismissReminders(long reminderTimestamp) {
        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getWritableDatabase();

        // Delete snoozed reminders
        PersistenceDbOps.Transaction transaction = PersistenceDbOps.startTransaction(db);
        transaction.cleanUpSnoozedReminders(reminderTimestamp);

        // Re-schedule reminders to their next fire time
        ScheduledReminder[] reminders = PersistenceDbOps.getPendingReminders(db, reminderTimestamp);
        final int currentMinuteOfDay = Utils.getCurrentMinuteOfDay();

        for (ScheduledReminder reminder : reminders) {
            if (reminder.time > currentMinuteOfDay) {
                reminder.scheduledAt = Utils.getTimeAt(reminder.time, 0).getTimeInMillis();
            } else {
                reminder.scheduledAt = Utils.getTimeAt(reminder.time, 1).getTimeInMillis();
            }
        }

        boolean status = transaction.updateReminders(reminders).commit();
        if (!status) {
            Log.e(TAG, "Could not update past reminders with new timestamps");
        }

        // Finally, request alarm update
        handleUpdateAlarm(reminderTimestamp);
    }

    private void handleUpdateAlarm(long currentTimestamp) {
        // Prepare pending intent. Setting, updating, or cancelling the alarm - we need it in either case
        Intent intent = new Intent(this, ReminderAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, REQ_CODE_NOTIFICATION_REMINDER, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Request nearest reminder timestamp via sql query (with fire time > currentTimestamp)
        final SQLiteDatabase db = PersistenceDbHelper.getInstance(this).getReadableDatabase();
        long nextNotificationTimestamp = PersistenceDbOps.getClosestReminderTimestamp(db, currentTimestamp);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (nextNotificationTimestamp == 0) {
            // No reminders (how strange! What's wrong with you, user?)
            alarmManager.cancel(pendingIntent);
        } else {
            if (Build.VERSION.SDK_INT < 19) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, nextNotificationTimestamp, pendingIntent);
            } else {
                alarmManager.setWindow(AlarmManager.RTC_WAKEUP, nextNotificationTimestamp, DateUtils.MINUTE_IN_MILLIS, pendingIntent);
            }
        }
    }

    /**
     * Builds a notification for single challenge
     *
     * @param currentTimestamp
     * @param reminder
     * @return
     */
    private Notification makeSingleChallengeNotification(long currentTimestamp, ScheduledReminder reminder) {
        // Primary action for single notification: open home, scroll to the challenge with given ID, display add progress dialog
        Intent primaryAction = new Intent(this, HomeActivity.class);
        primaryAction.setAction(HomeActivity.ACTION_ADD_PROGRESS);
        primaryAction.putExtra(HomeActivity.EXTRA_CHALLENGE_ID, reminder.challengeId);
        primaryAction.putExtra(HomeActivity.EXTRA_REMINDER_TIMESTAMP, currentTimestamp);

        // Create stack builder, get pending intent for primary action
        TaskStackBuilder tsBuilder = TaskStackBuilder.create(this)
                .addParentStack(HomeActivity.class)
                .addNextIntent(primaryAction);
        PendingIntent piPrimaryAction = tsBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Dismiss action for when the notification is swiped away (means the user acknowledged the reminder)
        Intent dismissAction = new Intent(this, ReminderService.class);
        dismissAction.setAction(ACTION_DISMISS_REMINDERS);
        dismissAction.putExtra(EXTRA_REMINDER_TIMESTAMP, reminder.scheduledAt);
        PendingIntent piDismissAction = PendingIntent.getService(this, REQ_CODE_POSTPONE_OR_DISMISS, dismissAction, PendingIntent.FLAG_UPDATE_CURRENT);

        // Postpone action
        Intent postponeAction = new Intent(this, ReminderService.class);
        postponeAction.setAction(ACTION_POSTPONE_REMINDERS);
        postponeAction.putExtra(EXTRA_REMINDER_TIMESTAMP, reminder.scheduledAt);
        PendingIntent piPostponeAction = PendingIntent.getService(this, REQ_CODE_POSTPONE_OR_DISMISS, postponeAction, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create public notification
        final Notification publicNotification = getCommonBuilder(currentTimestamp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getResources().getQuantityString(R.plurals.stacked_header, 1, 1))
                .setContentIntent(piPrimaryAction)
                .setDeleteIntent(piDismissAction)
                .build();

        // Create and return private notification
        return getCommonBuilder(currentTimestamp)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setPublicVersion(publicNotification)
                .setContentTitle(reminder.challengeTitle)
                .setContentText(getString(R.string.single_subtitle))
                .setContentIntent(piPrimaryAction)
                .setDeleteIntent(piDismissAction)
                .addAction(R.drawable.ic_access_time_white_24dp, getString(R.string.snooze_30_min), piPostponeAction)
                .build();
    }

    /**
     * Builds a notification for multiple challenges
     *
     * @param currentTimestamp
     * @param reminders
     * @return
     */
    private Notification makeMultipleChallengesNotification(long currentTimestamp, ScheduledReminder[] reminders) {
        final int length = reminders.length;

        // Primary action for multiple notifications: open home, where challenges will be re-sorted so that urgent ones are on top
        Intent primaryAction = new Intent(this, HomeActivity.class);
        primaryAction.putExtra(HomeActivity.EXTRA_REMINDER_TIMESTAMP, currentTimestamp);

        // Create stack builder, get pending intent for primary action
        TaskStackBuilder tsBuilder = TaskStackBuilder.create(this)
                .addParentStack(HomeActivity.class)
                .addNextIntent(primaryAction);
        PendingIntent piPrimaryAction = tsBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Dismiss action for when the notification is swiped away (means the user acknowledged the reminder)
        Intent dismissAction = new Intent(this, ReminderService.class);
        dismissAction.setAction(ACTION_DISMISS_REMINDERS);
        dismissAction.putExtra(EXTRA_REMINDER_TIMESTAMP, currentTimestamp);
        PendingIntent piDismissAction = PendingIntent.getService(this, REQ_CODE_POSTPONE_OR_DISMISS, dismissAction, PendingIntent.FLAG_UPDATE_CURRENT);

        // Postpone action
        Intent postponeAction = new Intent(this, ReminderService.class);
        postponeAction.setAction(ACTION_POSTPONE_REMINDERS);
        postponeAction.putExtra(EXTRA_REMINDER_TIMESTAMP, currentTimestamp);
        PendingIntent piPostponeAction = PendingIntent.getService(this, REQ_CODE_POSTPONE_OR_DISMISS, postponeAction, PendingIntent.FLAG_UPDATE_CURRENT);

        String attentionText = getResources().getQuantityString(R.plurals.stacked_header, length, length);

        // Create public notification
        final Notification publicNotification = getCommonBuilder(currentTimestamp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(attentionText)
                .setContentIntent(piPrimaryAction)
                .setDeleteIntent(piDismissAction)
                .build();

        // Create expanded style for private notification.
        // If there are more than 6 challenges, enlist first 4 and add "+x more"
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(attentionText);
        if (length <= 5) {
            for (ScheduledReminder reminder : reminders) {
                inboxStyle.addLine(reminder.challengeTitle);
            }
        } else {
            for (int i = 0; i < 4; i++) {
                inboxStyle.addLine(reminders[i].challengeTitle);
            }
            inboxStyle.addLine(getString(R.string.stacked_overflow, length - 4));
        }

        // Create and return private notification
        return getCommonBuilder(currentTimestamp)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setPublicVersion(publicNotification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(attentionText)
                .setContentIntent(piPrimaryAction)
                .setDeleteIntent(piDismissAction)
                .addAction(R.drawable.ic_access_time_white_24dp, getString(R.string.snooze_30_min), piPostponeAction)
                .setStyle(inboxStyle)
                .build();
    }

    /**
     * Returns common builder that still needs all intents and texts to be set
     *
     * @param currentTimestamp Timestamp to show in notification
     * @return
     */
    private NotificationCompat.Builder getCommonBuilder(long currentTimestamp) {
        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_check_white_24dp)
                .setColor(getResources().getColor(R.color.accentAsPrimary))
                .setCategory(NotificationCompat.CATEGORY_EVENT)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setWhen(currentTimestamp)
                .setShowWhen(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL);
    }

    /**
     * Filter the list of reminders to contain only the latest reminder per challenge (in the future - also filter away
     * non-critical reminders based on reminder type / today's progress).
     *
     * @param reminders Reminders to filter
     * @return Filtered list of reminders
     */
    private static ScheduledReminder[] filterReminders(ScheduledReminder[] reminders) {
        // Reminders already come in order of timestamp asc. So just assemble a map of challenge id -> reminder, but
        // keep the order so that first reminders come first
        // For that we iterate reminders collection from the end
        int len = reminders.length;
        List<ScheduledReminder> filteredBackwards = new ArrayList<>(len);
        HashSet<Long> usedChallengeIds = new HashSet<>(len, 1);
        for (int i = len; i > 0; ) {
            i--;

            // First let's check if this reminder must fire given its setup and current progress
            final ScheduledReminder reminder = reminders[i];
            final int reminderFlags = reminder.getFlags();
            if ((reminderFlags == Reminder.FLAG_FIRE_IF_NO_PROGRESS && reminder.totalProgress > 0)
                    || (reminderFlags == Reminder.FLAG_FIRE_IF_GOAL_NOT_MET && reminder.totalProgress >= reminder.totalGoal)) {
                continue;
            }

            // Don't add if challenge with this ID was already added
            if (!usedChallengeIds.contains(reminders[i].challengeId)) {
                usedChallengeIds.add(reminders[i].challengeId);
                filteredBackwards.add(reminders[i]);
            }
        }
        len = filteredBackwards.size();
        ScheduledReminder[] filtered = new ScheduledReminder[len];
        for (int i = 0, j = len; i < len; i++) {
            filtered[i] = filteredBackwards.get(--j);
        }
        return filtered;
    }
}
