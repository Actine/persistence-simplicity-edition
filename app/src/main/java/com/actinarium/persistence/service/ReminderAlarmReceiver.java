package com.actinarium.persistence.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class ReminderAlarmReceiver extends WakefulBroadcastReceiver {

    public ReminderAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        ReminderService.fireAlarm(context);
    }
}
