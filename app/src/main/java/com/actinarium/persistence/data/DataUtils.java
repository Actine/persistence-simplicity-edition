package com.actinarium.persistence.data;

import android.database.Cursor;
import android.support.annotation.Nullable;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.PersistenceContract.SyntheticEntry;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.dto.ScheduledReminder;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Utility methods for Persistence data calculations and integrity
 *
 * @author Paul Danyliuk
 */
public final class DataUtils {

    private DataUtils() {}

    /**
     * Returns the sum array of given projection and required columns, without duplicates
     *
     * @param projection      Initial projection
     * @param requiredColumns Columns to append if not present in initial projection
     * @return Combined projection
     */
    public static String[] ensureProjectionIncludes(String[] projection, String... requiredColumns) {
        if (projection == null) {
            return null;
        } else {
            Set<String> result = new LinkedHashSet<>(Arrays.asList(projection));
            result.addAll(Arrays.asList(requiredColumns));
            return result.toArray(new String[result.size()]);
        }
    }

    /**
     * Generates a zero-progress record for given day based on previous day, if present
     *
     * @param previous Previous day (for debt and base reserve calculation). Set null if starting a new challenge
     * @param day      Current epoch day
     * @param rules    Set of rules for current challenge, to determine daily goal
     * @return New progress record for today
     */
    public static Progress makeEmptyRecord(long challengeId, int day, Rule[] rules, @Nullable Progress previous) {
        Progress record = new Progress();
        record.challengeId = challengeId;
        record.day = day;
        Rule applicantRule = determineRuleForDay(rules, day);
        record.dailyGoal = applicantRule.quantity;
        if (previous == null) {
            record.debt = 0;
            record.debtMultiplier = Progress.DEFAULT_DEBT_MULTIPLIER;
            record.doubleReserveBalance = 0;
        } else {
            record.debt = previous.getProspectedDebt();
            record.debtMultiplier = previous.debtMultiplier;
            record.doubleReserveBalance = previous.doubleReserveBalance + previous.getDoubleReserveDelta();
        }
        record.completed = 0;
        record.compensated = 0;
        return record;
    }

    /**
     * Recalculate given record if previous record is changed (e.g. when editing previous day)
     *
     * @param record   Record to update
     * @param previous Previous record with allegedly updated values
     */
    public static void recalculateRecord(Progress record, Progress previous) {
        record.debt = previous.getProspectedDebt();
        record.debtMultiplier = previous.debtMultiplier;
        record.doubleReserveBalance = previous.doubleReserveBalance + previous.getDoubleReserveDelta();
    }

    /**
     * Recalculate given record with new rules (e.g. when challenge is edited). It is your responsibility to propagate
     * the changes to subsequent records using this and {@link #recalculateRecord(Progress, Progress)} functions
     *
     * @param record Record to update
     * @param rules  Rules that apply to this challenge, allegedly updated
     */
    public static void recalculateRecord(Progress record, Rule[] rules) {
        Rule applicantRule = determineRuleForDay(rules, record.day);
        record.dailyGoal = applicantRule.quantity;
    }

    /**
     * Recalculate the records in a cascaded manner, applying the new set of rules to each, and recalculating i-th
     * progress record based on (i-1)-th. <b>Important:</b> the records must be sorted by day ascending. If rules are
     * unchanged, use {@link #recalculateRecordsCascade(Progress[])} instead for better efficiency.
     *
     * @param records array of records to update in a cascaded manner, sorted by day ascending
     * @param rules   challenge rules to apply to each record
     */
    public static void recalculateRecordsCascade(Progress[] records, Rule[] rules) {
        final int length = records.length;
        if (length > 0) {
            DataUtils.recalculateRecord(records[0], rules);
            for (int i = 1; i < length; i++) {
                DataUtils.recalculateRecord(records[i], rules);
                DataUtils.recalculateRecord(records[i], records[i - 1]);
            }
        }
    }

    /**
     * Recalculate the records in a cascaded manner, recalculating i-th progress record based on (i-1)-th (i starts from
     * 1, the first item is unchanged and serves as cascade start). <b>Important:</b> the records must be sorted by day
     * ascending. Use this when updating progress for some day possibly in the past, without changing rules - otherwise
     * use {@link #recalculateRecord(Progress, Rule[])}.
     *
     * @param records array of records to update in a cascaded manner, sorted by day ascending
     */
    public static void recalculateRecordsCascade(Progress[] records) {
        final int length = records.length;
        if (length > 1) {
            for (int i = 1; i < length; i++) {
                DataUtils.recalculateRecord(records[i], records[i - 1]);
            }
        }
    }

    /**
     * Determine which of the given rules applies to given day. <b>Note:</b> it is assumed that the rules are ordered by
     * priority ASCENDING, i.e. the first one is the default one
     *
     * @param rules List of rules ordered by "order" ascending (i.e. 0-th element is default)
     * @param day   day to determine rule for
     * @return rule matching given day
     */
    public static Rule determineRuleForDay(Rule[] rules, int day) {
        int lastIndex = rules.length - 1;
        // assume that rules.size() is always > 0;

        int todayFlag = 1 << (Utils.getDayOfWeek(day));
        for (int i = lastIndex; i > 0; i--) {
            Rule rule = rules[i];
            // Skip if rule is for certain time range but not for now
            if (rule.isTemporary && (day < rule.startDay || day > rule.endDay)) {
                continue;
            }
            // Return this rule if it matches current day of week
            if ((rule.weekdayFlags & todayFlag) == todayFlag) {
                return rule;
            }
        }
        // otherwise if nothing matched, return default (zeroth) rule
        return rules[0];
    }

    /**
     * Extract the spark for the last two days from {@link PersistenceContract.BuildingBlocks#sChallengesWithLastRecordsJoinAnd2DaySpark}
     * request.
     *
     * @param cursor Cursor for the request
     * @return Array of two spark values - [0] is 1d ago, [1] is 2d ago
     */
    public static int[] extractLast2DaySpark(Cursor cursor) {
        final Integer debt1dAgo = CursorReaderUtils.getNullableInt(cursor, SyntheticEntry.YESTERDAY_DEBT);
        final Integer doubleReserve1dAgo = CursorReaderUtils.getNullableInt(cursor, SyntheticEntry.YESTERDAY_2X_RESERVE);
        final Integer debt2dAgo = CursorReaderUtils.getNullableInt(cursor, SyntheticEntry.BEFORE_YESTERDAY_DEBT);
        final Integer doubleReserve2dAgo = CursorReaderUtils.getNullableInt(cursor, SyntheticEntry.BEFORE_YESTERDAY_2X_RESERVE);
        if (debt1dAgo != null && doubleReserve1dAgo != null) {
            final int spark1dAgo, spark2dAgo;
            spark1dAgo = Progress.calculateSpark(doubleReserve1dAgo, debt1dAgo);
            if (debt2dAgo != null && doubleReserve2dAgo != null) {
                spark2dAgo = Progress.calculateSpark(doubleReserve2dAgo, debt2dAgo);
            } else {
                spark2dAgo = 0;
            }
            return new int[]{spark1dAgo, spark2dAgo};
        } else {
            return new int[]{0, 0};
        }
    }

    public static ScheduledReminder[] makeScheduledReminders(Reminder[] reminders, int currentMinuteOfDay, long challengeId) {
        ScheduledReminder[] scheduledReminders = new ScheduledReminder[reminders.length];
        int i = 0;
        for (Reminder reminder : reminders) {
            long timestamp;

            // If reminder time already passed today, schedule for tomorrow
            if (reminder.time > currentMinuteOfDay) {
                timestamp = Utils.getTimeAt(reminder.time, 0).getTimeInMillis();
            } else {
                timestamp = Utils.getTimeAt(reminder.time, 1).getTimeInMillis();
            }

            scheduledReminders[i++] = new ScheduledReminder(reminder, challengeId, timestamp);
        }

        return scheduledReminders;
    }
}
