package com.actinarium.persistence.data;

import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import com.actinarium.persistence.BuildConfig;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.ScheduledReminder;

/**
 * Contract for Persistence database. ContentProvider implements only one request (to support widgets in the future),
 * all other interaction is performed directly on DB via services so far. If required, the content provider will be
 * extended eventually Available URI's: <dl> <dt>content://com.actinarium.tiomantas.provider/challenges/today</dt>
 * <dd>Get all active challenges joined with today's progress entry</dd> </dl>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class PersistenceContract {

    public static final String CONTENT_AUTHORITY = BuildConfig.CONTENT_AUTHORITY;
    public static final String PATH_CHALLENGES = "challenges";
    public static final String PATH_PROGRESS = "progress";
    public static final String PATH_TODAY = "today";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Contract for Challenges
     */
    public static class ChallengeEntry implements BaseColumns {

        // Database stuff
        public static final String TABLE_NAME = "challenges";

        public static final String COL_UUID = "uuid";
        public static final String COL_TITLE = "title";
        public static final String COL_DESCRIPTION = "description";
        public static final String COL_DISPLAY_PRIORITY = "display_priority";      // Sorted from highest to lowest. New challenges are given 0 priority
        public static final String COL_STATUS_FLAGS = "status_flags";
        public static final String COL_CREATED_ON = "created_on";                  // Day when the challenge was created
        public static final String COL_STARTED_ON = "started_on";                  // Day when the challenge was started
        public static final String COL_RULES_SET_ON = "rules_set_on";              // Day when the challenge rules were last edited
        public static final String COL_UNITS = "units";
        public static final String COL_PROTOTYPE_ID = "prototype_id";              // "Parent" challenge - e.g. the one that was given up and spawned this
        public static final String COL_STOPPED_ON = "stopped_on";                  // Day when the challenge is stopped
        public static final String COL_STOP_REASON = "stop_reason";                // Reason why the challenge was stopped (entered by user)
        public static final String COL_RULES = "rules";                            // Serialized collection of daily rules, instead of separate table
        public static final String COL_REMINDERS = "reminders";                    // Serialized collection of reminder settings

        // Content provider stuff
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHALLENGES).build();
        public static final Uri CONTENT_URI_EXTENDED = CONTENT_URI.buildUpon().appendPath(PATH_TODAY).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "." + PATH_CHALLENGES;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "." + PATH_CHALLENGES;
    }

    /**
     * Contract for Challenge Progress entries
     */
    public static class ProgressEntry implements BaseColumns {

        // Database stuff
        public static final String TABLE_NAME = "progress";

        public static final String COL_CHALLENGE_ID = "challenge_id";
        public static final String COL_DAY = "day";
        public static final String COL_DAILY_GOAL = "daily_goal";
        public static final String COL_DEBT = "debt";
        public static final String COL_DEBT_MULTIPLIER = "debt_multiplier";    // in percent, e.g. default is 150
        public static final String COL_COMPLETED = "completed";
        public static final String COL_COMPENSATED = "compensated";
        public static final String COL_2X_RESERVE = "reserve";                 // 2x value of reserve balance AS OF START OF THE DAY
        public static final String COL_NOTE = "note";

        // Content provider stuff
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PROGRESS).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "." + PATH_PROGRESS;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "." + PATH_PROGRESS;
    }

    public static class ReminderEntry implements BaseColumns {

        // Database stuff
        public static final String TABLE_NAME = "reminders";

        public static final String COL_SCHEDULED_AT = "scheduled_at";
        public static final String COL_CHALLENGE_ID = "challenge_id";
        public static final String COL_REMINDER_TIME = "reminder_time";
        public static final String COL_REMINDER_FLAGS = "reminder_flags";

    }

    // Synthetic table and column aliases etc
    public static class SyntheticEntry {

        // tables
        public static final String YESTERDAY = "py";
        public static final String BEFORE_YESTERDAY = "ppy";

        // fields
        public static final String YESTERDAY_DEBT = YESTERDAY + "_" + ProgressEntry.COL_DEBT;
        public static final String YESTERDAY_2X_RESERVE = YESTERDAY + "_" + ProgressEntry.COL_2X_RESERVE;
        public static final String BEFORE_YESTERDAY_DEBT = BEFORE_YESTERDAY + "_" + ProgressEntry.COL_DEBT;
        public static final String BEFORE_YESTERDAY_2X_RESERVE = BEFORE_YESTERDAY + "_" + ProgressEntry.COL_2X_RESERVE;

        // calculations
        public static final String PROGRESS_TOTAL_GOAL_FORMULA = ProgressEntry.COL_DAILY_GOAL + " + " + ProgressEntry.COL_DEBT;
        public static final String PROGRESS_TOTAL_PROGRESS_FORMULA = ProgressEntry.COL_COMPLETED + " + " + ProgressEntry.COL_COMPENSATED;
        public static final String PROGRESS_TOTAL_GOAL = "total_goal";
        public static final String PROGRESS_TOTAL_PROGRESS = "total_progress";

    }

    public static final class Projections {

        private Projections() {}

        public static final String[] CHALLENGE_WITH_PROGRESS_FULL = {
                ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID,
                ChallengeEntry.COL_TITLE,
                ChallengeEntry.COL_DESCRIPTION,
                ChallengeEntry.COL_DISPLAY_PRIORITY,
                ChallengeEntry.COL_STATUS_FLAGS,
                ChallengeEntry.COL_CREATED_ON,
                ChallengeEntry.COL_STARTED_ON,
                ChallengeEntry.COL_RULES_SET_ON,
                ChallengeEntry.COL_PROTOTYPE_ID,
                ChallengeEntry.COL_UNITS,
                ChallengeEntry.COL_STOPPED_ON,
                ChallengeEntry.COL_STOP_REASON,
                ChallengeEntry.COL_RULES,
                ChallengeEntry.COL_REMINDERS,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry._ID + " AS " + ProgressEntry.TABLE_NAME + "_" + ProgressEntry._ID,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_CHALLENGE_ID,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAILY_GOAL,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DEBT,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DEBT_MULTIPLIER,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_COMPLETED,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_COMPENSATED,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_2X_RESERVE,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_NOTE,
                SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_DEBT + " AS " + SyntheticEntry.YESTERDAY_DEBT,
                SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_2X_RESERVE + " AS " + SyntheticEntry.YESTERDAY_2X_RESERVE,
                SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_DEBT + " AS " + SyntheticEntry.BEFORE_YESTERDAY_DEBT,
                SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_2X_RESERVE + " AS " + SyntheticEntry.BEFORE_YESTERDAY_2X_RESERVE,

        };
        public static final String[] CHALLENGE_WITH_PROGRESS_MINIMAL_FOR_INTEGRITY = {
                ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID,
                ChallengeEntry.COL_STATUS_FLAGS,
                ChallengeEntry.COL_STARTED_ON,
                ChallengeEntry.COL_RULES,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAILY_GOAL,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DEBT,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DEBT_MULTIPLIER,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_COMPLETED,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_COMPENSATED,
                ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_2X_RESERVE
        };
        public static final String[] REMINDER_WITH_PROGRESS_AND_CHALLENGE_DATA = {
                ReminderEntry.TABLE_NAME + ".*",
                ChallengeEntry.COL_TITLE,
                SyntheticEntry.PROGRESS_TOTAL_GOAL_FORMULA + " AS " + SyntheticEntry.PROGRESS_TOTAL_GOAL,
                SyntheticEntry.PROGRESS_TOTAL_PROGRESS_FORMULA + " AS " + SyntheticEntry.PROGRESS_TOTAL_PROGRESS
        };
        public static final String[] REMINDER_WITH_PROGRESS_DATA = {
                ReminderEntry.TABLE_NAME + ".*",
                SyntheticEntry.PROGRESS_TOTAL_GOAL_FORMULA + " AS " + SyntheticEntry.PROGRESS_TOTAL_GOAL,
                SyntheticEntry.PROGRESS_TOTAL_PROGRESS_FORMULA + " AS " + SyntheticEntry.PROGRESS_TOTAL_PROGRESS
        };
    }

    /**
     * Contains prepared queries, selection and sort order expressions etc
     */
    public static final class BuildingBlocks {

        private BuildingBlocks() {}

        public static final String SELECTION_DISPLAYED_CHALLENGES = ChallengeEntry.COL_STATUS_FLAGS + " & " + Challenge.FLAG_DISPLAYED;
        public static final String SELECTION_ID = BaseColumns._ID + " = ?";
        public static final String SELECTION_CHALLENGE_ID = ProgressEntry.COL_CHALLENGE_ID + " = ?";
        public static final String SELECTION_PROGRESS_DAY_IS = ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY + " = ?";
        public static final String SELECTION_PROGRESS_DAY_IS_ON_OR_AFTER = ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY + " >= ?";
        public static final String SELECTION_PROGRESS_CHALLENGE_SINCE = SELECTION_CHALLENGE_ID + " AND " + SELECTION_PROGRESS_DAY_IS_ON_OR_AFTER;
        public static final String SELECTION_PROGRESS_DAY_ISNULL = ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY + " ISNULL";
        public static final String SELECTION_REMINDER_IS_AFTER = ReminderEntry.COL_SCHEDULED_AT + " > ?";
        public static final String SELECTION_REMINDER_IS_ON_OR_BEFORE = ReminderEntry.COL_SCHEDULED_AT + " <= ?";
        public static final String SELECTION_REMINDER_PAST_AND_SNOOZED = SELECTION_REMINDER_IS_ON_OR_BEFORE + " AND " + ReminderEntry.COL_REMINDER_FLAGS + " & " + ScheduledReminder.FLAG_IS_SNOOZED;

        public static final String SORT_ORDER_CHALLENGES = ChallengeEntry.COL_DISPLAY_PRIORITY + " DESC";
        public static final String SORT_ORDER_PROGRESS = ProgressEntry.COL_DAY + " ASC";
        public static final String SORT_ORDER_REMINDERS = ReminderEntry.COL_SCHEDULED_AT + " ASC";

        public static SQLiteQueryBuilder sChallengesWithLastRecordsJoin = buildChallengesWithLastProgressJoin();
        public static SQLiteQueryBuilder sChallengesWithLastRecordsJoinAnd2DaySpark = buildChallengesWithLastProgressJoinAnd2DaySpark();
        public static SQLiteQueryBuilder sRemindersWithProgressJoin = buildRemindersWithProgressJoin();
        public static SQLiteQueryBuilder sRemindersWithChallengeAndProgressJoin = buildRemindersWithChallengeAndProgressJoin();

        // Reusable join statement
        private static final String STATEMENT_LEFT_JOIN_PROGRESS = " LEFT JOIN " + ProgressEntry.TABLE_NAME
                + " ON " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_CHALLENGE_ID
                + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID;

        /**
         * Creates a prepared query builder for Challenge [1]-[0..1] Record, where record is the one with MAX(day) per
         * challenge
         *
         * @return builder
         */
        private static SQLiteQueryBuilder buildChallengesWithLastProgressJoin() {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            final String statement =
                    ChallengeEntry.TABLE_NAME
                            + " LEFT JOIN "
                            + "(SELECT " + ProgressEntry.COL_CHALLENGE_ID + " AS tmp_cid"
                            + ", MAX(" + ProgressEntry.COL_DAY + ") AS tmp_md"
                            + " FROM " + ProgressEntry.TABLE_NAME
                            + " GROUP BY " + ProgressEntry.COL_CHALLENGE_ID
                            + ") AS tmp" +
                            " ON tmp.tmp_cid  = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " LEFT JOIN " + ProgressEntry.TABLE_NAME
                            + " ON " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_CHALLENGE_ID
                            + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " AND " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY
                            + " = tmp.tmp_md";
            builder.setTables(statement);
            return builder;
        }

        /**
         * Creates a prepared query builder for Challenge [1]-[0..1] Record, where record is the one with MAX(day) per
         * challenge
         *
         * @return builder
         */
        private static SQLiteQueryBuilder buildChallengesWithLastProgressJoinAnd2DaySpark() {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            final String statement =
                    ChallengeEntry.TABLE_NAME
                            + " LEFT JOIN "
                            + "(SELECT " + ProgressEntry.COL_CHALLENGE_ID + " AS tmp_cid"
                            + ", MAX(" + ProgressEntry.COL_DAY + ") AS tmp_md"
                            + " FROM " + ProgressEntry.TABLE_NAME
                            + " GROUP BY " + ProgressEntry.COL_CHALLENGE_ID
                            + ") AS tmp" +
                            " ON tmp.tmp_cid  = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " LEFT JOIN " + ProgressEntry.TABLE_NAME
                            + " ON " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_CHALLENGE_ID
                            + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " AND " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_DAY
                            + " = tmp.tmp_md"
                            + " LEFT JOIN " + ProgressEntry.TABLE_NAME + " AS " + SyntheticEntry.YESTERDAY
                            + " ON " + SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_CHALLENGE_ID
                            + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " AND " + SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_DAY
                            + " = tmp.tmp_md - 1"
                            + " LEFT JOIN " + ProgressEntry.TABLE_NAME + " AS " + SyntheticEntry.BEFORE_YESTERDAY
                            + " ON " + SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_CHALLENGE_ID
                            + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + " AND " + SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_DAY
                            + " = tmp.tmp_md - 2";
            builder.setTables(statement);
            return builder;
        }

        /**
         * Creates a prepared query builder for simple Challenge [1]-[inf] Record join, to be filtered by record day or
         * whatever
         *
         * @return builder
         */
        public static SQLiteQueryBuilder buildChallengesWithRecordsJoin(int today) {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            final String statement = ChallengeEntry.TABLE_NAME + STATEMENT_LEFT_JOIN_PROGRESS
                    + " LEFT JOIN " + ProgressEntry.TABLE_NAME + " AS " + SyntheticEntry.YESTERDAY
                    + " ON " + SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_CHALLENGE_ID
                    + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                    + " AND " + SyntheticEntry.YESTERDAY + "." + ProgressEntry.COL_DAY
                    + " = " + (today - 1)
                    + " LEFT JOIN " + ProgressEntry.TABLE_NAME + " AS " + SyntheticEntry.BEFORE_YESTERDAY
                    + " ON " + SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_CHALLENGE_ID
                    + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                    + " AND " + SyntheticEntry.BEFORE_YESTERDAY + "." + ProgressEntry.COL_DAY
                    + " = " + (today - 2);
            builder.setTables(statement);
            return builder;
        }

        private static SQLiteQueryBuilder buildRemindersWithProgressJoin() {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            final String statement = ReminderEntry.TABLE_NAME
                    + " LEFT JOIN " + ProgressEntry.TABLE_NAME
                    + " ON " + ProgressEntry.TABLE_NAME + "." + ProgressEntry.COL_CHALLENGE_ID
                    + " = " + ReminderEntry.TABLE_NAME + "." + ReminderEntry.COL_CHALLENGE_ID;
            builder.setTables(statement);
            return builder;
        }

        private static SQLiteQueryBuilder buildRemindersWithChallengeAndProgressJoin() {
            SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
            final String statement =
                    ReminderEntry.TABLE_NAME
                            + " JOIN " + ChallengeEntry.TABLE_NAME
                            + " ON " + ReminderEntry.TABLE_NAME + "." + ReminderEntry.COL_CHALLENGE_ID
                            + " = " + ChallengeEntry.TABLE_NAME + "." + ChallengeEntry._ID
                            + STATEMENT_LEFT_JOIN_PROGRESS;
            builder.setTables(statement);
            return builder;
        }

    }

}
