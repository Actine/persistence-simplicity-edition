package com.actinarium.persistence.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.actinarium.persistence.data.PersistenceContract.ReminderEntry;

import static com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import static com.actinarium.persistence.data.PersistenceContract.ProgressEntry;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class PersistenceDbHelper extends SQLiteOpenHelper {

    public static final String TAG = "PersistenceDbHelper";

    public static final String DATABASE_NAME = "persistence.db";
    private static final int DATABASE_VERSION = 1;

    private static PersistenceDbHelper mInstance;

    private PersistenceDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized PersistenceDbHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PersistenceDbHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String CREATE_CHALLENGE_TABLE =
                "CREATE TABLE " + ChallengeEntry.TABLE_NAME
                        + " (" + ChallengeEntry._ID + " INTEGER PRIMARY KEY"
                        + ", " + ChallengeEntry.COL_UUID + " TEXT NOT NULL"
                        + ", " + ChallengeEntry.COL_TITLE + " TEXT NOT NULL"
                        + ", " + ChallengeEntry.COL_DESCRIPTION + " TEXT"
                        + ", " + ChallengeEntry.COL_DISPLAY_PRIORITY + " INTEGER NOT NULL"
                        + ", " + ChallengeEntry.COL_STATUS_FLAGS + " INTEGER NOT NULL"
                        + ", " + ChallengeEntry.COL_CREATED_ON + " INTEGER NOT NULL"
                        + ", " + ChallengeEntry.COL_STARTED_ON + " INTEGER"
                        + ", " + ChallengeEntry.COL_RULES_SET_ON + " INTEGER NOT NULL"
                        + ", " + ChallengeEntry.COL_STOPPED_ON + " INTEGER"
                        + ", " + ChallengeEntry.COL_UNITS + " TEXT NOT NULL"
                        + ", " + ChallengeEntry.COL_PROTOTYPE_ID + " INTEGER"
                        + ", " + ChallengeEntry.COL_STOP_REASON + " TEXT"
                        + ", " + ChallengeEntry.COL_RULES + " TEXT NOT NULL"
                        + ", " + ChallengeEntry.COL_REMINDERS + " TEXT NOT NULL"
                        + ", FOREIGN KEY (" + ChallengeEntry.COL_PROTOTYPE_ID
                        + ") REFERENCES " + ChallengeEntry.TABLE_NAME + "(" + ChallengeEntry._ID
                        + ") ON UPDATE CASCADE ON DELETE SET NULL"
                        + ", UNIQUE (" + ChallengeEntry.COL_UUID + ")"
                        + ")";

        final String CREATE_PROGRESS_TABLE =
                "CREATE TABLE " + ProgressEntry.TABLE_NAME
                        + " (" + ProgressEntry._ID + " INTEGER PRIMARY KEY"
                        + ", " + ProgressEntry.COL_CHALLENGE_ID + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_DAY + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_DAILY_GOAL + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_DEBT + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_DEBT_MULTIPLIER + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_COMPLETED + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_COMPENSATED + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_2X_RESERVE + " INTEGER NOT NULL"
                        + ", " + ProgressEntry.COL_NOTE + " TEXT"
                        + ", FOREIGN KEY (" + ProgressEntry.COL_CHALLENGE_ID
                        + ") REFERENCES " + ChallengeEntry.TABLE_NAME + "(" + ChallengeEntry._ID
                        + ") ON UPDATE CASCADE ON DELETE RESTRICT"
                        + ", UNIQUE (" + ProgressEntry.COL_CHALLENGE_ID + ", " + ProgressEntry.COL_DAY
                        + ") ON CONFLICT REPLACE"
                        + ")";

        final String CREATE_REMINDERS_TABLE =
                "CREATE TABLE " + ReminderEntry.TABLE_NAME
                        + " (" + ReminderEntry._ID + " INTEGER PRIMARY KEY"
                        + ", " + ReminderEntry.COL_SCHEDULED_AT + " INTEGER NOT NULL"
                        + ", " + ReminderEntry.COL_CHALLENGE_ID + " INTEGER NOT NULL"
                        + ", " + ReminderEntry.COL_REMINDER_TIME + " INTEGER"
                        + ", " + ReminderEntry.COL_REMINDER_FLAGS + " SMALLINT NOT NULL"
                        + ", FOREIGN KEY (" + ReminderEntry.COL_CHALLENGE_ID
                        + ") REFERENCES " + ChallengeEntry.TABLE_NAME + "(" + ChallengeEntry._ID
                        + ") ON UPDATE CASCADE ON DELETE CASCADE"
                        + ")";

        db.execSQL(CREATE_CHALLENGE_TABLE);
        db.execSQL(CREATE_PROGRESS_TABLE);
        db.execSQL(CREATE_REMINDERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // no-op so far
    }

}
