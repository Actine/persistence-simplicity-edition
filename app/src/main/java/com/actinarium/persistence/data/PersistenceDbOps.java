package com.actinarium.persistence.data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.data.PersistenceContract.BuildingBlocks;
import com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import com.actinarium.persistence.data.PersistenceContract.ProgressEntry;
import com.actinarium.persistence.data.PersistenceContract.ReminderEntry;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.dto.ScheduledReminder;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Database operations facade with transaction builder
 *
 * @author Paul Danyliuk
 */
public final class PersistenceDbOps {

    private PersistenceDbOps() {}

    /**
     * Get a transaction builder for inserting/updating stuff
     *
     * @param writableDb Database
     * @return Transaction builder and executor
     */
    public static Transaction startTransaction(SQLiteDatabase writableDb) {
        return new Transaction(writableDb);
    }

    /**
     * Retrieve progress entries for a given challenge since a given day inclusively. Call this when implementing
     * cascading of progress update, when next day entry depends on previous day entry
     *
     * @param db          Readable database
     * @param epochDay    Day since when records must be queried, inclusively.
     * @param challengeId ID of the challenge for which progress must be queried
     * @return array of progress entries
     */
    public static Progress[] getChallengeProgressSinceDay(SQLiteDatabase db, int epochDay, long challengeId) {
        Cursor cursor = db.query(
                ProgressEntry.TABLE_NAME,
                null,
                BuildingBlocks.SELECTION_PROGRESS_CHALLENGE_SINCE,
                new String[]{Long.toString(challengeId), Integer.toString(epochDay)},
                null, null,
                BuildingBlocks.SORT_ORDER_PROGRESS
        );
        Progress[] result = new Progress[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            result[i++] = new Progress().fillFromCursor(cursor);
        }
        return result;
    }

    /**
     * Query reminders table and determine the closest reminder to fire (i.e. with min timestamp, which is >= current
     * timestamp)
     *
     * @param currentTimestamp Current timestamp, required to filter away previously fired but not yet discarded alarms
     * @return timestamp of the next reminder, or 0 if no reminders present
     */
    public static long getClosestReminderTimestamp(SQLiteDatabase db, long currentTimestamp) {
        Cursor cursor = db.query(
                ReminderEntry.TABLE_NAME,
                new String[]{ReminderEntry.COL_SCHEDULED_AT},
                BuildingBlocks.SELECTION_REMINDER_IS_AFTER,
                new String[]{Long.toString(currentTimestamp)},
                null,
                null,
                BuildingBlocks.SORT_ORDER_REMINDERS,
                "1"
        );
        return cursor.moveToFirst() ? CursorReaderUtils.getLong(cursor, ReminderEntry.COL_SCHEDULED_AT) : 0;
    }

    public static ScheduledReminder[] getPendingReminders(SQLiteDatabase db, long currentTimestamp, boolean withChallengeData, int progressDay) {
        Cursor cursor;
        if (withChallengeData) {
            cursor = BuildingBlocks.sRemindersWithChallengeAndProgressJoin.query(
                    db,
                    PersistenceContract.Projections.REMINDER_WITH_PROGRESS_AND_CHALLENGE_DATA,
                    BuildingBlocks.SELECTION_REMINDER_IS_ON_OR_BEFORE + " AND " + BuildingBlocks.SELECTION_PROGRESS_DAY_IS,
                    new String[]{Long.toString(currentTimestamp), Integer.toString(progressDay)},
                    null,
                    null,
                    BuildingBlocks.SORT_ORDER_REMINDERS
            );
        } else {
            cursor = BuildingBlocks.sRemindersWithProgressJoin.query(
                    db,
                    PersistenceContract.Projections.REMINDER_WITH_PROGRESS_DATA,
                    BuildingBlocks.SELECTION_REMINDER_IS_ON_OR_BEFORE + " AND " + BuildingBlocks.SELECTION_PROGRESS_DAY_IS,
                    new String[]{Long.toString(currentTimestamp), Integer.toString(progressDay)},
                    null,
                    null,
                    BuildingBlocks.SORT_ORDER_REMINDERS
            );
        }
        final int count = cursor.getCount();
        ScheduledReminder[] reminders = new ScheduledReminder[count];
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            reminders[i] = new ScheduledReminder().fillFromCursor(cursor);
        }
        return reminders;
    }

    public static ScheduledReminder[] getPendingReminders(SQLiteDatabase db, long currentTimestamp) {
        Cursor cursor = db.query(
                ReminderEntry.TABLE_NAME,
                null,
                BuildingBlocks.SELECTION_REMINDER_IS_ON_OR_BEFORE,
                new String[]{Long.toString(currentTimestamp)},
                null,
                null,
                BuildingBlocks.SORT_ORDER_REMINDERS
        );
        final int count = cursor.getCount();
        ScheduledReminder[] reminders = new ScheduledReminder[count];
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            reminders[i] = new ScheduledReminder().fillFromCursor(cursor);
        }
        return reminders;
    }

    /**
     * Makes sure there are progress records for each active project for all days since project start / last reset and
     * till given day: creates missing progress entries if required. Uses custom projection if supplied. If no changes
     * are required, returns cursor as of original request, so that it can be reused by whatever (content provider etc),
     * otherwise returns null. <b>Note:</b> uses request that joins each active project with the last available record.
     * If you need different data, e.g. a list of records, you won't be able to reuse the result anyway.
     *
     * @param thisDay          Day till which integrity must be ensured, usually this day (today)
     * @param customProjection Callers may provide custom projection with extra fields. Leave null if you won't need the
     *                         result
     * @return Cursor, if data is already intact and the cursor may be reused, or null if certain actions were performed
     * and whoever invoked this integrity check should re-query data.
     */
    public static Cursor ensureIntegrity(SQLiteDatabase db, int thisDay, @Nullable String[] customProjection) {
        String[] projection;
        Cursor cursor;
        boolean isReusable = true;

        if (customProjection != null) {
            projection = DataUtils.ensureProjectionIncludes(
                    customProjection,
                    PersistenceContract.Projections.CHALLENGE_WITH_PROGRESS_MINIMAL_FOR_INTEGRITY
            );

            // Since this is a reusable cursor, request it along with 2 day spark data
            cursor = PersistenceContract.BuildingBlocks.sChallengesWithLastRecordsJoinAnd2DaySpark.query(
                    db,
                    projection,
                    PersistenceContract.BuildingBlocks.SELECTION_DISPLAYED_CHALLENGES, null,
                    null, null,
                    PersistenceContract.BuildingBlocks.SORT_ORDER_CHALLENGES
            );
        } else {
            projection = PersistenceContract.Projections.CHALLENGE_WITH_PROGRESS_MINIMAL_FOR_INTEGRITY;
            isReusable = false;

            // We know this cursor won't be reused, so optimize the request without joining unnecessary tables
            cursor = PersistenceContract.BuildingBlocks.sChallengesWithLastRecordsJoin.query(
                    db,
                    projection,
                    PersistenceContract.BuildingBlocks.SELECTION_DISPLAYED_CHALLENGES, null,
                    null, null,
                    PersistenceContract.BuildingBlocks.SORT_ORDER_CHALLENGES
            );
        }

        final int count = cursor.getCount();

        // If there's no data, exit the function right now
        if (count == 0) {
            return cursor;
        }

        // Init lists of offending challenges and records
        List<Challenge> offendingChallenges = new LinkedList<>();
        List<Progress> offendingRecords = new LinkedList<>();
        int statusFlags;
        Integer recordDay;

        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);

            // Read status flags. If the challenge is not active, not action needed
            statusFlags = CursorReaderUtils.getInt(cursor, PersistenceContract.ChallengeEntry.COL_STATUS_FLAGS);
            if ((statusFlags & Challenge.FLAG_ACTIVE) == 0) {
                continue;
            }

            // Otherwise check the latest available record
            recordDay = CursorReaderUtils.getNullableInt(cursor, PersistenceContract.ProgressEntry.COL_DAY);
            if (recordDay == null) {
                // No entries at all so far - store offending tuple
                final Challenge challenge = new Challenge().fillFromCursor(cursor);
                offendingChallenges.add(challenge);
                offendingRecords.add(null);
            } else if (recordDay > thisDay) {
                // There are record items for next days (the future?) - meaning this data set cannot be returned
                isReusable = false;
            } else if (recordDay < thisDay) {
                // The last record item is in the past - meaning the new ones must be generated
                final Challenge challenge = new Challenge().fillFromCursor(cursor);
                offendingChallenges.add(challenge);
                offendingRecords.add(new Progress().fillFromCursor(cursor));
            }
        }
        // If there are no offending challenges, break the function
        if (offendingChallenges.isEmpty()) {
            if (isReusable) {
                return cursor;
            } else {
                cursor.close();
                return null;
            }
        }

        cursor.close();

        // Ok, so we have offending challenges
        int size = offendingChallenges.size();
        if (size != offendingRecords.size()) {
            throw new AssertionError("For unknown reason list sizes for offending challenges, records, and rule groups do not match");
        }

        // Walk over each offending challenge and calculate missing records
        Challenge challenge;
        Progress previous;
        // Assume that integrity check is rarely performed less than once per day
        List<Progress> recordsToInsert = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            challenge = offendingChallenges.get(i);
            previous = offendingRecords.get(i);
            Rule[] rules = challenge.getRules();
            int startDay;

            // If no records present, take start day from challenge inception day
            startDay = previous != null ? previous.day + 1 : challenge.startedOn;

            for (int day = startDay; day <= thisDay; day++) {
                previous = DataUtils.makeEmptyRecord(challenge.id, day, rules, previous);
                recordsToInsert.add(previous);
            }
        }

        // Finally, insert all records and hope for success
        startTransaction(db).bulkInsertProgress(recordsToInsert).commit();

        return null;
    }

    /**
     * Database insert/update transaction builder and executor. <b>Note:</b> actually commands are executed as called,
     * not in the end as it's usually in builders, so it's possible to obtain intermediate result
     */
    public static class Transaction {

        private SQLiteDatabase mDatabase;
        private boolean mIsSuccess;

        public Transaction(SQLiteDatabase database) {
            mDatabase = database;
            mIsSuccess = true;
            mDatabase.beginTransaction();
        }

        /**
         * @return intermediate result after the last performed action. Don't call this after commit.
         */
        public boolean getIntermediateResult() {
            return mIsSuccess;
        }

        public boolean commit() {
            if (mIsSuccess) {
                mDatabase.setTransactionSuccessful();
                mDatabase.endTransaction();
            }
            // Set mIsSuccess to false so that subsequent calls to the transaction are ignored
            boolean isRealSuccess = mIsSuccess;
            mIsSuccess = false;

            return isRealSuccess;
        }

        /**
         * Create challenge using {@link Challenge#toContentValuesOnCreate()}. If creation was successful, sets the ID
         * on challenge object.
         *
         * @param challenge Challenge
         * @return this for chaining
         */
        public Transaction createChallenge(Challenge challenge) {
            if (!mIsSuccess) {
                return this;
            }

            long challengeId = mDatabase.insert(ChallengeEntry.TABLE_NAME, null, challenge.toContentValuesOnCreate());
            if (challengeId != -1) {
                challenge.id = challengeId;
            } else {
                mIsSuccess = false;
                mDatabase.endTransaction();
            }

            return this;
        }

        /**
         * Update challenge using {@link Challenge#toContentValuesOnUpdate()}. Also use this to change challenge status
         * (start, stop, hide etc), although don't forget to update rules, reminders etc in the same transaction.
         *
         * @param challenge Challenge to update, must have proper {@link Challenge#id id} set
         * @return this for chaining
         */
        public Transaction updateChallenge(Challenge challenge) {
            if (!mIsSuccess) {
                return this;
            }

            int rowsAffected = mDatabase.update(
                    ChallengeEntry.TABLE_NAME,
                    challenge.toContentValuesOnUpdate(),
                    BuildingBlocks.SELECTION_ID,
                    new String[]{Long.toString(challenge.id)}
            );
            if (rowsAffected != 1) {
                mIsSuccess = false;
                mDatabase.endTransaction();
            }

            return this;
        }

        /**
         * Remove the challenge with provided ID. Regardless of affected rows, this method
         * cannot abort the transaction
         *
         * @param challengeId ID of the challenge to remove reminders for
         * @return this for chaining
         */
        public Transaction deleteChallenge(long challengeId) {
            if (!mIsSuccess) {
                return this;
            }

            mDatabase.delete(
                    ChallengeEntry.TABLE_NAME,
                    BuildingBlocks.SELECTION_ID,
                    new String[]{Long.toString(challengeId)}
            );

            return this;
        }

        /**
         * Insert or update single record. If {@link Progress#id id} <code>!= 0</code>, will attempt update operation,
         * otherwise will perform insert with on conflict replace. Either way, there won't be two records with the same
         * day and challenge id
         *
         * @param progress Progress to insert or update
         * @return this for chaining
         */
        public Transaction insertOrUpdateProgress(Progress progress) {
            if (!mIsSuccess) {
                return this;
            }

            if (progress.id > 0) {
                int rowsAffected = mDatabase.update(
                        ProgressEntry.TABLE_NAME,
                        progress.toContentValues(),
                        BuildingBlocks.SELECTION_ID,
                        new String[]{Long.toString(progress.id)}
                );
                if (rowsAffected != 1) {
                    mIsSuccess = false;
                    mDatabase.endTransaction();
                }
            } else {
                long id = mDatabase.insert(ProgressEntry.TABLE_NAME, null, progress.toContentValues());
                if (id == -1) {
                    mIsSuccess = false;
                    mDatabase.endTransaction();
                }
            }

            return this;
        }

        /**
         * Bulk insert records into the database. Will assign progress IDs. Don't use this method for updating existing
         * records - even though it will work because of ON CONFLICT constraint, it is recommended to use {@link
         * #insertOrUpdateProgress(Progress)} per progress entry instead, and use this method only for integrity
         *
         * @param progressRecords Collection of records to insert
         * @return this for chaining
         */
        public Transaction bulkInsertProgress(List<Progress> progressRecords) {
            if (!mIsSuccess) {
                return this;
            }

            for (int i = 0, size = progressRecords.size(); i < size; i++) {
                Progress progress = progressRecords.get(i);
                long id = mDatabase.insert(ProgressEntry.TABLE_NAME, null, progress.toContentValues());
                if (id == -1) {
                    mIsSuccess = false;
                    mDatabase.endTransaction();
                    // Reset ID's
                    for (int j = 0; j <= i; j++) {
                        progressRecords.get(j).id = 0;
                    }
                    return this;
                }
                progress.id = id;
            }

            return this;
        }

        /**
         * Insert scheduled reminder entries using {@link ScheduledReminder#toContentValuesOnCreate()}
         *
         * @param reminders reminders to schedule
         * @return this for chaining
         */
        public Transaction insertReminders(ScheduledReminder[] reminders) {
            if (!mIsSuccess) {
                return this;
            }

            for (ScheduledReminder reminder : reminders) {
                long id = mDatabase.insert(ReminderEntry.TABLE_NAME, null, reminder.toContentValuesOnCreate());
                if (id == -1) {
                    mIsSuccess = false;
                    mDatabase.endTransaction();
                }
            }

            return this;
        }

        /**
         * Update provided reminders using {@link ScheduledReminder#toContentValuesOnUpdate()} (i.e. updating timestamp
         * only)
         *
         * @param reminders scheduled reminders to update
         * @return success status
         */
        public Transaction updateReminders(ScheduledReminder[] reminders) {
            if (!mIsSuccess) {
                return this;
            }

            for (ScheduledReminder reminder : reminders) {
                int rowsAffected = mDatabase.update(
                        ReminderEntry.TABLE_NAME,
                        reminder.toContentValuesOnUpdate(),
                        BuildingBlocks.SELECTION_ID,
                        new String[]{Long.toString(reminder.id)}
                );
                if (rowsAffected != 1) {
                    mIsSuccess = false;
                    mDatabase.endTransaction();
                }
            }

            return this;
        }

        /**
         * Remove all scheduled reminders for challenge with provided ID. Regardless of affected rows, this method
         * cannot abort the transaction
         *
         * @param challengeId ID of the challenge to remove reminders for
         * @return this for chaining
         */
        public Transaction deleteRemindersForChallenge(long challengeId) {
            if (!mIsSuccess) {
                return this;
            }

            mDatabase.delete(
                    ReminderEntry.TABLE_NAME,
                    BuildingBlocks.SELECTION_CHALLENGE_ID,
                    new String[]{Long.toString(challengeId)}
            );

            return this;
        }

        /**
         * Delete snoozed reminders that have fireAt <= maxTimestamp
         *
         * @param maxTimestamp upper filter for past snoozed reminders
         * @return this for chaining
         */
        public Transaction cleanUpSnoozedReminders(long maxTimestamp) {
            if (!mIsSuccess) {
                return this;
            }

            mDatabase.delete(
                    ReminderEntry.TABLE_NAME,
                    BuildingBlocks.SELECTION_REMINDER_PAST_AND_SNOOZED,
                    new String[]{Long.toString(maxTimestamp)}
            );

            return this;
        }

    }

}
