package com.actinarium.persistence.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.PersistenceContract.BuildingBlocks;
import com.actinarium.persistence.data.PersistenceContract.ProgressEntry;

import static com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import static com.actinarium.persistence.data.PersistenceContract.PATH_TODAY;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class PersistenceContentProvider extends ContentProvider {

    private static UriMatcher sUriMatcher = buildUriMatcher();
    private static final int CHALLENGES = 0;
    private static final int CHALLENGES_TODAY = 1;
    private static final int CHALLENGE_PROGRESS = 2;

    private PersistenceDbHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = PersistenceDbHelper.getInstance(getContext());
        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case CHALLENGES:
            case CHALLENGES_TODAY:
                return ChallengeEntry.CONTENT_TYPE;
            case CHALLENGE_PROGRESS:
                return ProgressEntry.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Cannot resolve given URI: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        switch (sUriMatcher.match(uri)) {
            case CHALLENGES:
                final Cursor cursor = mDbHelper.getReadableDatabase().query(
                        ChallengeEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder
                );
                cursor.setNotificationUri(getContext().getContentResolver(), ChallengeEntry.CONTENT_URI);
                return cursor;
            case CHALLENGES_TODAY:
                return getDisplayedChallengesWithTodayRecords(projection, Utils.getCurrentEpochDay(getContext()));
            case CHALLENGE_PROGRESS:
                return getProgressForChallenge(projection, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Cannot resolve given URI: " + uri);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new IllegalArgumentException("Insert operations not supported");
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new IllegalArgumentException("Update operations not supported");
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        throw new IllegalArgumentException("Delete operations not supported");
    }

    /**
     * Creates a static matcher for this content provider
     *
     * @return Initialized URI matcher
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(
                PersistenceContract.CONTENT_AUTHORITY,
                PersistenceContract.PATH_CHALLENGES,
                CHALLENGES
        );
        uriMatcher.addURI(
                PersistenceContract.CONTENT_AUTHORITY,
                PersistenceContract.PATH_CHALLENGES + "/" + PATH_TODAY,
                CHALLENGES_TODAY
        );
        uriMatcher.addURI(
                PersistenceContract.CONTENT_AUTHORITY,
                PersistenceContract.PATH_PROGRESS,
                CHALLENGE_PROGRESS
        );
        return uriMatcher;
    }

    /**
     * This method will return all active challenges with TODAY records joined to them.
     * <b>Note!</b> This method DOES ensure database integrity, i.e. it creates missing progress entries for each
     * active challenge
     * @return cursor
     */
    private Cursor getDisplayedChallengesWithTodayRecords(String[] projection, int today) {
        // Since this is usually the entry point to the application, request integrity check
        Cursor cursor = PersistenceDbOps.ensureIntegrity(mDbHelper.getWritableDatabase(), today, projection);

        // If no modification required, the check returns the cursor that can be reused; otherwise we must query again
        // Also notify that progress has changed
        final ContentResolver contentResolver = getContext().getContentResolver();
        if (cursor == null) {
            contentResolver.notifyChange(ProgressEntry.CONTENT_URI, null);

            cursor = BuildingBlocks.buildChallengesWithRecordsJoin(today).query(
                    mDbHelper.getReadableDatabase(),
                    projection,
                    BuildingBlocks.SELECTION_DISPLAYED_CHALLENGES
                            + " AND (" + BuildingBlocks.SELECTION_PROGRESS_DAY_IS
                            + " OR " + BuildingBlocks.SELECTION_PROGRESS_DAY_ISNULL + ")",
                    new String[]{String.valueOf(today)},
                    null, null,
                    BuildingBlocks.SORT_ORDER_CHALLENGES
            );
        }
        cursor.setNotificationUri(contentResolver, ChallengeEntry.CONTENT_URI_EXTENDED);
        return cursor;
    }

    /**
     * This method will return aggregated percentage of completed/compensated goals for given day
     * @return cursor
     */
    private Cursor getProgressForChallenge(String[] projection, String selection, String[] selectionArgs) {
        final Cursor cursor = mDbHelper.getReadableDatabase().query(
                ProgressEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null, null,
                BuildingBlocks.SORT_ORDER_PROGRESS
        );
        cursor.setNotificationUri(getContext().getContentResolver(), ProgressEntry.CONTENT_URI);
        return cursor;
    }
}
