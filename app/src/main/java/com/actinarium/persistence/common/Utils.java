package com.actinarium.persistence.common;

import android.content.Context;
import android.os.Build;
import android.support.annotation.IntDef;
import android.text.format.DateFormat;
import android.util.Log;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Challenge.ChallengeStatus;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.ui.settings.PrefUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public final class Utils {

    public static final int MINUTES_IN_24_H = 1440;
    public static final int MINUTES_IN_16_H = 960;
    public static final int EPOCH_DAYS_BEFORE_JAN_1_2015 = 16436;
    public static final int EPOCH_DAYS_BEFORE_JAN_1_2016 = 16801;

    public static final int DATE_FORMAT_NO_WEEKDAY = 0;
    public static final int DATE_FORMAT_SHORT_WEEKDAY = 1;
    public static final int DATE_FORMAT_LONG_WEEKDAY = 2;

    public static final int DEFAULT_ICON_ALPHA = 138;
    private static final char EN_DASH = '\u2013';
    private static final char ONE_HALF = '\u00bd';
    private static final String[] WEEKDAYS = DateFormatSymbols.getInstance().getShortWeekdays();

    private static final NumberFormat DEBT_MULT_FORMAT = new DecimalFormat("#.##x");

    private Utils() {
        throw new AssertionError();
    }

    /**
     * Get number of given day since epoch (Jan 1, 1970 is the 1st day)
     *
     * @param calendar Calendar to determine day for
     * @return Day since epoch
     */
    public static int getEpochDay(Calendar calendar) {
        return getEpochDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_YEAR));
    }

    public static int getEpochDay(int year, int dayOfYear) {
        if (year == 2016) {
            return EPOCH_DAYS_BEFORE_JAN_1_2016 + dayOfYear;
        } else if (year == 2015) {
            return EPOCH_DAYS_BEFORE_JAN_1_2015 + dayOfYear;
        } else {
            return (year - 1970) * 365
                    + (int) Math.floor((year - 1969) / 4.0)
                    - (int) Math.floor((year - 1901) / 100.0)
                    + (int) Math.floor((year - 1601) / 400.0)
                    + dayOfYear;
        }
    }

    /**
     * Get number of given day since epoch (Jan 1, 1970 is the 1st day) for a given moment, accounting for current
     * setting regarding day end
     *
     * @param milliseconds given moment in time
     * @param context      Context required to resolve the setting
     * @return Day since epoch
     */
    public static int getEpochDay(long milliseconds, Context context) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        int day = getEpochDay(calendar);
        // Check if it's not the new day yet
        int newDayOffset = PrefUtils.getNewDayOffset(context);
        if (getMinuteOfDay(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)) < newDayOffset) {
            day--;
        }
        return day;
    }

    /**
     * Get integer (number of day since epoch) for current day with respect to day change time setting
     *
     * @param context Context required to resolve the setting
     * @return Current day (Jan 1, 1970 = 1st day)
     */
    public static int getCurrentEpochDay(Context context) {
        return getEpochDay(System.currentTimeMillis(), context);
    }

    /**
     * Get calendar for day corresponding to given epoch day
     *
     * @param epochDay
     * @return
     */
    public static Calendar createDayFromEpoch(int epochDay) {
        // Get approximate number of full years passed
        int year = (int) Math.floor((epochDay - 1) / 365.2425) + 1970;
        // When does the next year start? Because of leap years and one extra day, the above statement may fail for Dec 31 / Jan 1
        int nextYearStartDay = getEpochDay(year + 1, 1);
        Calendar calendar = new GregorianCalendar(year, Calendar.JANUARY, 1, 0, 0, 0);
        if (epochDay < nextYearStartDay) {
            calendar.set(Calendar.YEAR, year);
            int thisYearStartDay = getEpochDay(year, 1);
            calendar.set(Calendar.DAY_OF_YEAR, epochDay - thisYearStartDay + 1);
        } else {
            calendar.set(Calendar.YEAR, year + 1);
            calendar.set(Calendar.DAY_OF_YEAR, epochDay - nextYearStartDay + 1);
        }
        return calendar;
    }

    /**
     * Get calendar corresponding to the minuteOfDay at [today+offset]
     *
     * @param minuteOfDay Minute of day to get time for
     * @param dayOffset   e.g. 0 for today, 1 for tomorrow
     * @return
     */
    public static Calendar getTimeAt(int minuteOfDay, int dayOffset) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, extractHour(minuteOfDay));
        calendar.set(Calendar.MINUTE, extractMinute(minuteOfDay));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DATE, dayOffset);
        return calendar;
    }

    /**
     * Get day of week corresponding to given epoch day
     *
     * @param epochDay Number of given day since epoch (Jan 1, 1970 is the 1st day)
     * @return Week day <b>1 POSITION LESS</b> than as per Calendar.DAY_OF_WEEK (i.e. SUNDAY = 0, SATURDAY = 6)
     */
    public static int getDayOfWeek(int epochDay) {
        // 1st day of epoch (Jan 1, 1970) was Thursday
        return (epochDay + 3) % 7;
    }

    public static int getMinuteOfDay(int hour, int minute) {
        return hour * 60 + minute;
    }

    public static int getCurrentMinuteOfDay() {
        Calendar calendar = GregorianCalendar.getInstance();
        return getMinuteOfDay(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    public static int getClosestHalfHour() {
        Calendar calendar = GregorianCalendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        if (minute >= 30) {
            hour++;
            minute = 0;
        } else {
            minute = 30;
        }
        if (hour > 23) {
            hour = hour - 24;
        }
        return getMinuteOfDay(hour, minute);
    }

    public static String timeToString(int hour, int minute, Context context) {
        Calendar calendar = new GregorianCalendar(2015, 1, 1, hour, minute);
        return DateFormat.getTimeFormat(context).format(calendar.getTime());
    }

    public static int extractHour(int minuteOfDay) {
        final int hour = minuteOfDay / 60;
        return minuteOfDay < 0 ? hour + 23 : hour;
    }

    public static int extractMinute(int minuteOfDay) {
        final int minute = minuteOfDay % 60;
        return minuteOfDay < 0 ? minute + 60 : minute;
    }

    public static String prettyPrintRule(Rule rule, Context context) {
        if (rule.isTemporary) {
            if (rule.startDay.equals(rule.endDay)) {
                // Simply return "on Thu, Aug 18"
                return context.getString(R.string.rule_on, prettyPrintDate(createDayFromEpoch(rule.startDay), DATE_FORMAT_SHORT_WEEKDAY));
            } else {
                int activeMask = getAvailableDaysBetween(rule.startDay, rule.endDay);
                if ((rule.weekdayFlags & activeMask) == activeMask) {
                    // All possible days selected. Simply return "from Thu, Aug 18 to Fri, Aug 31" then
                    return context.getString(
                            R.string.rule_from_to,
                            prettyPrintDate(createDayFromEpoch(rule.startDay), DATE_FORMAT_SHORT_WEEKDAY),
                            prettyPrintDate(createDayFromEpoch(rule.endDay), DATE_FORMAT_SHORT_WEEKDAY)
                    );
                } else if (rule.endDay - rule.startDay >= 7) {
                    // Range is more than one week - so certain days may repeat
                    // todo: and may not - if too much of a concern, improve this method (i.e. move start/end days to match flags)

                    return context.getString(
                            R.string.rule_every,
                            buildDayExpression(unwrapDays(rule.weekdayFlags), context)
                    ) + " " + context.getString(
                            R.string.rule_from_to,
                            prettyPrintDate(createDayFromEpoch(rule.startDay), DATE_FORMAT_NO_WEEKDAY),
                            prettyPrintDate(createDayFromEpoch(rule.endDay), DATE_FORMAT_NO_WEEKDAY)
                    );
                } else {
                    // Range is 7 days or less - so it's appropriate to use "on" instead of "every", e.g. "on Mon and Tue between..."
                    return context.getString(
                            R.string.rule_on,
                            buildDayExpression(unwrapDays(rule.weekdayFlags), context)
                    ) + " " + context.getString(
                            R.string.rule_between_and,
                            prettyPrintDate(createDayFromEpoch(rule.startDay), DATE_FORMAT_NO_WEEKDAY),
                            prettyPrintDate(createDayFromEpoch(rule.endDay), DATE_FORMAT_NO_WEEKDAY)
                    );
                }
            }
        } else if (rule.weekdayFlags != Rule.FLAG_ALL) {
            // It will never happen that weekdayFlags == 0 (unless DB tampered with) - the form won't let the app save it
            boolean[] days = unwrapDays(rule.weekdayFlags);
            return context.getString(R.string.rule_every, buildDayExpression(days, context));
        } else {
            return context.getString(R.string.rule_every_day);
        }
    }

    /**
     * Builds expression for days, e.g. "Sun, Mon-Wed, and Fri".<p/> Rules:<ul> <li>Days are enumerated optionally with
     * Oxford comma, i.e. Sun, Mon, and Tue</li> <li>If there's three or more consecutive days, they are grouped with en
     * dash, e.g. Mon-Wed</li> <li>If there's both Saturday and Sunday selected, they wrap into one group in the
     * end</li> </ul> This method is pretty logic-heavy, so it's expected that at least one day is selected.
     *
     * @param days
     * @param context
     * @return
     */
    private static String buildDayExpression(boolean[] days, Context context) {
        // Interleaved values for group start and end (max 4 groups possible)
        int[] groups = new int[8];
        // Switch: if previous day is selected
        boolean prev = false;
        // First day will forward-track until the first gap is found, starting from the day before week start (e.g. Sat for en_US)
        int firstDay = GregorianCalendar.getInstance().getFirstDayOfWeek() + 5;
        int lastDay = firstDay + 7;
        while (days[firstDay % 7] && firstDay < lastDay) {
            firstDay++;
        }
        if (firstDay == lastDay) {
            Log.w("persistence.Utils", "Unexpected code branch: buildDayExpression was provided with ALL_DAYS flags");
            return context.getString(R.string.rule_every_day);
        }

        // Now when we determined where the first gap that interests us is, we can walk the week and collect groups
        int index = 0;
        firstDay++;
        lastDay = firstDay + 7;
        for (int i = firstDay; i < lastDay; i++) {
            final int day = i % 7;
            if (!prev && days[day]) {
                // Start of the group
                groups[index++] = day;
                prev = true;
            } else if (prev && !days[day]) {
                // End of the group, exclusive. Now, if last group had only two elements (i.e. day - 2 is not selected), save as two separate groups
                final int oneDayAgo = (i - 1) % 7;
                final int twoDaysAgo = (i - 2) % 7;
                final int threeDaysAgo = (i - 3) % 7;
                if (days[twoDaysAgo] && !days[threeDaysAgo]) {
                    // Last group spanned exactly for two days - register as two separate groups
                    // If previous day was not yet traversed, end previous group
                    groups[index++] = twoDaysAgo;
                    groups[index++] = oneDayAgo;
                    groups[index++] = oneDayAgo;
                } else {
                    // Last group spanned exactly for one, or more than for 2 days in the row
                    groups[index++] = oneDayAgo;
                }
                prev = false;
            }
        }

        // Now there are three options: 1) there's only one group; 2) there are two groups (never using comma); 3) there
        // are three groups with/without Oxford comma
        switch (index) {
            case 2:
                return buildGroup(groups[0], groups[1]);
            case 4:
                return context.getString(
                        R.string.day_expr_two,
                        buildGroup(groups[0], groups[1]),
                        buildGroup(groups[2], groups[3])
                );
            case 6:
                return context.getString(
                        R.string.day_expr_three,
                        buildGroup(groups[0], groups[1]),
                        buildGroup(groups[2], groups[3]),
                        buildGroup(groups[4], groups[5])
                );
            case 8:
                return context.getString(
                        R.string.day_expr_four,
                        buildGroup(groups[0], groups[1]),
                        buildGroup(groups[2], groups[3]),
                        buildGroup(groups[4], groups[5]),
                        buildGroup(groups[6], groups[7])
                );
            default:
                throw new RuntimeException("Something went wrong with calculating pretty print for given rule");
        }
    }

    private static String buildGroup(int dayStart, int dayEnd) {
        if (dayStart == dayEnd) {
            return WEEKDAYS[dayStart + 1];
        } else {
            return WEEKDAYS[dayStart + 1] + EN_DASH + WEEKDAYS[dayEnd + 1];
        }
    }

    public static boolean[] unwrapDays(int flags) {
        return new boolean[]{
                (flags & Rule.FLAG_SUNDAY) != 0,
                (flags & Rule.FLAG_MONDAY) != 0,
                (flags & Rule.FLAG_TUESDAY) != 0,
                (flags & Rule.FLAG_WEDNESDAY) != 0,
                (flags & Rule.FLAG_THURSDAY) != 0,
                (flags & Rule.FLAG_FRIDAY) != 0,
                (flags & Rule.FLAG_SATURDAY) != 0
        };
    }

    public static String prettyPrintDate(Calendar calendar, @PrettyPrintDateFormat int format) {
        // Show or omit years depending on whether the date is in current year
        StringBuilder builder = new StringBuilder(16);
        if (format == DATE_FORMAT_LONG_WEEKDAY) {
            builder.append("EEEE, ");
        } else if (format == DATE_FORMAT_SHORT_WEEKDAY) {
            builder.append("EEE, ");
        }
        builder.append("MMM d");
        if (calendar.get(Calendar.YEAR) != GregorianCalendar.getInstance().get(Calendar.YEAR)) {
            builder.append(", yyyy");
        }
        // Generate best format based on recommendation skeleton, if device supports it
        String bestFormatString;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bestFormatString = DateFormat.getBestDateTimePattern(Locale.getDefault(), builder.toString());
        } else {
            bestFormatString = builder.toString();
        }
        return DateFormat.format(bestFormatString, calendar).toString();
    }

    /**
     * Returns flags for weekdays available between date 1 and date 2
     *
     * @param date1 Epoch day "from"
     * @param date2 Epoch day "to" (inclusive)
     * @return bitmask
     */
    public static int getAvailableDaysBetween(int date1, int date2) {
        if (date2 - date1 >= 6) {
            return Rule.FLAG_ALL;
        } else if (date2 == date1) {
            return 1 << getDayOfWeek(date1);
        } else {
            int day1 = getDayOfWeek(date1);
            int day2 = getDayOfWeek(date2);
            if (day2 < day1) {
                day2 = day2 + 7;
            }
            int mask = 0;
            for (int i = day1; i <= day2; i++) {
                mask += 1 << (i % 7);
            }
            return mask;
        }
    }

    public static String getDurationAgoString(Context context, int daysAgo) {
        if (daysAgo == 0) {
            return context.getResources().getString(R.string.today);
        } else if (daysAgo == 1) {
            return context.getResources().getString(R.string.yesterday);
        } else if (daysAgo < 7) {
            return context.getResources().getQuantityString(R.plurals.x_days_ago, daysAgo, daysAgo);
        } else {
            final int weeks = (daysAgo + 2) / 7;
            if (weeks == 1) {
                return context.getResources().getString(R.string.a_week_ago);
            } else if (weeks < 7) {
                return context.getResources().getQuantityString(R.plurals.x_weeks_ago, weeks, weeks);
            } else {
                return context.getString(R.string.long_ago);
            }
        }
    }

    public static
    @ChallengeStatus
    int getStatus(int sparkToday, int spark1dAgo, int spark2dAgo) {
        if (spark1dAgo > spark2dAgo) {
            return sparkToday > spark1dAgo ? Challenge.STATUS_IN_THE_ZONE :
                    sparkToday < spark1dAgo ? Challenge.STATUS_HICCUPS : Challenge.STATUS_ON_TRACK;
        } else if (spark1dAgo < spark2dAgo) {
            return sparkToday < spark1dAgo ? Challenge.STATUS_NOT_GOOD : Challenge.STATUS_BACK_ON_TRACK;
        } else {
            return spark1dAgo > sparkToday ? Challenge.STATUS_HICCUPS : Challenge.STATUS_ON_TRACK;
        }
    }

    public static String getStatusText(Context context, @ChallengeStatus int status) {
        switch (status) {
            case Challenge.STATUS_ON_TRACK:
                return context.getString(R.string.status_on_track);
            case Challenge.STATUS_BACK_ON_TRACK:
                return context.getString(R.string.status_back_on_track);
            case Challenge.STATUS_IN_THE_ZONE:
                return context.getString(R.string.status_in_the_zone);
            case Challenge.STATUS_HICCUPS:
                return context.getString(R.string.status_hiccups);
            case Challenge.STATUS_NOT_GOOD:
                return context.getString(R.string.status_not_good);
            case Challenge.STATUS_CRITICAL:
                return context.getString(R.string.status_critical);
            default:
                return null;
        }
    }

    public static String formatDebtMultiplier(int debtMultiplier) {
        return DEBT_MULT_FORMAT.format(debtMultiplier / 100.0);
    }

    public static String formatReserve(int doubleReserveValue) {
        if (doubleReserveValue % 2 == 0) {
            return String.valueOf(doubleReserveValue / 2);
        } else if (doubleReserveValue != 1) {
            return String.valueOf(doubleReserveValue / 2) + ONE_HALF;
        } else {
            return String.valueOf(ONE_HALF);
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({DATE_FORMAT_NO_WEEKDAY, DATE_FORMAT_LONG_WEEKDAY, DATE_FORMAT_SHORT_WEEKDAY})
    @interface PrettyPrintDateFormat {
    }
}
