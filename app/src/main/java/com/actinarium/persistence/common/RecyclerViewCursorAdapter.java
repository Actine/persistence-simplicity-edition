package com.actinarium.persistence.common;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.widget.CursorAdapter;

/**
 * {@link CursorAdapter} meets {@link RecyclerView.Adapter}
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public abstract class RecyclerViewCursorAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected Context mContext;
    protected Cursor mCursor;
    protected boolean mDataValid;

    public RecyclerViewCursorAdapter(Context context, Cursor cursor) {
        final boolean cursorPresent = cursor != null;
        mContext = context;
        mCursor = cursor;
        mDataValid = cursorPresent;
    }

    protected Context getContext() {
        return this.mContext;
    }

    protected Cursor getCursor() {
        return this.mCursor;
    }

    /**
     * Change the underlying cursor to a new cursor. If there is an existing cursor it will be
     * closed.
     *
     * @param cursor The new cursor to be used
     */
    public void changeCursor(Cursor cursor) {
        Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    /**
     * Swap in a new Cursor, returning the old Cursor.  Unlike
     * {@link #changeCursor(Cursor)}, the returned old Cursor is <em>not</em>
     * closed.
     *
     * @param newCursor The new cursor to be used.
     * @return Returns the previously set Cursor, or null if there wasa not one.
     * If the given new Cursor is the same instance is the previously set
     * Cursor, null is also returned.
     */
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        mCursor = newCursor;
        if (newCursor != null) {
            mDataValid = true;
            notifyDataSetChanged();
        } else {
            mDataValid = false;
            notifyDataSetChanged();
        }
        return oldCursor;
    }
}
