package com.actinarium.persistence.common;

import android.database.Cursor;
import android.provider.BaseColumns;

/**
 * Convenience util class to read data from cursor
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public final class CursorReaderUtils {

    private CursorReaderUtils() {}

    public static byte[] getBlob(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.getBlob(index);
        }
    }

    public static String getString(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.getString(index);
        }
    }

    public static int getInt(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return 0;
        } else {
            return cursor.getInt(index);
        }
    }

    public static Integer getNullableInt(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.isNull(index) ? null : cursor.getInt(index);
        }
    }

    public static long getId(Cursor cursor, String tableName) {
        // Workaround for Android bug 903852
        int index = cursor.getColumnIndex(tableName + "_" + BaseColumns._ID);
        if (index == -1) {
            index = cursor.getColumnIndex(BaseColumns._ID);
            if (index == -1) {
                return -1;
            }
        }
        return cursor.getLong(index);
    }

    public static long getLong(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return 0;
        } else {
            return cursor.getLong(index);
        }
    }

    public static Long getNullableLong(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.isNull(index) ? null : cursor.getLong(index);
        }
    }

    public static float getFloat(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return 0;
        } else {
            return cursor.getFloat(index);
        }
    }

    public static Float getNullableFloat(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.isNull(index) ? null : cursor.getFloat(index);
        }
    }

    public static double getDouble(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return 0;
        } else {
            return cursor.getDouble(index);
        }
    }

    public static Double getNullableDouble(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if (index == -1) {
            return null;
        } else {
            return cursor.isNull(index) ? null : cursor.getDouble(index);
        }
    }
}
