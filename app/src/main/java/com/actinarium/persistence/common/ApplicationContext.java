package com.actinarium.persistence.common;

import android.support.annotation.Nullable;
import com.actinarium.persistence.dto.Challenge;

/**
 * A singleton class holding various temporary application data
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ApplicationContext {

    private static ApplicationContext sInstance;

    public static ApplicationContext get() {
        if (sInstance == null) {
            sInstance = new ApplicationContext();
        }
        return sInstance;
    }

    private int mId = 0;

    private boolean mEmptyChallengeDiscardedStoredStatus;
    private EmptyChallengeDiscardedListener mDiscardedListener;

    private boolean mChallengeSavedStoredStatus;
    private Challenge mChallengeSavedStored;
    private ChallengeSavedListener mSavedListener;

    private ApplicationContext() {

    }

    synchronized public int getNextId() {
        return mId++;
    }

    public void setEmptyChallengeDiscarded() {
        if (mDiscardedListener != null) {
            mDiscardedListener.onEmptyChallengeDiscarded();
        } else {
            mEmptyChallengeDiscardedStoredStatus = true;
        }
    }

    public void setEmptyChallengeDiscardedListener(@Nullable EmptyChallengeDiscardedListener listener) {
        mDiscardedListener = listener;
        if (mDiscardedListener != null && mEmptyChallengeDiscardedStoredStatus) {
            mEmptyChallengeDiscardedStoredStatus = false;
            mDiscardedListener.onEmptyChallengeDiscarded();
        }
    }

    public void setChallengeSaved(Challenge challenge) {
        if (mSavedListener != null) {
            mSavedListener.onChallengeSaved(challenge);
        } else {
            mChallengeSavedStoredStatus = true;
            mChallengeSavedStored = challenge;
        }
    }

    public void setChallengeSavedListener(@Nullable ChallengeSavedListener listener) {
        mSavedListener = listener;
        if (mSavedListener != null && mChallengeSavedStoredStatus) {
            mChallengeSavedStoredStatus = false;
            mSavedListener.onChallengeSaved(mChallengeSavedStored);
        }
    }

    public interface EmptyChallengeDiscardedListener {
        void onEmptyChallengeDiscarded();
    }

    public interface ChallengeSavedListener {
        void onChallengeSaved(Challenge challenge);
    }

}
