package com.actinarium.persistence.common;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public interface BackInterceptor {
    /**
     * Intercept back button press if required
     * @return true if back button press was consumed, false otherwise
     */
    boolean onBackPressed();
}
