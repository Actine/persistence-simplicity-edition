package com.actinarium.persistence.ui.setupchallenge.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ReminderTypeSpinnerAdapter extends ArrayAdapter<String> {

    private String[] mLongDescriptions;
    private LayoutInflater mInflater;

    public ReminderTypeSpinnerAdapter(Context context) {
        super(
                context,
                android.R.layout.simple_spinner_item,
                context.getResources().getStringArray(R.array.reminder_types_short)
        );
        mLongDescriptions = context.getResources().getStringArray(R.array.reminder_types_long);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view;

        if (convertView == null) {
            view = (TextView) mInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        } else {
            view = (TextView) convertView;
        }

        view.setText(mLongDescriptions[position]);
        return view;
    }
}
