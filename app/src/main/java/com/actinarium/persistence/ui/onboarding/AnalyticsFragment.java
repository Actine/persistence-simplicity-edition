package com.actinarium.persistence.ui.onboarding;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.actinarium.aligned.TextView;
import com.actinarium.aligned.Utils;
import com.actinarium.persistence.BuildConfig;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.settings.PrefUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnalyticsFragment extends Fragment {

    public static final String TAG = "AboutPersistenceFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ob_analytics, container, false);

        final Context context = getContext();

        final CheckBox allowAnalyticsCheckbox = (CheckBox) view.findViewById(R.id.allow_analytics);
        final int i4dp = getResources().getDimensionPixelSize(R.dimen.i4dips);
        Utils.setExactMetrics(allowAnalyticsCheckbox, i4dp * 7, i4dp * 5, i4dp * 4);

        final TextView analyticsText3 = (TextView) view.findViewById(R.id.analytics_text_3);
        analyticsText3.setText(ViewUtils.hyperlinkifyText(getText(R.string.ob_analytics_text_3), new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.PRIVACY_POLICY_URL)));
            }
        }));
        analyticsText3.setMovementMethod(LinkMovementMethod.getInstance());


        allowAnalyticsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PrefUtils.setAnalyticsAllowed(context, isChecked);
            }
        });

        if (savedInstanceState == null) {
            // Fill the checkbox with initial values
            final boolean allowed = PrefUtils.isAnalyticsAllowed(context);
            allowAnalyticsCheckbox.setChecked(allowed);
        }

        return view;
    }

}
