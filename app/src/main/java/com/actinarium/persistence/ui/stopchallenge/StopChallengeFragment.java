package com.actinarium.persistence.ui.stopchallenge;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.service.PersistenceService;
import com.actinarium.persistence.ui.common.PagerIndicator;
import com.actinarium.persistence.ui.common.RadioGroup;
import com.actinarium.persistence.ui.common.SwitchableButton;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.settings.PrefUtils;
import com.actinarium.persistence.ui.support.SupportActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class StopChallengeFragment extends Fragment {

    public static final String TAG = "StopChallengeFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    private static final String ARG_CURRENT_PAGE = "com.actinarium.persistence.intent.arg.CURRENT_PAGE";
    private static final String ARG_SELECTED_RADIO = "com.actinarium.persistence.intent.arg.SELECTED_RADIO";
    private static final String ARG_REASON = "com.actinarium.persistence.intent.arg.REASON";

    private static final int LAST_PAGE_INDEX = 2;
    private static final int RADIO_NOT_SELECTED = -1;

    private FrameLayout mPageHost;
    private Button mCancelBtn;
    private SwitchableButton mConfirmBtn;
    private Button mNextBtn;
    private PagerIndicator mIndicator;
    private int mCurrentPageIndex;
    private int[] mOptionIndices;
    private int mSelectedRadio;
    private String mReason;

    private static final Interpolator sInterpolator = new Interpolator() {
        public float getInterpolation(float t) {
            t -= 1.0f;
            return t * t * t * t * t + 1.0f;
        }
    };
    private View mCurrentPage;
    private Challenge mChallenge;
    private EditText mCustomOption;

    public static StopChallengeFragment newInstance(Challenge challenge) {
        StopChallengeFragment fragment = new StopChallengeFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(ARG_CHALLENGE, challenge);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stop_challenge, container, false);

        mChallenge = getArguments().getParcelable(ARG_CHALLENGE);
        if (savedInstanceState != null) {
            mCurrentPageIndex = savedInstanceState.getInt(ARG_CURRENT_PAGE, 0);
            mSelectedRadio = savedInstanceState.getInt(ARG_SELECTED_RADIO, RADIO_NOT_SELECTED);
            mReason = savedInstanceState.getString(ARG_REASON);
        } else {
            mCurrentPageIndex = 0;
            mSelectedRadio = RADIO_NOT_SELECTED;
        }

        // Get controls
        mPageHost = (FrameLayout) view.findViewById(R.id.stop_challenge_page_host);
        mCancelBtn = (Button) view.findViewById(R.id.cancel);
        mConfirmBtn = (SwitchableButton) view.findViewById(R.id.confirm);
        mNextBtn = (Button) view.findViewById(R.id.next);
        mIndicator = (PagerIndicator) view.findViewById(R.id.pager_indicator);

        // Display current page
        mCurrentPage = prepareViewForPage(inflater, mPageHost, mCurrentPageIndex, mChallenge);
        mPageHost.addView(mCurrentPage);

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveForward(inflater);
            }
        });

        mConfirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedRadio < mOptionIndices.length) {
                    // One of pre-defined reasons, store simply as int
                    mReason = Integer.toString(mOptionIndices[mSelectedRadio]);
                } else {
                    // Custom reason
                    String mReasonRaw = mCustomOption.getText().toString().trim();
                    if (mReasonRaw.isEmpty()) {
                        mReason = null;
                    } else {
                        mReason = "0|" + mReasonRaw;
                    }
                }

                mChallenge.setIsOverFlags();
                mChallenge.stoppedOn = Utils.getCurrentEpochDay(getContext());
                mChallenge.stopReason = mReason;
                PersistenceService.stopChallenge(getContext(), mChallenge);

                moveForward(inflater);
            }
        });
        mConfirmBtn.setOnDisabledClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), R.string.reason_invalid, Toast.LENGTH_SHORT).show();
            }
        });

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return view;
    }

    @SuppressWarnings("ConstantConditions")
    private void moveForward(LayoutInflater inflater) {
        if (mCurrentPageIndex != LAST_PAGE_INDEX) {
            final View previousPage = mCurrentPage;

            final int offset = previousPage.getWidth();
            previousPage.animate().translationX(-offset).setInterpolator(sInterpolator).setDuration(200).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPageHost.removeView(previousPage);
                }
            }).start();

            mCurrentPageIndex++;
            final View nextPage = prepareViewForPage(inflater, mPageHost, mCurrentPageIndex, mChallenge);
            mCurrentPage = nextPage;
            mPageHost.addView(nextPage);
            nextPage.setTranslationX(offset);
            nextPage.animate().translationX(0).setInterpolator(sInterpolator).setDuration(200).start();
        } else {
            // todo: maybe not finish but go somewhere (e.g. back to home)
            getActivity().finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_CURRENT_PAGE, mCurrentPageIndex);
        outState.putInt(ARG_SELECTED_RADIO, mSelectedRadio);
        outState.putString(ARG_REASON, mReason);
    }

    private View prepareViewForPage(LayoutInflater inflater, ViewGroup container, int page, Challenge challenge) {
        final View view;
        switch (page) {
            case 0:
                view = inflater.inflate(R.layout.fragment_stc_facts, container, false);
                ((TextView) view.findViewById(R.id.first_sentence)).setText(
                        ViewUtils.formatFromHtmlWithMediumFix(getString(R.string.stop_challenge_message_1, challenge.title))
                );
                ((TextView) view.findViewById(R.id.facts_paragraph)).setText(composeFactsParagraph(challenge));
                ((TextView) view.findViewById(R.id.challenge_reason)).setText(composeChallengeReason(challenge));
                return view;
            case 1:
                view = inflater.inflate(R.layout.fragment_stc_question, container, false);
                mConfirmBtn.setVisibility(View.VISIBLE);
                mNextBtn.setVisibility(View.GONE);
                mIndicator.setCurrentItem(1, true);
                setUpOptionsPage(view, inflater, challenge);
                return view;
            case 2:
                view = inflater.inflate(R.layout.fragment_stc_done, container, false);
                ((TextView) view.findViewById(R.id.stop_challenge_message)).setText(composeChallengeStoppedParagraph(challenge));

                final TextView donationMessage = (TextView) view.findViewById(R.id.stop_challenge_donate);
                CharSequence donationCall = composeDonationCall(challenge);
                if (donationCall != null) {
                    donationMessage.setText(donationCall);
                    donationMessage.setMovementMethod(LinkMovementMethod.getInstance());
                } else {
                    donationMessage.setVisibility(View.GONE);
                }

                mCancelBtn.setVisibility(View.INVISIBLE);
                mConfirmBtn.setVisibility(View.GONE);
                mNextBtn.setVisibility(View.VISIBLE);
                mNextBtn.setText(R.string.done);
                mIndicator.setCurrentItem(2, false);
                return view;
            default:
                return null;
        }
    }

    @SuppressWarnings("ResourceType")
    private void setUpOptionsPage(View rootView, LayoutInflater inflater, Challenge challenge) {
        String[] options = getResources().getStringArray(R.array.stop_challenge_reasons);

        GridLayout optionsLayout = (GridLayout) rootView.findViewById(R.id.options);

        final int startedDaysAgo = Utils.getCurrentEpochDay(getContext()) - challenge.startedOn;
        if (startedDaysAgo < 4) {
            // Just started a few days ago
            mOptionIndices = new int[]{4, 2, 7};
        } else if (challenge.status < 3) {
            // Status not so bad
            mOptionIndices = new int[]{1, 2, 3, 5, 6};
        } else {
            // Status kinda bad
            mOptionIndices = new int[]{3, 5, 6, 2, 1};
        }

        final int length = mOptionIndices.length;
        RadioGroup group = new RadioGroup(length + 1);

        // First the options
        for (int optionIndex : mOptionIndices) {
            RadioButton button = (RadioButton) inflater.inflate(R.layout.grid_radio_button, optionsLayout, false);
            TextView label = (TextView) inflater.inflate(R.layout.grid_radio_label, optionsLayout, false);
            if (Build.VERSION.SDK_INT >= 17) {
                int id = View.generateViewId();
                button.setId(id);
                label.setLabelFor(id);
            }
            label.setText(options[optionIndex]);
            group.addRadioButton(button, label);
            optionsLayout.addView(button);
            optionsLayout.addView(label);
        }

        // Now the last option
        RadioButton button = (RadioButton) inflater.inflate(R.layout.grid_radio_button, optionsLayout, false);
        TextView label = (TextView) inflater.inflate(R.layout.grid_radio_label, optionsLayout, false);
        final int otherOptionId;
        if (Build.VERSION.SDK_INT >= 17) {
            otherOptionId = View.generateViewId();
            label.setLabelFor(otherOptionId);
        } else {
            otherOptionId = 0xFF00;
        }
        button.setId(otherOptionId);
        label.setText(options[0]);
        group.addRadioButton(button, label);
        optionsLayout.addView(button);
        optionsLayout.addView(label);

        // Also don't forget the other option text
        mCustomOption = (EditText) inflater.inflate(R.layout.grid_other_option, optionsLayout, false);
        mCustomOption.setMaxLines(Integer.MAX_VALUE);
        mCustomOption.setHorizontallyScrolling(false);
        optionsLayout.addView(mCustomOption);

        group.setOnChangeListener(new RadioGroup.OnChangeListener() {
            @Override
            public void onSelectionChanged(int viewId, int index) {
                mSelectedRadio = index;
                if (!mConfirmBtn.isEnabled()) {
                    mConfirmBtn.setEnabled(true);
                    mIndicator.setCurrentItem(1, false);
                }
                if (viewId == otherOptionId) {
                    // other group selected
                    mCustomOption.setVisibility(View.VISIBLE);
                } else {
                    mCustomOption.setVisibility(View.GONE);
                }
            }
        });

        if (mSelectedRadio != RADIO_NOT_SELECTED) {
            group.selectRadio(mSelectedRadio);
        }
    }

    /**
     * Compose the first paragraph of text based on challenge metrics
     * @param challenge Challenge to get data from
     * @return
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"}) // todo: remove when the method is completed
    private String composeFactsParagraph(Challenge challenge) {
        final Context context = getContext();

        final int totalGoal = 300;
        final int totalDone = 400;
        final boolean doingWell = challenge.status < 3;
        final boolean reserveAvailable = true;
        final int today = Utils.getCurrentEpochDay(context);

        StringBuilder builder = new StringBuilder();

        // Started...
        final int startedDaysAgo = today - challenge.startedOn;
        if (startedDaysAgo < 4) {
            // ...just today/yesterday...
            builder.append(getString(R.string.stop_challenge_message_2b, Utils.getDurationAgoString(context, startedDaysAgo))).append(' ');
            if (totalDone == 0) {
                // ...and did nothing so far.
                builder.append(getString(R.string.stop_challenge_message_2b1));
            } else if (doingWell) {
                // ...and doing quite well
                builder.append(getString(R.string.stop_challenge_message_2b3));
            } else {
                // ...and already accomplished something
                builder.append(getString(R.string.stop_challenge_message_2b2));
            }
        } else {
            // ...on xxx and persisted for yyy days
            builder.append(getString(
                    R.string.stop_challenge_message_2c,
                    Utils.prettyPrintDate(Utils.createDayFromEpoch(challenge.startedOn), Utils.DATE_FORMAT_NO_WEEKDAY),
                    getResources().getQuantityString(R.plurals.x_days, startedDaysAgo, startedDaysAgo)
            )).append(' ');
            if (totalDone == 0) {
                // You aimed to do xxx, but did nothing
                builder.append(getString(R.string.stop_challenge_message_2c1, totalGoal, challenge.units));
            } else {
                final float diff = totalDone / (float) totalGoal - 1;
                if (diff > -0.1 && diff < 0.04) {
                    // Logged xxx, which is just about expected
                    builder.append(getString(R.string.stop_challenge_message_2c3, totalDone, challenge.units));
                } else if (diff < 0) {
                    // Aimed to do xxx, but accomplished only yyy
                    builder.append(getString(R.string.stop_challenge_message_2c2, totalGoal, challenge.units, totalDone));
                } else {
                    // Logged xxx, which is yyy% more than aimed
                    int percent = Math.round(diff * 100);
                    builder.append(getString(R.string.stop_challenge_message_2c4, totalDone, challenge.units, percent));
                }
            }
        }

        builder.append(' ');

        if (challenge.areRulesEditableOn(today)) {
            if (reserveAvailable) {
                builder.append(getString(R.string.stop_challenge_message_3_11));
            } else {
                builder.append(getString(R.string.stop_challenge_message_3_01));
            }
        } else {
            final int editableInDays = challenge.getEditableSince() - today;
            String s = getResources().getQuantityString(R.plurals.in_x_days, editableInDays, editableInDays);
            if (reserveAvailable) {
                builder.append(getString(R.string.stop_challenge_message_3_10, s));
            } else {
                builder.append(getString(R.string.stop_challenge_message_3_00, s));
            }
        }

        return builder.toString();
    }

    private CharSequence composeChallengeReason(Challenge challenge) {
        if (challenge.description != null) {
            return new SpannableStringBuilder()
                    .append(getString(R.string.stop_challenge_message_4))
                    .append(' ')
                    .append(ViewUtils.formatFromHtmlWithMediumFix(getString(R.string.stop_challenge_message_4opt, challenge.description)));
        } else {
            return getString(R.string.stop_challenge_message_4);
        }
    }

    private CharSequence composeChallengeStoppedParagraph(Challenge challenge) {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder
                .append(ViewUtils.formatFromHtmlWithMediumFix(getString(R.string.challenge_stopped_message_1, challenge.title)))
                .append(' ');

        if ("1".equals(mReason)) {
            // Good luck with your next...
            builder.append(getString(R.string.challenge_stopped_message_2a));
        } else {
            // Should you change your mind...
            builder.append(getString(R.string.challenge_stopped_message_2b));
        }

        return builder;
    }

    private CharSequence composeDonationCall(Challenge challenge) {
        boolean alreadyDonated = PrefUtils.hasDonated(getContext());

        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                widget.playSoundEffect(SoundEffectConstants.CLICK);
                SupportActivity.launch(getContext());
            }
        };

        if (alreadyDonated) {
            if (mReason == null || (!mReason.startsWith("0|") && !"1".equals(mReason))) {
                return ViewUtils.hyperlinkifyText(getText(R.string.challenge_stopped_donate_fail_returning), span);
            } else {
                return null;
            }
        } else {
            if (mReason != null && (mReason.startsWith("0|") || "1".equals(mReason))) {
                return ViewUtils.hyperlinkifyText(getText(R.string.challenge_stopped_donate_success_1st_time), span);
            } else {
                return ViewUtils.hyperlinkifyText(getText(R.string.challenge_stopped_donate_fail_1st_time), span);
            }
        }
    }
}
