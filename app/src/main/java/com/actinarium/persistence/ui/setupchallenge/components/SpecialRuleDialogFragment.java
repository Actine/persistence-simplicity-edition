package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Rule;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class SpecialRuleDialogFragment extends DialogFragment {

    public static final String TAG = "SpecialRuleDialogFragment";

    private static final String ARG_RULE = "com.actinarium.persistence.intent.arg.RULE";
    private static final String ARG_IS_EDIT = "com.actinarium.persistence.intent.arg.IS_EDIT";

    private static final int DATE_PICKER_FROM_ID = 1;

    private static final int BTN_ANIM_DURATION = 350;
    private static final int BTN_ANIM_DELAY = 50;
    private static final Interpolator BTN_ANIM_INTERPOLATOR = new FastOutSlowInInterpolator();

    private Host mHost;
    private Context mContext;

    private Rule mRule;
    private Calendar mDateFrom;
    private Calendar mDateTo;
    private int mEnabledDays = Rule.FLAG_ALL;
    private Button[] mDayButtons;

    private Button mDateFromBtn;
    private Button mDateToBtn;
    private TextView mDateToText;
    private RadioButton mRadio1;
    private RadioButton mRadio2;
    private Button mOkButton;

    public static SpecialRuleDialogFragment newInstance(Rule rule, boolean isEdit) {
        SpecialRuleDialogFragment fragment = new SpecialRuleDialogFragment();
        Bundle args = new Bundle(2);
        args.putParcelable(ARG_RULE, rule);
        args.putBoolean(ARG_IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_RULE, mRule);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean isEdit = isEdit();

        mRule = getRule(savedInstanceState);
        if (mRule.startDay == null) {
            mRule.startDay = Utils.getCurrentEpochDay(mContext);
        }
        if (mRule.endDay == null) {
            mRule.endDay = Utils.getCurrentEpochDay(mContext);
        }
        mDateFrom = Utils.createDayFromEpoch(mRule.startDay);
        mDateTo = Utils.createDayFromEpoch(mRule.endDay);

        final View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_special_rule, null);
        mDateFromBtn = (Button) view.findViewById(R.id.rule_temporary_from);
        mDateToBtn = (Button) view.findViewById(R.id.rule_temporary_to);
        mDateToText = (TextView) view.findViewById(R.id.rule_temporary_to_text);
        mDateFromBtn.setText(Utils.prettyPrintDate(mDateFrom, Utils.DATE_FORMAT_NO_WEEKDAY));
        mDateToBtn.setText(Utils.prettyPrintDate(mDateTo, Utils.DATE_FORMAT_NO_WEEKDAY));

        mDateFromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialogFragment datePicker = DatePickerDialogFragment.newInstance(1);
                datePicker.show(getFragmentManager(), DatePickerDialogFragment.TAG);
            }
        });
        mDateToBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialogFragment datePicker = DatePickerDialogFragment.newInstance(2);
                datePicker.show(getFragmentManager(), DatePickerDialogFragment.TAG);
            }
        });

        // bind weekday buttons
        mDayButtons = new Button[7];
        String[] weekdays = DateFormatSymbols.getInstance().getShortWeekdays();
        int firstDay = GregorianCalendar.getInstance().getFirstDayOfWeek();
        LinearLayout daysRowOne = (LinearLayout) view.findViewById(R.id.days_row_one);
        LinearLayout daysRowTwo = (LinearLayout) view.findViewById(R.id.days_row_two);
        int day;
        if (daysRowTwo != null) {
            // Render 4 buttons in the first row and 3 buttons in the second
            for (int i = 0; i < 4; i++) {
                // Day is 0..6 where 0 is Sunday
                day = (firstDay + i + 6) % 7;
                Button button = makeDayButton(weekdays[day + 1], daysRowOne, 1 << day);
                button.animate().setDuration(BTN_ANIM_DURATION).setInterpolator(BTN_ANIM_INTERPOLATOR).setStartDelay(BTN_ANIM_DELAY * i);
                mDayButtons[i] = button;
                daysRowOne.addView(button);
            }
            for (int i = 4; i < 7; i++) {
                day = (firstDay + i + 6) % 7;
                Button button = makeDayButton(weekdays[day + 1], daysRowTwo, 1 << day);
                button.animate().setDuration(BTN_ANIM_DURATION).setInterpolator(BTN_ANIM_INTERPOLATOR).setStartDelay(BTN_ANIM_DELAY * (i - 3));
                mDayButtons[i] = button;
                daysRowTwo.addView(button);
            }
        } else {
            // Render all 7 buttons in the single row
            for (int i = 0; i < 7; i++) {
                day = (firstDay + i + 6) % 7;
                Button button = makeDayButton(weekdays[day + 1], daysRowOne, 1 << day);
                button.animate().setDuration(BTN_ANIM_DURATION).setInterpolator(BTN_ANIM_INTERPOLATOR).setStartDelay(BTN_ANIM_DELAY * i);
                mDayButtons[i] = button;
                daysRowOne.addView(button);
            }
        }

        // Bind radios
        mRadio1 = (RadioButton) view.findViewById(R.id.rule_every_week);
        mRadio2 = (RadioButton) view.findViewById(R.id.rule_temporary);
        mRadio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRule.isTemporary = false;
                    mRadio2.setChecked(false);
                    mDateFromBtn.setVisibility(View.INVISIBLE);
                    mDateToBtn.setVisibility(View.GONE);
                    mDateToText.setVisibility(View.GONE);
                    onFormUpdate(false);
                }
            }
        });
        mRadio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRule.isTemporary = true;
                    mRadio1.setChecked(false);
                    mDateFromBtn.setVisibility(View.VISIBLE);
                    mDateToBtn.setVisibility(View.VISIBLE);
                    mDateToText.setVisibility(View.VISIBLE);
                    onFormUpdate(false);
                }
            }
        });

        if (mRule.isTemporary) {
            mRadio2.setChecked(true);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.Theme_Persistence_Dialog);
        builder
                .setTitle(isEdit ? R.string.edit_special_rule : R.string.add_rule)
                .setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // strip days data if it's not temporary
                        if (!mRule.isTemporary) {
                            mRule.startDay = null;
                            mRule.endDay = null;
                        }
                        // don't strip inactive days it won't hurt if they are still marked as selected.
                        // instead strip when building pretty print for the rule

                        // save new rule / save edits
                        if (isEdit) {
                            mHost.onRuleOverrideEdited(mRule);
                        } else {
                            mHost.onRuleOverrideAdded(mRule);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                mOkButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                onFormUpdate(true);
            }
        });
        return alertDialog;
    }

    public Calendar getCalendarForDatePicker(int id) {
        return (id == DATE_PICKER_FROM_ID) ? mDateFrom : mDateTo;
    }

    public void saveDatePickerResult(int id, Calendar calendar) {
        if (id == DATE_PICKER_FROM_ID) {
            mDateFrom = calendar;
            mRule.startDay = Utils.getEpochDay(calendar);
            mDateFromBtn.setText(Utils.prettyPrintDate(calendar, Utils.DATE_FORMAT_NO_WEEKDAY));
            // Update end day if start day is moved after end day
            if (mRule.startDay > mRule.endDay) {
                saveDatePickerResult(2, calendar);
            }
        } else {
            mDateTo = calendar;
            mRule.endDay = Utils.getEpochDay(calendar);
            mDateToBtn.setText(Utils.prettyPrintDate(calendar, Utils.DATE_FORMAT_NO_WEEKDAY));
            // Update start day if end day is moved before start day
            if (mRule.endDay < mRule.startDay) {
                saveDatePickerResult(1, calendar);
            }
        }
        refreshEnabledDays();
        onFormUpdate(false);
    }

    /**
     * Get rule for this dialog out of proper bundle (from instance state if present, otherwise from arguments)
     * @param savedInstanceState Instance state to extract rule from upon config change
     * @return Rule dto
     */
    private Rule getRule(Bundle savedInstanceState) {
        Rule rule;
        if (savedInstanceState != null) {
            rule = (Rule) savedInstanceState.getParcelable(ARG_RULE);
        } else {
            rule = (Rule) getArguments().getParcelable(ARG_RULE);
        }
        if (rule == null) {
            rule = new Rule();
        }
        return rule;
    }

    private boolean isEdit() {
        return getArguments().getBoolean(ARG_IS_EDIT, false);
    }

    private Button makeDayButton(String dayLabel, ViewGroup parent, final int flag) {
        final Button button = (Button) LayoutInflater.from(mContext).inflate(R.layout.day_button, parent, false);
        button.setText(dayLabel);
        button.setSelected((mRule.weekdayFlags & flag) != 0);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setSelected(!button.isSelected());
                mRule.weekdayFlags ^= flag;
                // If we select all days, automatically jump the radio
                if (mRule.weekdayFlags == Rule.FLAG_ALL && mRadio1.isChecked()) {
                    mRadio2.setChecked(true);
                } else {
                    onFormUpdate(false);
                }
            }
        });
        return button;
    }

    /**
     * Must be called initially and whenever anything changes
     * @param isInitial true to call once when form is first drawn
     */
    private void onFormUpdate(boolean isInitial) {
        // Calculate available days mask
        refreshEnabledDays();

        // If no days are selected, or all days + every week are selected, or none of active days are selected, disable OK button
        if (mOkButton != null) {
            if (mRule.weekdayFlags == 0 || (mRule.weekdayFlags == Rule.FLAG_ALL && mRadio1.isChecked()) || ((mRule.weekdayFlags & mEnabledDays) == 0)) {
                mOkButton.setEnabled(false);
            } else {
                mOkButton.setEnabled(true);
            }
        }

        // If initial setup, check if date setup controls need to be hidden
        if (isInitial) {
            if (mRadio1.isChecked()) {
                mDateFromBtn.setVisibility(View.INVISIBLE);
                mDateToBtn.setVisibility(View.GONE);
                mDateToText.setVisibility(View.GONE);
            }
        }
    }

    private void refreshEnabledDays() {
        int mask = mRule.isTemporary ? Utils.getAvailableDaysBetween(mRule.startDay, mRule.endDay) : Rule.FLAG_ALL;
        if (mask == mEnabledDays) {
            return;
        }

        mEnabledDays = mask;

        int firstDay = GregorianCalendar.getInstance().getFirstDayOfWeek();
        int day;

        // todo: solve this somehow more elegantly
        if (Build.VERSION.SDK_INT > 15) {
            for (int i = 0; i < 7; i++) {
                day = (firstDay + i + 6) % 7;
                if ((mask & (1 << day)) == 0) {
                    mDayButtons[i].setEnabled(false);
                    mDayButtons[i].animate().alpha(0.3f).withLayer();
                } else {
                    mDayButtons[i].setEnabled(true);
                    mDayButtons[i].animate().alpha(1).withLayer();
                }
            }
        } else {
            for (int i = 0; i < 7; i++) {
                day = (firstDay + i + 6) % 7;
                if ((mask & (1 << day)) == 0) {
                    mDayButtons[i].setEnabled(false);
                    mDayButtons[i].animate().alpha(0.3f);
                } else {
                    mDayButtons[i].setEnabled(true);
                    mDayButtons[i].animate().alpha(1);
                }
            }
        }

    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {

        void onRuleOverrideAdded(Rule rule);
        void onRuleOverrideEdited(Rule rule);

    }

}
