package com.actinarium.persistence.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.ApplicationContext;
import com.actinarium.persistence.common.BackInterceptor;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.service.PersistenceService;
import com.actinarium.persistence.ui.editprogress.EditProgressActivity;
import com.actinarium.persistence.ui.home.components.AddProgressDialogFragment;
import com.actinarium.persistence.ui.onboarding.OnboardingFragment;
import com.actinarium.persistence.ui.settings.PrefUtils;
import com.actinarium.persistence.ui.setupchallenge.SetupChallengeActivity;
import com.actinarium.persistence.ui.setupchallenge.SetupChallengeFragment;
import com.actinarium.persistence.ui.setupchallenge.components.SpecialRuleDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.StartChallengeDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.wizard.RunWizardDialogFragment;


public class HomeActivity extends AppCompatActivity implements AddProgressDialogFragment.Host,
        StartChallengeDialogFragment.Host, ApplicationContext.EmptyChallengeDiscardedListener,
        OnboardingFragment.Host, RunWizardDialogFragment.Host {

    public static final String ACTION_ADD_PROGRESS = "com.actinarium.persistence.intent.action.ADD_PROGRESS";
    public static final String EXTRA_CHALLENGE_ID = "com.actinarium.persistence.intent.extra.CHALLENGE_ID";

    /**
     * Set this extra if HomeActivity is launched from stacked notification. It should correspond to notification
     * timestamp. When set, challenges will be re-ordered (reminders on top), and "remind in 1h" actions will be added
     * to given notifications.
     */
    public static final String EXTRA_REMINDER_TIMESTAMP = "com.actinarium.persistence.intent.extra.REMINDER_TIMESTAMP";

    private BackInterceptor mInterceptor;

    public static void restartOnboarding(Context context) {
        PrefUtils.setSeenOnboarding(context, false);
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK + Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState != null) {
            return;
        }

        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (!PrefUtils.hasSeenOnboarding(this)) {
            // Hasn't seen onboarding - show it then
            PreferenceManager.setDefaultValues(this, R.xml.settings, false);
            fragmentTransaction
                    .add(R.id.fragment_host, new OnboardingFragment(), OnboardingFragment.TAG);
        } else {
            // Even though we're using a custom action, so far the presence of challengeId is enough to arbitrate
            final long challengeId = getIntent().getLongExtra(EXTRA_CHALLENGE_ID, -1);
            final long reminderTimestamp = getIntent().getLongExtra(EXTRA_REMINDER_TIMESTAMP, 0);

            fragmentTransaction
                    .add(R.id.fragment_host, HomeFragment.newInstance(challengeId, reminderTimestamp), HomeFragment.TAG);
        }

        fragmentTransaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ApplicationContext.get().setEmptyChallengeDiscardedListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationContext.get().setEmptyChallengeDiscardedListener(this);
    }

    @Override
    public void onBackPressed() {
        boolean isIntercepted = mInterceptor != null && mInterceptor.onBackPressed();
        if (!isIntercepted) {
            super.onBackPressed();
        }
    }

    @Override
    public void onNewProgressSaved(Progress progress) {
        PersistenceService.updateProgress(this, progress);
    }

    @Override
    public void onMoreOptionsClicked(Challenge challenge, Progress currentProgress) {
        // todo: handle differently for tablets
        EditProgressActivity.launch(this, challenge, currentProgress);
    }

    @Override
    public void onStartChallengeConfirmed(Challenge challenge) {
        final int today = Utils.getCurrentEpochDay(this);
        challenge.startedOn = today;
        challenge.statusFlags |= Challenge.FLAG_ACTIVE;
        challenge.rulesSetOn = today;
        PersistenceService.startChallenge(this, challenge);
    }

    @Override
    public void onEmptyChallengeDiscarded() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(findViewById(android.R.id.content), R.string.empty_challenge_discarded, Snackbar.LENGTH_SHORT)
                        .show();
            }
        }, 250);
    }

    @Override
    public void setButtonInterceptor(BackInterceptor interceptor) {
        mInterceptor = interceptor;
    }

    @Override
    public void onSeenOnboarding() {
        // On phone, replace onboarding fragment with home fragment.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_host, HomeFragment.newInstance(-1, 0), HomeFragment.TAG)
                .commit();

        PrefUtils.setSeenOnboarding(this, true);
    }

    @Override
    public void onWizardConfirmed() {
        SetupChallengeActivity.launchWizard(this);
    }

    @Override
    public void onWizardRejected() {
        SetupChallengeActivity.launchCreate(this);
    }

    // GET FRAGMENT HELPER METHODS

    private HomeFragment getChallengesListFragment() {
        return (HomeFragment) getSupportFragmentManager()
                .findFragmentByTag(HomeFragment.TAG);
    }

    private OnboardingFragment getOnboardingFragment() {
        return (OnboardingFragment) getSupportFragmentManager()
                .findFragmentByTag(OnboardingFragment.TAG);
    }

    private SetupChallengeFragment getSetupChallengeFragment() {
        return (SetupChallengeFragment) getSupportFragmentManager()
                .findFragmentByTag(SetupChallengeFragment.TAG);
    }

    private SpecialRuleDialogFragment getSpecialRuleDialogFragment() {
        return (SpecialRuleDialogFragment) getSupportFragmentManager()
                .findFragmentByTag(SpecialRuleDialogFragment.TAG);
    }
}
