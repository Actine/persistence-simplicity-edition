package com.actinarium.persistence.ui.home.components;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.common.RecyclerViewCursorAdapter;
import com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.settings.PrefUtils;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ChallengesAdapter extends RecyclerViewCursorAdapter<RecyclerView.ViewHolder> {

    private Host mHost;
    private HeaderHolder mHeaderHolder;
    private EmptyStateCardHolder mWelcomeCardHolder;
    private EmptyStateCardHolder mNoChallengesCardHolder;
    private ViewUtils.HomeScrollWire mWire;
    private double mAvgPercentCompleted;
    private int mGoalsMet;
    private int mActiveGoalsCount;
    private boolean mIsEmpty;

    private static final int CARD_WELCOME_TYPE = 0;

    public ChallengesAdapter(Context context, Cursor cursor, @NonNull Host host, ViewUtils.HomeScrollWire wire) {
        super(context, cursor);
        mHost = host;
        setHasStableIds(true);
        mWire = wire;
        mIsEmpty = false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case R.layout.item_challenges_header:
                if (mHeaderHolder == null) {
                    view = LayoutInflater.from(mContext).inflate(viewType, parent, false);
                    mHeaderHolder = new HeaderHolder(mContext, view);
                }
                return mHeaderHolder;
            case R.layout.card_challenge_active:
                view = LayoutInflater.from(mContext).inflate(viewType, parent, false);
                return new ChallengeCardHolder(view, mHost, mContext);
            case R.layout.card_challenge_pending:
                view = LayoutInflater.from(mContext).inflate(viewType, parent, false);
                return new PendingChallengeCardHolder(view, mHost, mContext);
            case CARD_WELCOME_TYPE:
                if (mWelcomeCardHolder == null) {
                    view = LayoutInflater.from(mContext).inflate(R.layout.card_empty_state, parent, false);
                    mWelcomeCardHolder = new EmptyStateCardHolder(view, mHost, mContext, true);
                }
                return mWelcomeCardHolder;
            case R.layout.card_empty_state:
                if (mNoChallengesCardHolder == null) {
                    view = LayoutInflater.from(mContext).inflate(viewType, parent, false);
                    mNoChallengesCardHolder = new EmptyStateCardHolder(view, mHost, mContext, false);
                }
                return mNoChallengesCardHolder;
            default:
                throw new IllegalArgumentException("Could not create view holder for given view type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case R.layout.item_challenges_header:
                ((HeaderHolder) holder).refreshStats(mAvgPercentCompleted, mGoalsMet, mActiveGoalsCount);
                break;
            case R.layout.card_challenge_active:
                ((ChallengeCardHolder) holder).fillFromCursor(getCursorAtPosition(position));
                break;
            case R.layout.card_challenge_pending:
                ((PendingChallengeCardHolder) holder).fillFromCursor(getCursorAtPosition(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        // If we know that it's an empty state, we only have the welcome card to display
        if (mIsEmpty) {
            return 1;
        }

        final Cursor cursor = getCursor();
        if (cursor == null) {
            return 0;
        }
        return cursor.getCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            if (!mIsEmpty) {
                return R.layout.item_challenges_header;
            } else {
                boolean seenWelcome = PrefUtils.hasCreatedChallenge(mContext);
                return seenWelcome ? R.layout.card_empty_state : CARD_WELCOME_TYPE;
            }
        } else {
            int flags = CursorReaderUtils.getInt(getCursorAtPosition(position), ChallengeEntry.COL_STATUS_FLAGS);
            boolean isActive = (flags & Challenge.FLAG_ACTIVE) != 0;
            return isActive ? R.layout.card_challenge_active : R.layout.card_challenge_pending;
        }
    }

    @Override
    public long getItemId(int position) {
        if (position == 0) {
            return 0;
        } else {
            return CursorReaderUtils.getId(getCursorAtPosition(position), ChallengeEntry.TABLE_NAME);
        }
    }

    // These two are required for backplate scroll impl

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        if ((!mIsEmpty && holder.equals(mHeaderHolder))
                || (mIsEmpty && holder.equals(mWelcomeCardHolder) || holder.equals(mNoChallengesCardHolder))) {
            mWire.setMasterView(holder.itemView);
        }
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        if ((!mIsEmpty && holder.equals(mHeaderHolder))
                || (mIsEmpty && holder.equals(mWelcomeCardHolder) || holder.equals(mNoChallengesCardHolder))) {
            mWire.setMasterView(null);
        }
    }

    private Cursor getCursorAtPosition(int position) {
        final Cursor cursor = getCursor();
        cursor.moveToPosition(position - 1);
        return cursor;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        mCursor = newCursor;
        // todo: do not call notifyDataSetChanged if we know what is going to be changed
        if (newCursor != null) {
            mDataValid = true;
            recalculateStateOnCursorChange(newCursor);
            notifyDataSetChanged();
        } else {
            mDataValid = false;
            notifyDataSetChanged();
        }
        return oldCursor;
    }

    private void recalculateStateOnCursorChange(Cursor cursor) {
        if (cursor.getCount() == 0) {
            mIsEmpty = true;
            return;
        }

        mIsEmpty = false;
        cursor.moveToPosition(-1);
        mAvgPercentCompleted = 0;
        mActiveGoalsCount = 0;
        mGoalsMet = 0;

        // todo: optimize, don't read all rows
        Progress temp = new Progress();
        while (cursor.moveToNext()) {
            // if challenge is not active, skip
            if ((CursorReaderUtils.getInt(cursor, ChallengeEntry.COL_STATUS_FLAGS) & Challenge.FLAG_ACTIVE) == 0) {
                continue;
            }
            // otherwise process
            mActiveGoalsCount++;
            temp.fillFromCursor(cursor);
            final int totalGoal = temp.dailyGoal + temp.debt;
            mAvgPercentCompleted += Math.min((double) (temp.completed + temp.compensated) / totalGoal, 1.0);
            if (temp.completed + temp.compensated >= totalGoal) {
                mGoalsMet++;
            }
        }

        mAvgPercentCompleted = mAvgPercentCompleted / mActiveGoalsCount;

        if (mHeaderHolder != null) {
            mHeaderHolder.refreshStats(mAvgPercentCompleted, mGoalsMet, mActiveGoalsCount);
        }
    }

    /**
     * Interface that must be implemented by the object (usually fragment or activity) hosting this adapter. Aggregates
     * all callbacks from recyclerView's items (e.g. different cards)
     */
    public interface Host extends ChallengeCardHolder.Host, PendingChallengeCardHolder.Host, EmptyStateCardHolder.Host {
    }
}
