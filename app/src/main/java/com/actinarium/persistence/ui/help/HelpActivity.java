package com.actinarium.persistence.ui.help;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.actinarium.persistence.BuildConfig;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.home.HomeActivity;

public class HelpActivity extends AppCompatActivity {

    public static void launch(Context context) {
        Intent intent = new Intent(context, HelpActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState == null) {
            // todo: probably just replace with hardcoded fragment in XML?
            //       do I even need a fragment here?
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_host, new HelpFragment(), HelpFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_help, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_play_store) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            final ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (resolveInfo == null) {
                intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID));
            }
            startActivity(intent);
            return true;
        } else if (id == R.id.action_run_onboarding) {
            // todo cleanup obsolete options
            HomeActivity.restartOnboarding(this);
            return true;
        } else if (id == R.id.action_privacy_policy) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.PRIVACY_POLICY_URL)));
            return true;
        } else if (id == R.id.action_open_source_licenses) {
            // todo: Open dialog with open source licenses
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
