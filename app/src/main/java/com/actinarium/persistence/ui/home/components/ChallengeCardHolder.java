package com.actinarium.persistence.ui.home.components;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.DataUtils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;

/**
* <p></p>
*
* @author Paul Danyliuk
* @version $Id$
*/
public class ChallengeCardHolder extends RecyclerView.ViewHolder {

    private Challenge mChallenge;
    private Progress mProgress;

    private Context mContext;

    private TextView mTitle;
    private TextView mStatus;
    private View mInProgressInfoBlock;
    private TextView mCompletedQty;
    private TextView mCompletedLabel;
    private TextView mLeftQty;
    private TextView mLeftLabel;
    private ProgressBar mProgressBar;
    private Button mAddProgress;
    private Button mGiveUp;

    public ChallengeCardHolder(final View itemView, final Host host, final Context context) {
        super(itemView);
        mChallenge = new Challenge();
        mProgress = new Progress();

        mContext = context;

        mTitle = (TextView) itemView.findViewById(R.id.challenge_title);
        mStatus = (TextView) itemView.findViewById(R.id.challenge_status);

        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

        mInProgressInfoBlock = itemView.findViewById(R.id.block_inprogress);
        mCompletedQty = (TextView) itemView.findViewById(R.id.progress_completed_qty);
        mCompletedLabel = (TextView) itemView.findViewById(R.id.progress_completed_label);
        mLeftQty = (TextView) itemView.findViewById(R.id.progress_left_qty);
        mLeftLabel = (TextView) itemView.findViewById(R.id.progress_left_label);

        mAddProgress = (Button) itemView.findViewById(R.id.add_progress);
        mGiveUp = (Button) itemView.findViewById(R.id.give_up);

        mAddProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Let's assume mProgress is initialized at the moment
                host.addProgress(mChallenge, mProgress);
            }
        });

        // Card click reaction
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                host.onCardClicked(mChallenge);
            }
        });
    }

    public void fillFromCursor(Cursor cursor) {
        mChallenge.fillFromCursor(cursor);
        mProgress.fillFromCursor(cursor);

        // Calculate spark
        final int[] last2DaySpark = DataUtils.extractLast2DaySpark(cursor);
        mChallenge.status = Utils.getStatus(mProgress.getSpark(), last2DaySpark[0], last2DaySpark[1]);

        mTitle.setText(mChallenge.title);
        mStatus.setText(Utils.getStatusText(mContext, mChallenge.status));

        int maxValue = mProgress.dailyGoal + mProgress.debt;
        mProgressBar.setMax(maxValue);
        mProgressBar.setProgress(mProgress.completed);
        mProgressBar.setSecondaryProgress(mProgress.completed + mProgress.compensated);

        mCompletedQty.setText(String.valueOf(mProgress.completed + mProgress.compensated));
        mCompletedLabel.setText(mContext.getString(R.string.qty_down, mChallenge.units));
        mLeftQty.setText(String.valueOf(mProgress.getUnderachievement() - mProgress.compensated));
        mLeftLabel.setText(mContext.getString(R.string.qty_to_go, mChallenge.units));

        boolean suggestGiveUp = false;
        mGiveUp.setVisibility(suggestGiveUp ? View.VISIBLE : View.GONE);
    }

    public interface Host {
        void addProgress(Challenge challenge, Progress todayProgress);
        void onCardClicked(Challenge challenge);
    }
}
