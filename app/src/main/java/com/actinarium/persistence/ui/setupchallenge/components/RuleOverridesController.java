package com.actinarium.persistence.ui.setupchallenge.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Rule;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller responsible for rule overrides
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class RuleOverridesController {

    private static final int ANIM_DURATION = 200;
    private static final int REMOVE_BTN_ROTATION_DEG = -135;

    private Host mHost;
    private Context mContext;
    private LinearLayout mParentLayout;
    private int mItemLayoutId;

    // BOTH LISTS ARE SORTED BY PRIORITY ASC, i.e. topmost element is the last, to facilitate quicker inserts
    private List<RuleViewHolder> mRuleViewHolders;
    private RuleViewHolder mDefaultRuleHolder;
    private int mSize = 0;

    private String mUnitsText;
    private boolean mInteractionLockSet = false;
    private String[] mEasterEggsArray = null;
    private final ColorStateList mDefaultTextTint;
    private final ColorStateList mErrorTint;

    public RuleOverridesController(@Nullable Host host, Context context, LinearLayout parentLayout, @LayoutRes int itemLayoutId) {
        mHost = host;
        mContext = context;
        mParentLayout = parentLayout;
        mItemLayoutId = itemLayoutId;
        mRuleViewHolders = new ArrayList<>(4);
        mUnitsText = null;
        mDefaultTextTint = ColorStateList.valueOf(context.getResources().getColor(R.color.primary));
        mErrorTint = ColorStateList.valueOf(context.getResources().getColor(R.color.warningOnLight));
    }

    public void setItemLayoutId(@LayoutRes int itemLayoutId) {
        mItemLayoutId = itemLayoutId;
    }

    public void addRule(Rule rule) {
        if (mInteractionLockSet) {
            return;
        }
        mInteractionLockSet = true;

        // Set correct priority to the rule
        rule.priority = ++mSize;

        // If quantity is -1, copy from default rule
        if (rule.quantity == Rule.NO_QUANTITY) {
            rule.quantity = mDefaultRuleHolder.mIsValid ? mDefaultRuleHolder.mRule.quantity : Rule.DEFAULT_QUANTITY;
        }

        final RuleViewHolder holder = createAndBindRuleOverride(rule, false);
        // If list is empty before creation, update text in default rule
        if (mRuleViewHolders.isEmpty()) {
            mDefaultRuleHolder.setConditionText(mContext.getString(R.string.rule_every_other_day));
        }

        mRuleViewHolders.add(holder);
        mParentLayout.addView(holder.mView, 0);

        // animation stuff
        holder.mView.setAlpha(0);
        mParentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int offset = holder.mView.getHeight();
                holder.mView.animate().alpha(1).setDuration(ANIM_DURATION);
                if (holder.mRemoveBtn != null) {
                    holder.mRemoveBtn.setRotation(REMOVE_BTN_ROTATION_DEG);
                    holder.mRemoveBtn.animate().rotation(0).setDuration(ANIM_DURATION);
                }
                mDefaultRuleHolder.mView.setTranslationY(-offset);
                mDefaultRuleHolder.mView.animate().translationY(0).setDuration(ANIM_DURATION);
                for (int i = 0; i < mSize; i++) {
                    RuleViewHolder h = mRuleViewHolders.get(i);
                    h.mView.setTranslationY(-offset);
                    h.mView.animate().translationY(0).setDuration(ANIM_DURATION);
                    if (Build.VERSION.SDK_INT > 15) {
                        h.mView.animate().withLayer();
                    }
                }
                //noinspection deprecation
                mParentLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        mInteractionLockSet = false;
        easterEgg();
    }

    public void editRule(Rule rule) {
        for (int i = 0; i < mSize; i++) {
            RuleViewHolder holder = mRuleViewHolders.get(i);
            if (rule.priority == holder.mRule.priority) {
                holder.bind(rule);
                break;
            }
        }
    }

    public void setUnitsText(String unitsText) {
        mUnitsText = unitsText;
        if (mDefaultRuleHolder != null) {
            mDefaultRuleHolder.setUnitsText(unitsText);
        }
        for (int i = 0; i < mSize; i++) {
            RuleViewHolder holder = mRuleViewHolders.get(i);
            holder.setUnitsText(unitsText);
        }
    }

    public boolean isValid() {
        if (!mDefaultRuleHolder.mIsValid) {
            return false;
        }
        for (RuleViewHolder holder : mRuleViewHolders) {
            if (!holder.mIsValid) {
                return false;
            }
        }
        return true;
    }

    public View getFirstInvalidView() {
        for (int i = 0; i < mSize; i++) {
            final RuleViewHolder holder = mRuleViewHolders.get(i);
            if (!holder.mIsValid) {
                return holder.mView;
            }
        }
        if (!mDefaultRuleHolder.mIsValid) {
            return mDefaultRuleHolder.mView;
        }
        return null;
    }

    /**
     * Dump all rules from the controller as the list. First item of the list is always a default rule
     * @return
     */
    public Rule[] getRules() {
        Rule[] rules = new Rule[mSize + 1];
        rules[0] = mDefaultRuleHolder.mRule;
        for (int i = 0; i < mSize; ) {
            RuleViewHolder holder = mRuleViewHolders.get(i);
            rules[++i] = holder.mRule;
        }
        return rules;
    }

    /**
     * Fill the controller with rules (e.g. from persisted state)
     * @param rules List of rules, <b>must be ordered.</b> First rule is the default one
     */
    public void setRules(Rule[] rules) {
        if (rules.length == 0) {
            throw new IllegalArgumentException("List of rules must have at least one element (default rule)");
        }

        // Clean up everything
        mParentLayout.removeAllViews();
        mRuleViewHolders.clear();

        setDefaultRule(rules[0]);

        if (rules.length != 1) {
            mDefaultRuleHolder.setConditionText(mContext.getString(R.string.rule_every_other_day));
            mSize = rules.length - 1;
            for (int i = 0; i < mSize; ) {
                Rule rule = rules[++i];
                rule.priority = i;
                RuleViewHolder holder = createAndBindRuleOverride(rule, false);
                mRuleViewHolders.add(holder);
                mParentLayout.addView(holder.mView, 0);
            }
        }
    }

    public void setDefaultRule(Rule rule) {
        rule.priority = 0;
        mDefaultRuleHolder = createAndBindRuleOverride(rule, true);
        mParentLayout.addView(mDefaultRuleHolder.mView, 0);
    }

    private void deleteRule(final RuleViewHolder holder) {
        // primitive lock - otherwise bad things may happen when clicking x on several items simultaneously
        if (mInteractionLockSet) {
            return;
        }
        if (mRuleViewHolders.remove(holder)) {
            mInteractionLockSet = true;
            mSize--;

            // animate transitions of all rules beneath
            int deletedViewHeight = holder.mView.getHeight();
            final int affectedViews = holder.mRule.priority - 1;
            for (int i = 0; i < affectedViews; i++) {
                final ViewPropertyAnimator animate = mRuleViewHolders.get(i).mView.animate();
                animate.translationY(-deletedViewHeight).setDuration(ANIM_DURATION);
                if (Build.VERSION.SDK_INT > 15) {
                    animate.withLayer();
                }
            }
            mDefaultRuleHolder.mView.animate().translationY(-deletedViewHeight).setDuration(ANIM_DURATION);
            if (Build.VERSION.SDK_INT > 15) {
                mDefaultRuleHolder.mView.animate().withLayer();
            }

            // Also for all rules above decrement priority by 1 (note that current rule is already removed)
            for (int i = affectedViews; i < mSize; i++) {
                mRuleViewHolders.get(i).mRule.priority--;
            }

            // tell the host to animate scroller up
            mHost.requestVerticalScroll(-deletedViewHeight, false);

            // animate disappearance of our view and consequential removal / reset
            holder.mView.animate().alpha(0).setDuration(ANIM_DURATION).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    for (int i = 0; i < affectedViews; i++) {
                        final View view = mRuleViewHolders.get(i).mView;
                        view.animate().cancel();
                        view.setTranslationY(0);
                    }
                    mDefaultRuleHolder.mView.animate().cancel();
                    mDefaultRuleHolder.mView.setTranslationY(0);

                    // If list becomes empty, update text in default rule
                    if (mRuleViewHolders.isEmpty()) {
                        mDefaultRuleHolder.setConditionText(mContext.getString(R.string.rule_every_day));
                    }

                    mParentLayout.removeView(holder.mView);
                    mInteractionLockSet = false;
                }
            });

            // tell the host to re-check if now rules are valid
            mHost.requestValidityCheck(false);
        }
    }

    private void easterEgg() {
        if (mEasterEggsArray == null) {
            mEasterEggsArray = mContext.getResources().getStringArray(R.array.challenge_easter_eggs);
        }

        if (mRuleViewHolders.size() == 9) {
            Toast.makeText(mContext, mEasterEggsArray[0], Toast.LENGTH_SHORT).show();
        } else if (mRuleViewHolders.size() == 19) {
            Toast.makeText(mContext, mEasterEggsArray[1], Toast.LENGTH_SHORT).show();
        } else if (mRuleViewHolders.size() == 29) {
            Toast.makeText(mContext, mEasterEggsArray[2], Toast.LENGTH_LONG).show();
        } else if (mRuleViewHolders.size() == 39) {
            Toast.makeText(mContext, mEasterEggsArray[3], Toast.LENGTH_SHORT).show();
        } else if (mRuleViewHolders.size() == 99) {
            Toast.makeText(mContext, mEasterEggsArray[4], Toast.LENGTH_LONG).show();
        } else if (mRuleViewHolders.size() == 100) {
            Toast.makeText(mContext, mEasterEggsArray[5], Toast.LENGTH_LONG).show();
        }
    }

    private RuleViewHolder createAndBindRuleOverride(final Rule rule, boolean isDefaultRule) {
        View view = LayoutInflater.from(mContext).inflate(mItemLayoutId, mParentLayout, false);
        return new RuleViewHolder(view, rule, mUnitsText, isDefaultRule);
    }

    /**
     * Holder for rule views
     */
    private class RuleViewHolder {

        View mView;
        Rule mRule;
        private final TextView mQuantity;
        private final TextView mUnits;
        private final TextView mCondition;
        private boolean mIsErrorTinted = false;
        private boolean mIsValid = true;
        private final ImageButton mRemoveBtn;

        RuleViewHolder(View view, Rule rule, String units, boolean isDefaultRule) {
            mView = view;

            // No remove button indicates it's uneditable item view (i.e. just to display the rule)
            mRemoveBtn = (ImageButton) view.findViewById(R.id.rule_remove);
            if (mRemoveBtn != null) {
                //noinspection deprecation
                mRemoveBtn.setAlpha(Utils.DEFAULT_ICON_ALPHA);

                if (isDefaultRule) {
                    mRemoveBtn.setVisibility(View.INVISIBLE);
                    view.setClickable(false);
                } else {
                    // click on the rule -> edit rule
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Also we need to clone the rule because apparently serialization does not create a copy...
                            mHost.openEditRuleDialog(new Rule(mRule));
                        }
                    });

                    // delete rule button
                    mRemoveBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteRule(RuleViewHolder.this);
                        }
                    });
                }
            }

            mQuantity = (TextView) view.findViewById(R.id.rule_quantity);
            mUnits = (TextView) view.findViewById(R.id.rule_units);
            mCondition = (TextView) view.findViewById(R.id.rule_condition);

            mUnits.setText(units);
            bind(rule);
            mQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    mIsValid = s.length() != 0;
                    if (mIsValid) {
                        mRule.quantity = Integer.parseInt(s.toString());
                        if (mRule.quantity < 1) {
                            mIsValid = false;
                            mHost.requestValidityCheck(true);
                        } else {
                            // Not sure if other rules are valid
                            mHost.requestValidityCheck(false);
                        }
                    } else {
                        mRule.quantity = Rule.NO_QUANTITY;
                        mHost.requestValidityCheck(false);
                    }

                    // Error state, if wrong input
                    if (!mIsValid && !mIsErrorTinted) {
                        ViewCompat.setBackgroundTintList(mQuantity, mErrorTint);
                        mIsErrorTinted = true;
                    } else if (mIsValid && mIsErrorTinted) {
                        ViewCompat.setBackgroundTintList(mQuantity, mDefaultTextTint);
                        mIsErrorTinted = false;
                    }
                }
            });
        }

        void setUnitsText(String text) {
            mUnits.setText(text);
        }

        void setConditionText(String text) {
            mCondition.setText(text);
        }

        void bind(Rule rule) {
            mRule = rule;
            mQuantity.setText(rule.quantity == Rule.NO_QUANTITY ? null : String.valueOf(rule.quantity));
            mCondition.setText(Utils.prettyPrintRule(rule, mContext));

            if (rule.quantity < 1) {
                ViewCompat.setBackgroundTintList(mQuantity, mErrorTint);
                mIsValid = false;
                mIsErrorTinted = true;
            } else {
                ViewCompat.setBackgroundTintList(mQuantity, mDefaultTextTint);
                mIsValid = true;
                mIsErrorTinted = false;
            }
        }

    }

    public interface Host {

        void requestVerticalScroll(int offset, boolean force);
        void openEditRuleDialog(Rule rule);
        void requestValidityCheck(boolean knownToBeInvalid);

    }

}
