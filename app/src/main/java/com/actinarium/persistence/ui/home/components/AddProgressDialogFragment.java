package com.actinarium.persistence.ui.home.components;


import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProgressDialogFragment extends DialogFragment {

    public static final String TAG = "AddProgressFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    private static final String ARG_PROGRESS = "com.actinarium.persistence.intent.arg.PROGRESS";

    private Host mHost;
    private Progress mProgress;
    private EditText mProgressQuantity;
    private Challenge mChallenge;

    public static AddProgressDialogFragment newInstance(Challenge challenge, Progress todayProgress) {
        AddProgressDialogFragment fragment = new AddProgressDialogFragment();
        Bundle args = new Bundle(2);
        args.putParcelable(ARG_CHALLENGE, challenge);
        args.putParcelable(ARG_PROGRESS, todayProgress);
        fragment.setArguments(args);
        return fragment;
    }

    public AddProgressDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHost = (Host) activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mChallenge = getArguments().getParcelable(ARG_CHALLENGE);
        mProgress = getArguments().getParcelable(ARG_PROGRESS);

        final Context context = getActivity();
        final View view = LayoutInflater.from(context).inflate(R.layout.dialog_edit_progress, null);

        final TextView subtitle = (TextView) view.findViewById(R.id.progress_subtitle);
        final TextView challengeUnits = (TextView) view.findViewById(R.id.challenge_units);
        final TextView challengeGoal = (TextView) view.findViewById(R.id.challenge_goal);
        mProgressQuantity = (EditText) view.findViewById(R.id.progress_quantity);

        String date = Utils.prettyPrintDate(Utils.createDayFromEpoch(mProgress.day), Utils.DATE_FORMAT_SHORT_WEEKDAY);
        subtitle.setText(context.getString(R.string.progress_for_day, date));

        if (savedInstanceState == null) {
            mProgressQuantity.setText(Integer.toString(mProgress.completed));
        }

        challengeUnits.setText(mChallenge.units);
        if (mProgress.debt == 0) {
            challengeGoal.setText(context.getString(R.string.goal_no_debt, mProgress.dailyGoal, mChallenge.units));
        } else {
            challengeGoal.setText(context.getString(
                    R.string.goal_with_debt,
                    mProgress.dailyGoal + mProgress.debt,
                    mChallenge.units,
                    mProgress.dailyGoal,
                    mProgress.debt
            ));
        }

        //noinspection UnnecessaryLocalVariable
        AlertDialog dialog = new AlertDialog.Builder(context, R.style.Theme_Persistence_Dialog_Alert)
                .setTitle(mChallenge.title)
                .setView(view)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateProgressFromForm();
                        mHost.onNewProgressSaved(mProgress);
                    }
                })
                .setNeutralButton(R.string.more_options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateProgressFromForm();
                        mHost.onMoreOptionsClicked(mChallenge, mProgress);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();

        // Display keyboard when dialog is displayed
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                if (mProgressQuantity.requestFocus()) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mProgressQuantity, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        return dialog;
    }

    private void updateProgressFromForm() {
        final String quantity = mProgressQuantity.getText().toString();
        mProgress.completed = quantity.isEmpty() ? 0 : Integer.parseInt(quantity);

        // Adjust reserve usage accordingly
        final int reserveBalance = mProgress.doubleReserveBalance / 2;
        final int maxPossibleCompensation = Math.min(mProgress.getUnderachievement(), reserveBalance);
        if (mProgress.compensated > maxPossibleCompensation) {
            mProgress.compensated = maxPossibleCompensation;
        }
    }


    public interface Host {
        void onMoreOptionsClicked(Challenge challenge, Progress currentProgress);
        void onNewProgressSaved(Progress progress);
    }
}
