package com.actinarium.persistence.ui.home.components;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;

/**
* <p></p>
*
* @author Paul Danyliuk
* @version $Id$
*/
public class PendingChallengeCardHolder extends RecyclerView.ViewHolder {

    private final Context mContext;

    private final Challenge mChallenge;

    private final TextView mTitle;
    private final TextView mDescription;

    private final Button mStart;

    public PendingChallengeCardHolder(View itemView, final Host host, Context context) {
        super(itemView);
        mContext = context;

        mChallenge = new Challenge();

        mTitle = ((TextView) itemView.findViewById(R.id.challenge_title));
        mDescription = ((TextView) itemView.findViewById(R.id.challenge_status));

        mStart = ((Button) itemView.findViewById(R.id.start_challenge));
        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                host.onStartChallengeClicked(mChallenge);
            }
        });

        // Card click reaction
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                host.onCardClicked(mChallenge);
            }
        });
    }

    public void fillFromCursor(Cursor cursor) {
        mChallenge.fillFromCursor(cursor);

        mTitle.setText(mChallenge.title);
        final int dayDiff = Utils.getCurrentEpochDay(mContext) - mChallenge.createdOn;
        mDescription.setText(mContext.getString(R.string.added_when, Utils.getDurationAgoString(mContext, dayDiff)));
        if (dayDiff >= Challenge.PENDING_NAG_AFTER_DAYS) {
            final int warnColor = mContext.getResources().getColor(R.color.warning);
            mDescription.setTextColor(warnColor);
            mStart.setTextColor(warnColor);
        } else {
            mDescription.setTextColor(mContext.getResources().getColor(R.color.secondary_text_default_material_light));
            mStart.setTextColor(mContext.getResources().getColor(R.color.accent));
        }
    }

    public interface Host {
        void onStartChallengeClicked(Challenge challenge);
        void onCardClicked(Challenge challenge);
    }
}
