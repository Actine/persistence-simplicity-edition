package com.actinarium.persistence.ui.editprogress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.service.PersistenceService;

public class EditProgressActivity extends AppCompatActivity implements EditProgressFragment.Host {

    public static final String EXTRA_CHALLENGE = "com.actinarium.persistence.intent.extra.CHALLENGE";
    public static final String EXTRA_PROGRESS = "com.actinarium.persistence.intent.extra.PROGRESS";

    public static void launch(Context context, Challenge challenge, @Nullable Progress progress) {
        Intent intent = new Intent(context, EditProgressActivity.class);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        intent.putExtra(EXTRA_PROGRESS, progress);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState == null) {
            final Challenge challenge = getIntent().getParcelableExtra(EXTRA_CHALLENGE);
            final Progress progress = getIntent().getParcelableExtra(EXTRA_PROGRESS);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_host, EditProgressFragment.newInstance(challenge, progress), EditProgressFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNewProgressSaved(Progress progress) {
        PersistenceService.updateProgress(this, progress);
        finish();
    }
}
