package com.actinarium.persistence.ui.editprogress;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.ui.common.ViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProgressFragment extends Fragment {

    public static final String TAG = "EditProgressFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    private static final String ARG_PROGRESS = "com.actinarium.persistence.intent.arg.PROGRESS";

    private Host mHost;
    private Challenge mChallenge;
    private Progress mProgress;

    private TextView mChallengeTitle;
    private Button mProgressDay;
    private EditText mProgressQuantity;
    private TextView mChallengeUnitsText;
    private TextView mChallengeGoal;
    private EditText mCompensatedQuantity;
    private TextView mCompensatedUnitsText;
    private TextView mReserveAvailability;
    private EditText mNote;
    private int mReserveBalance;

    public static EditProgressFragment newInstance(Challenge challenge, @Nullable Progress progress) {
        EditProgressFragment fragment = new EditProgressFragment();
        Bundle args = new Bundle(2);
        args.putParcelable(ARG_CHALLENGE, challenge);
        args.putParcelable(ARG_PROGRESS, progress);
        fragment.setArguments(args);
        return fragment;
    }

    public EditProgressFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHost = (Host) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mChallenge = getArguments().getParcelable(ARG_CHALLENGE);
        if (mChallenge == null) {
            throw new IllegalArgumentException("Challenge must not be null");
        }

        // todo: either get progress from arguments, or nag the loader to get progress instance from DB
        mProgress = getArguments().getParcelable(ARG_PROGRESS);

        // Inflate the view
        View view = inflater.inflate(R.layout.fragment_edit_progress, container, false);

        // Setting up toolbar
        final ActionBar actionBar = ViewUtils.setUpToolbar(getActivity(), view, R.string.set_progress, R.dimen.actionBarElevation);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        actionBar.setHomeActionContentDescription(R.string.accessibility_discard_close);

        initializeFields(view);
        setupFormFromChallenge();

        // Fill the form from progress (don't overwrite EditTexts if restoring fragment)
        bindProgress(mProgress, savedInstanceState == null);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_just_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveProgress();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Bind given progress to the form (e.g. when form is initialized or re-populated with progress for a different day)
     * @param progress Progress to fill the form from
     */
    private void bindProgress(final Progress progress, boolean updateQtyFields) {

        // Buttons don't persist state on rotation, so //todo: always fill it with selected progress
        mProgressDay.setText(Utils.prettyPrintDate(Utils.createDayFromEpoch(progress.day), Utils.DATE_FORMAT_SHORT_WEEKDAY));

        if (updateQtyFields) {
            mProgressQuantity.setText(Integer.toString(progress.completed));
            mCompensatedQuantity.setText(Integer.toString(progress.compensated));
        }

        if (progress.debt == 0) {
            mChallengeGoal.setText(getString(R.string.goal_no_debt, progress.dailyGoal, mChallenge.units));
        } else {
            mChallengeGoal.setText(getString(
                    R.string.goal_with_debt,
                    progress.dailyGoal + progress.debt,
                    mChallenge.units,
                    progress.dailyGoal,
                    progress.debt
            ));
        }

        mReserveBalance = progress.doubleReserveBalance / 2;
        mReserveAvailability.setText(getString(R.string.available_reserve, mReserveBalance, mChallenge.units));
        mNote.setText(mProgress.note);

        mProgressQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // todo: on losing focus, recalculate max possible compensation and animate if required
                Log.i(TAG, "Progress qty focus changed to " + Boolean.toString(hasFocus));
            }
        });
        mCompensatedQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.i(TAG, "Compensated qty focus changed to " + Boolean.toString(hasFocus));
                // todo: on losing focus, detect if not exceeding max possible compensation and animate down if required
            }
        });
    }

    private void saveProgress() {
        final String quantityCompleted = mProgressQuantity.getText().toString();
        final String quantityCompensated = mCompensatedQuantity.getText().toString();
        final String note = mNote.getText().toString().trim();
        mProgress.completed = quantityCompleted.isEmpty() ? 0 : Integer.parseInt(quantityCompleted);
        mProgress.compensated = quantityCompensated.isEmpty() ? 0 : Integer.parseInt(quantityCompensated);
        mProgress.note = note.isEmpty() ? null : note;

        // Shouldn't happen, but just in case
        final int maxPossibleCompensation = Math.min(mProgress.getUnderachievement(), mReserveBalance);
        if (mProgress.compensated > maxPossibleCompensation) {
            mProgress.compensated = maxPossibleCompensation;
        }

        mHost.onNewProgressSaved(mProgress);
    }

    private void initializeFields(View view) {
        mChallengeTitle = (TextView) view.findViewById(R.id.challenge_title);
        mProgressDay = (Button) view.findViewById(R.id.progress_day);

        mProgressQuantity = (EditText) view.findViewById(R.id.progress_quantity);
        mChallengeUnitsText = (TextView) view.findViewById(R.id.challenge_units);
        mChallengeGoal = (TextView) view.findViewById(R.id.challenge_goal);

        mCompensatedQuantity = (EditText) view.findViewById(R.id.compensated_quantity);
        mCompensatedUnitsText = (TextView) view.findViewById(R.id.compensated_units);
        mReserveAvailability = (TextView) view.findViewById(R.id.reserve_availability);

        mNote = (EditText) view.findViewById(R.id.day_note);

        int i4dp = getResources().getDimensionPixelOffset(R.dimen.i4dips);
        com.actinarium.aligned.Utils.setExactMetrics(mNote, i4dp * 8, i4dp * 5, i4dp);
    }

    private void setupFormFromChallenge() {
        mChallengeTitle.setText(mChallenge.title);
        mChallengeUnitsText.setText(getString(R.string.qty_completed, mChallenge.units));
        mCompensatedUnitsText.setText(getString(R.string.qty_compensated, mChallenge.units));
    }

    public interface Host {
        void onNewProgressSaved(Progress progress);
    }
}
