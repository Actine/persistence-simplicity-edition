package com.actinarium.persistence.ui.setupchallenge.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.ui.common.RemindersController;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class EditableRemindersController extends RemindersController {

    private static final int ANIM_DURATION = 200;

    private Host mHost;
    private ReminderTypeSpinnerAdapter mSpinnerAdapter;
    private boolean mInteractionLockSet = false;

    public EditableRemindersController(@Nullable Host host, Context context, LinearLayout parentLayout) {
        super(parentLayout, context);
        mHost = host;
        mSpinnerAdapter = new ReminderTypeSpinnerAdapter(mContext);
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    public void addReminder(int newTime) {
        if (mInteractionLockSet) {
            return;
        }
        mInteractionLockSet = true;

        // If time is still previous day (e.g. 2 AM when day starts at 5 AM), it should appear in the end of the list
        final int key = newTime < mDayStart ? newTime + DAY_IN_MINUTES : newTime;

        // Verify if there's no such reminder yet
        if (mReminders.indexOfKey(key) >= 0) {
            mHost.onDuplicateReminder();
            mInteractionLockSet = false;
            return;
        }

        // Create and bind reminder item
        final ReminderHolder holder = createHolder(newTime, 0);
        final View holderView =  holder.getView();

        mReminders.put(key, holder);
        final int index = mReminders.indexOfKey(key);
        mParentLayout.addView(holderView, index);

        // Update button label
        if (mSize == 0) {
            mHost.updateAddReminderButtonText(true);
        }
        mSize++;

        // animation stuff
        holderView.setAlpha(0);
        mParentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Height of inserting view
                final int offset = holderView.getHeight();
                // Inserting view will slowly fade in
                holderView.animate().alpha(1).setDuration(ANIM_DURATION);
                // Whereas all views after it will slide down
                for (int i = index + 1; i < mSize; i++) {
                    ReminderHolder h = mReminders.valueAt(i);
                    final View holderView = h.getView();
                    holderView.setTranslationY(-offset);
                    holderView.animate().translationY(0).setDuration(ANIM_DURATION);
                    if (Build.VERSION.SDK_INT > 15) {
                        holderView.animate().withLayer();
                    }
                }
                //noinspection deprecation
                mParentLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        mInteractionLockSet = false;
    }

    public void updateReminder(int oldTime, int newTime) {
        if (oldTime == newTime || mInteractionLockSet) {
            return;
        }
        mInteractionLockSet = true;

        final int oldKey = oldTime < mDayStart ? oldTime + DAY_IN_MINUTES : oldTime;
        final int newKey = newTime < mDayStart ? newTime + DAY_IN_MINUTES : newTime;

        if (mReminders.indexOfKey(newKey) >= 0) {
            mHost.onDuplicateReminder();
            mInteractionLockSet = false;
            return;
        }

        final int oldIndex = mReminders.indexOfKey(oldKey);
        final ReminderHolder holder = mReminders.valueAt(oldIndex);
        holder.setTime(newTime);
        mReminders.removeAt(oldIndex);
        mReminders.put(newKey, holder);
        final int newIndex = mReminders.indexOfKey(newKey);

        // if oldIndex == newIndex, then the item was simply updated, no animation needed
        if (oldIndex == newIndex) {
            mInteractionLockSet = false;
            return;
        }

        // otherwise store view's height and top, move the view, and after next layout animate changes
        final View holderView = holder.getView();
        final int movingViewTopBefore = holderView.getTop();
        final int movingViewHeight = holderView.getHeight();
        mParentLayout.removeViewAt(oldIndex);
        mParentLayout.addView(holderView, newIndex);
        mParentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Moving view offset is current height - prev height
                final int movingViewOffset = holderView.getTop() - movingViewTopBefore;
                holderView.setTranslationY(-movingViewOffset);
                holderView.animate().translationY(0).setDuration(ANIM_DURATION);
                if (Build.VERSION.SDK_INT > 15) {
                    holderView.animate().withLayer();
                }

                if (newIndex > oldIndex) {
                    // Slide all items up (item at oldIndex will be the first after the displaced one, so DO include it)
                    for (int i = oldIndex; i < newIndex; i++) {
                        View view = mParentLayout.getChildAt(i);
                        view.setTranslationY(movingViewHeight);
                        view.animate().translationY(0).setDuration(ANIM_DURATION);
                        if (Build.VERSION.SDK_INT > 15) {
                            view.animate().withLayer();
                        }
                    }
                } else {
                    // Slide all items down (item at oldIndex will be the first before the displaced one, so DO include it)
                    for (int i = newIndex + 1; i <= oldIndex; i++) {
                        View view = mParentLayout.getChildAt(i);
                        view.setTranslationY(-movingViewHeight);
                        view.animate().translationY(0).setDuration(ANIM_DURATION);
                        if (Build.VERSION.SDK_INT > 15) {
                            view.animate().withLayer();
                        }
                    }
                }

                //noinspection deprecation
                mParentLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        mInteractionLockSet = false;
    }

    public void deleteReminder(int time) {
        if (mInteractionLockSet) {
            return;
        }
        mInteractionLockSet = true;

        final int key = time < mDayStart ? time + DAY_IN_MINUTES : time;

        final int index = mReminders.indexOfKey(key);
        if (index < 0) {
            return;
        }

        mReminders.removeAt(index);

        // animate transitions of all reminders beneath
        final View removingView = mParentLayout.getChildAt(index);
        int deletedViewHeight = removingView.getHeight();
        for (int i = index + 1; i < mSize; i++) {
            final ViewPropertyAnimator animate = mParentLayout.getChildAt(i).animate();
            animate.translationY(-deletedViewHeight).setDuration(ANIM_DURATION);
            if (Build.VERSION.SDK_INT > 15) {
                animate.withLayer();
            }
        }

        // tell fragment to animate scroller up
        mHost.requestVerticalScroll(-deletedViewHeight, false);

        // animate disappearance of our view and consequential removal / reset
        removingView.animate().alpha(0).setDuration(ANIM_DURATION).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                for (int i = index + 1; i < mSize; i++) {
                    final View view = mParentLayout.getChildAt(i);
                    view.animate().cancel();
                    view.setTranslationY(0);
                }
                mParentLayout.removeViewAt(index);

                mSize--;
                if (mSize == 0) {
                    mHost.updateAddReminderButtonText(false);
                }

                mInteractionLockSet = false;
            }
        });
    }

    @Override
    protected void onRemindersChanged() {
        if (mHost != null) {
            mHost.updateAddReminderButtonText(mSize != 0);
        }
    }

    @Override
    protected ReminderHolder createHolder(int time, int type) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_reminder, mParentLayout, false);
        return new EditableReminderHolder(view, time, type);
    }

    private class EditableReminderHolder implements ReminderHolder {

        View mView;
        int mTime;

        private final TextView mTimeBtn;
        private final Spinner mType;
        private final ImageButton mRemoveBtn;

        EditableReminderHolder(View view, int time, int type) {
            mView = view;
            mTime = time;

            mTimeBtn = (TextView) view.findViewById(R.id.reminder_time);
            mTimeBtn.setText(Utils.timeToString(Utils.extractHour(mTime), Utils.extractMinute(mTime), mContext));
            if (mTimeBtn.isClickable()) {
                mTimeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mHost.editReminderTime(mTime);
                    }
                });
            }

            mType = (Spinner) view.findViewById(R.id.reminder_type);
            mType.setAdapter(mSpinnerAdapter);
            // Equal to the selection index, since those flags are exactly 0, 1, 2. Just saving some cycles
            mType.setSelection(type);

            mRemoveBtn = (ImageButton) view.findViewById(R.id.rule_remove);
            if (mRemoveBtn != null) {
                //noinspection deprecation
                mRemoveBtn.setAlpha(Utils.DEFAULT_ICON_ALPHA);
                mRemoveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteReminder(mTime);
                    }
                });
            }
        }

        public void setTime(int time) {
            mTime = time;
            mTimeBtn.setText(Utils.timeToString(Utils.extractHour(mTime), Utils.extractMinute(mTime), mContext));
        }

        @Override
        public int getType() {
            return mType.getSelectedItemPosition();

            // Equal to the following, since those flags are exactly 0, 1, 2. Just saving some cycles
            /*
            switch (mType.getSelectedItemPosition()) {
                case 0: return Reminder.FLAG_FIRE_ALWAYS;
                case 1: return Reminder.FLAG_FIRE_IF_GOAL_NOT_MET;
                case 2: return Reminder.FLAG_FIRE_IF_NO_PROGRESS;
            }
            */
        }

        @Override
        public int getTime() {
            return mTime;
        }

        @Override
        public View getView() {
            return mView;
        }
    }

    public interface Host {

        void onDuplicateReminder();
        void editReminderTime(int oldTime);
        void updateAddReminderButtonText(boolean hasReminders);
        void requestVerticalScroll(int offset, boolean force);

    }
}
