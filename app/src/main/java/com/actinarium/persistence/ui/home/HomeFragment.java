package com.actinarium.persistence.ui.home;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.data.PersistenceContract;
import com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.service.ReminderService;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.help.HelpActivity;
import com.actinarium.persistence.ui.home.components.AddProgressDialogFragment;
import com.actinarium.persistence.ui.home.components.ChallengesAdapter;
import com.actinarium.persistence.ui.settings.PrefUtils;
import com.actinarium.persistence.ui.settings.SettingsActivity;
import com.actinarium.persistence.ui.setupchallenge.SetupChallengeActivity;
import com.actinarium.persistence.ui.setupchallenge.components.StartChallengeDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.wizard.RunWizardDialogFragment;
import com.actinarium.persistence.ui.support.SupportActivity;
import com.actinarium.persistence.ui.viewchallenge.ViewChallengeActivity;

/**
 * Fragment that takes care of challenges list
 */
public class HomeFragment extends Fragment
        implements ChallengesAdapter.Host, LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = "HomeFragment";

    private static final String ARG_CHALLENGE_ID = "com.actinarium.persistence.intent.arg.CHALLENGE_ID";
    private static final String ARG_REMINDER_TIMESTAMP = "com.actinarium.persistence.intent.arg.REMINDER_TIMESTAMP";

    private static final int LOADER_CHALLENGES_WITH_TODAY = 0;

    private ChallengesAdapter mAdapter;
    private RecyclerView mRecycler;


    public static HomeFragment newInstance(long challengeId, long reminderTimestamp) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle(2);
        args.putLong(ARG_CHALLENGE_ID, challengeId);
        args.putLong(ARG_REMINDER_TIMESTAMP, reminderTimestamp);
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Activity activity = getActivity();
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        ActionBar actionBar = ViewUtils.setUpToolbar(activity, view, R.string.title_today, 0);

        // Set up things required for parallax wiring
        final Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        final View parallaxView = view.findViewById(R.id.flex_space);
        final boolean isLandscape = getResources().getBoolean(R.bool.isLandscape);

        // determine parallax multiplier
        final float parallaxMultiplier;
        final int BACKPLATE_HEIGHT = 144;
        final int CARD_OVERLAP = 48;
        final int LANDSCAPE_ACTION_BAR = 48;
        final int SPACING = 8;
        if (isLandscape) {
            parallaxMultiplier = (BACKPLATE_HEIGHT + LANDSCAPE_ACTION_BAR)
                    / (float)(BACKPLATE_HEIGHT + LANDSCAPE_ACTION_BAR - CARD_OVERLAP - SPACING);
        } else {
            parallaxMultiplier = BACKPLATE_HEIGHT
                    / (float)(BACKPLATE_HEIGHT - CARD_OVERLAP - SPACING);
        }

        // Set up RecyclerView and wire
        mRecycler = (RecyclerView) view.findViewById(R.id.recycler);
        ViewUtils.HomeScrollWire wire = new ViewUtils.HomeScrollWire()
                .setRaisingActionBar(actionBar, getResources().getDimension(R.dimen.actionBarElevation))
                .setExitingToolbar(toolbar, isLandscape)
                .setParallaxView(parallaxView, parallaxMultiplier)
                .setMasterViewInitialTop(getResources().getDimensionPixelOffset(R.dimen.recyclerView_header_top_margin))
                .wireTo(mRecycler);
        mAdapter = new ChallengesAdapter(activity, null, this, wire);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(activity));
        mRecycler.setItemAnimator(new ViewUtils.WiredAnimator(wire));

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        // Initialize loaders here to prevent loading data twice (hack per http://stackoverflow.com/a/14524031/2299005)
        getLoaderManager().initLoader(LOADER_CHALLENGES_WITH_TODAY, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_challenge_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_challenge) {
            onAddChallengeClicked();
            return true;
        } else if (id == R.id.action_settings) {
            SettingsActivity.launch(getActivity());
            return true;
        } else if (id == R.id.action_support_developer) {
            SupportActivity.launch(getActivity());
            return true;
        } else if (id == R.id.action_help_about) {
            HelpActivity.launch(getActivity());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
       Add progress dialog
    */

    private void showAddProgressDialog(Challenge challenge, Progress progress) {
        AddProgressDialogFragment
                .newInstance(challenge, progress)
                .show(getFragmentManager(), AddProgressDialogFragment.TAG);
    }

    private void handleSingleReminderClick(long challengeId, long reminderTimestamp, Cursor cursor) {
        final Activity activity = getActivity();
        if (activity != null) {
            ReminderService.dismissReminders(activity, reminderTimestamp);

            // Find notification from data
            final int itemCount = cursor.getCount();
            for (int i = 0; i < itemCount; i++) {
                cursor.moveToPosition(i);
                if (CursorReaderUtils.getId(cursor, ChallengeEntry.TABLE_NAME) == challengeId) {
                    final Challenge challenge = new Challenge().fillFromCursor(cursor);
                    final Progress progress = new Progress().fillFromCursor(cursor);
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            showAddProgressDialog(challenge, progress);
                        }
                    });
                    break;
                }
            }
        }
    }

    /*
       Card callbacks
    */

    @Override
    public void addProgress(Challenge challenge, Progress todayProgress) {
        // Clone the progress so that edits don't mutate card data
        showAddProgressDialog(challenge, new Progress(todayProgress));
    }

    @Override
    public void onCardClicked(Challenge challenge) {
        // Start activity for challenge view (todo: on tablet, display appropriate card)
        ViewChallengeActivity.launch(getActivity(), challenge);
    }

    @Override
    public void onStartChallengeClicked(Challenge challenge) {
        StartChallengeDialogFragment dialog = StartChallengeDialogFragment.newInstance(false, challenge);
        dialog.show(getFragmentManager(), StartChallengeDialogFragment.TAG);
    }

    @Override
    public void onAddChallengeClicked() {
        final Context context = getContext();
        if (PrefUtils.hasCreatedChallenge(context)) {
            SetupChallengeActivity.launchCreate(context);
        } else {
            RunWizardDialogFragment fragment = new RunWizardDialogFragment();
            fragment.show(getFragmentManager(), RunWizardDialogFragment.TAG);
        }
    }

    /*
       Dialog callbacks
    */

    public void onUpdateProgressOperationStarted(Progress progress, int id) {
        // todo: reconsider if this is still needed
        // Initially this method was designed to communicate progress update to the fragment and scroll the card into view
    }

    // ------------------- LOADER -------------------

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Only one loader, no need to arbitrate
        return new CursorLoader(
                getActivity(),
                ChallengeEntry.CONTENT_URI_EXTENDED,
                PersistenceContract.Projections.CHALLENGE_WITH_PROGRESS_FULL,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Only one loader, no need to arbitrate
        mAdapter.swapCursor(data);

        // If activity started from single notification, handle it here (show progress dialog)
        final long challengeId = getArguments().getLong(ARG_CHALLENGE_ID, -1);
        if (challengeId != -1) {
            final long reminderTimestamp = getArguments().getLong(ARG_REMINDER_TIMESTAMP, 0);
            getArguments().putLong(ARG_CHALLENGE_ID, -1);
            handleSingleReminderClick(challengeId, reminderTimestamp, data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.changeCursor(null);
    }
}
