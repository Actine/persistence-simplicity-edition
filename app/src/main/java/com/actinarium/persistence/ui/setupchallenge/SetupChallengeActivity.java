package com.actinarium.persistence.ui.setupchallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.BackInterceptor;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.ui.setupchallenge.components.SpecialRuleDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.wizard.ChallengeRemindersFragment;
import com.actinarium.persistence.ui.setupchallenge.wizard.ChallengeWizardAdapter;
import com.actinarium.persistence.ui.setupchallenge.wizard.ChallengeWizardFragment;
import com.actinarium.persistence.ui.setupchallenge.wizard.SettingAGoalFragment;

import java.util.Calendar;


public class SetupChallengeActivity extends AppCompatActivity implements SetupChallengeFragment.Host, ChallengeWizardFragment.Host {

    public static final String ACTION_CREATE = "com.actinarium.persistence.intent.action.CREATE";
    public static final String ACTION_RESTART = "com.actinarium.persistence.intent.action.RESTART";
    public static final String EXTRA_CHALLENGE = "com.actinarium.persistence.intent.extra.CHALLENGE";
    public static final String EXTRA_RUN_WIZARD = "com.actinarium.persistence.intent.extra.RUN_WIZARD";

    private BackInterceptor mInterceptor;
    private boolean mInWizard;

    public static void launchCreate(Context context) {
        Intent intent = new Intent(context, SetupChallengeActivity.class);
        intent.setAction(ACTION_CREATE);
        context.startActivity(intent);
    }

    public static void launchWizard(Context context) {
        Intent intent = new Intent(context, SetupChallengeActivity.class);
        intent.setAction(ACTION_CREATE);
        intent.putExtra(EXTRA_RUN_WIZARD, true);
        context.startActivity(intent);
    }

    public static void launchEdit(Context context, Challenge challenge) {
        Intent intent = new Intent(context, SetupChallengeActivity.class);
        intent.setAction(Intent.ACTION_EDIT);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startActivity(intent);
    }

    public static void launchRestart(Context context, Challenge challenge) {
        Intent intent = new Intent(context, SetupChallengeActivity.class);
        intent.setAction(ACTION_RESTART);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        final Intent intent = getIntent();
        mInWizard = intent.getBooleanExtra(EXTRA_RUN_WIZARD, false);

        if (savedInstanceState != null) {
            return;
        }

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (mInWizard) {
            transaction
                    .add(R.id.fragment_host, new ChallengeWizardFragment(), ChallengeWizardFragment.TAG)
                    .commit();
        } else {
            final Challenge challenge = intent.getParcelableExtra(EXTRA_CHALLENGE);
            final String action = intent.getAction();
            final int actionCode = action.equals(ACTION_CREATE) ? SetupChallengeFragment.ACTION_NEW
                    : action.equals(Intent.ACTION_EDIT) ? SetupChallengeFragment.ACTION_EDIT
                    : SetupChallengeFragment.ACTION_NEW_FROM_PROTO;

            transaction
                    .add(R.id.fragment_host, SetupChallengeFragment.newInstance(challenge, actionCode), SetupChallengeFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        boolean isIntercepted = mInterceptor != null && mInterceptor.onBackPressed();
        if (!isIntercepted) {
            super.onBackPressed();
        }
    }

    /* ------------------- COMING FROM FRAGMENTS ----------------- */

    @Override
    public void onChallengeDiscarded() {
        finish();
    }

    @Override
    public void onWizardChallengeSaved(Challenge challenge) {
        // todo: no need for this callback. also rethink how communication will be implemented when tablet UI is added.
        finish();
    }

    @Override
    public void onCancelChallengeWizard() {
        finish();
    }

    @Override
    public void onCustomUnitAdded(String unit) {
        if (mInWizard) {
            getCwGoalsFragment().onCustomUnitAdded(unit);
        } else {
            getSetupChallengeFragment().onCustomUnitAdded(unit);
        }
    }

    @Override
    public void onCustomUnitDialogDiscarded() {
        if (mInWizard) {
            getCwGoalsFragment().onCustomUnitDialogDiscarded();
        } else {
            getSetupChallengeFragment().onCustomUnitDialogDiscarded();
        }
    }

    @Override
    public void onRuleOverrideAdded(Rule rule) {
        if (mInWizard) {
            getCwGoalsFragment().onRuleOverrideAdded(rule);
        } else {
            getSetupChallengeFragment().onRuleOverrideAdded(rule);
        }
    }

    @Override
    public void onRuleOverrideEdited(Rule rule) {
        if (mInWizard) {
            getCwGoalsFragment().onRuleOverrideEdited(rule);
        } else {
            getSetupChallengeFragment().onRuleOverrideEdited(rule);
        }
    }

    @Override
    public Calendar getCalendarForDatePicker(int id) {
        if (mInWizard) {
            return getCwSpecialRuleDialogFragment().getCalendarForDatePicker(id);
        } else {
            return ((SpecialRuleDialogFragment) getSupportFragmentManager()
                    .findFragmentByTag(SpecialRuleDialogFragment.TAG))
                    .getCalendarForDatePicker(id);
        }
    }

    @Override
    public void saveDatePickerResult(int id, Calendar calendar) {
        if (mInWizard) {
            getCwSpecialRuleDialogFragment().saveDatePickerResult(id, calendar);
        } else {
            ((SpecialRuleDialogFragment) getSupportFragmentManager()
                    .findFragmentByTag(SpecialRuleDialogFragment.TAG))
                    .saveDatePickerResult(id, calendar);
        }
    }

    @Override
    public void saveTimePickerResult(int initTime, int newTime) {
        if (mInWizard) {
            getCwRemindersFragment().saveDatePickerResult(initTime, newTime);
        } else {
            getSetupChallengeFragment().saveDatePickerResult(initTime, newTime);
        }
    }

    @Override
    public void onIncompleteFormConfirmed(boolean noReason, boolean noReminders) {
        getSetupChallengeFragment().onIncompleteFormConfirmed(noReason, noReminders);
    }

    @Override
    public void onStartChallengeConfirmed(Challenge challenge) {
        getSetupChallengeFragment().onStartChallengeConfirmed();
    }

    @Override
    public void setButtonInterceptor(BackInterceptor interceptor) {
        mInterceptor = interceptor;
    }

    @Override
    public Challenge getChallenge() {
        return getChallengeWizardFragment().getChallenge();
    }

    @Override
    public ChallengeWizardAdapter getWizardAdapter() {
        return getChallengeWizardFragment().getWizardAdapter();
    }

    private SetupChallengeFragment getSetupChallengeFragment() {
        return (SetupChallengeFragment) getSupportFragmentManager()
                .findFragmentByTag(SetupChallengeFragment.TAG);
    }

    private ChallengeWizardFragment getChallengeWizardFragment() {
        return (ChallengeWizardFragment) getSupportFragmentManager()
                .findFragmentByTag(ChallengeWizardFragment.TAG);
    }

    private SettingAGoalFragment getCwGoalsFragment() {
        ChallengeWizardFragment fragment = getChallengeWizardFragment();
        if (fragment == null) {
            return null;
        }
        // todo: this is insanely bad, fix me...
        for (Fragment subFragment : fragment.getChildFragmentManager().getFragments()) {
            if (subFragment instanceof SettingAGoalFragment) {
                return (SettingAGoalFragment) subFragment;
            }
        }
        return null;
    }

    private ChallengeRemindersFragment getCwRemindersFragment() {
        ChallengeWizardFragment fragment = getChallengeWizardFragment();
        if (fragment == null) {
            return null;
        }
        // todo: this is insanely bad, fix me...
        for (Fragment subFragment : fragment.getChildFragmentManager().getFragments()) {
            if (subFragment instanceof ChallengeRemindersFragment) {
                return (ChallengeRemindersFragment) subFragment;
            }
        }
        return null;
    }

    private SpecialRuleDialogFragment getCwSpecialRuleDialogFragment() {
        ChallengeWizardFragment fragment = getChallengeWizardFragment();
        if (fragment == null) {
            return null;
        }
        // todo: this is insanely bad, fix me...
        for (Fragment subFragment : fragment.getChildFragmentManager().getFragments()) {
            if (subFragment instanceof SpecialRuleDialogFragment) {
                return (SpecialRuleDialogFragment) subFragment;
            }
        }
        return null;
    }
}
