package com.actinarium.persistence.ui.common;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.actinarium.persistence.R;

/**
 * Dots indicating current page
 *
 * @author Paul Danyliuk
 */
public class PagerIndicator extends View {

    private static final int DEFAULT_DOT_COUNT = 7;
    private static final int DEFAULT_DOT_RADIUS = 4;        // dp
    private static final int DEFAULT_DOT_RADIUS_SMALL = 3;  // dp
    private static final int DEFAULT_DOT_GAP = 4;           // dp

    private int mDotCount;
    private int mDotRadius;
    private int mDotRadiusSmall;
    private int mDotGap;
    private int mSelectedDotColor;
    private int mMutedDotColor;
    private int mWarningDotColor;
    private int mRequiredWidth;
    private int mStartX;           // x position of the center of the first dot
    private int mCenterY;

    private Paint mDotPaint;

    private int mCurrentItem;
    private boolean mIsWarning;

    public PagerIndicator(Context context) {
        super(context);
        init(context);
    }

    public PagerIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PagerIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray array = context.getTheme()
                .obtainStyledAttributes(attrs, R.styleable.PagerIndicator, defStyleAttr, 0);

        mDotCount = array.getInteger(R.styleable.PagerIndicator_dotCount, DEFAULT_DOT_COUNT);
        mSelectedDotColor = array.getColor(R.styleable.PagerIndicator_selectedDotColor, Color.BLACK);
        mWarningDotColor = array.getColor(R.styleable.PagerIndicator_warningDotColor, Color.RED);

        array.recycle();

        init(context);
    }

    private void init(Context context) {
        final Resources resources = context.getResources();
        mMutedDotColor = resources.getColor(R.color.mutedDot);

        final float density = resources.getDisplayMetrics().density;
        mDotRadius = (int) (DEFAULT_DOT_RADIUS * density + 0.5f);
        mDotRadiusSmall = (int) (DEFAULT_DOT_RADIUS_SMALL * density + 0.5f);
        mDotGap = (int) (DEFAULT_DOT_GAP * density + 0.5f);
        mRequiredWidth = getRequiredWidth(mDotRadius, mDotGap, mDotCount);

        mDotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDotPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);

        if (widthMode == MeasureSpec.UNSPECIFIED) {
            width = mRequiredWidth;
            height = 2 * mDotRadius;
        } else if (width < mRequiredWidth) {
            // try to fit
            final float factor = ((float) width) / mRequiredWidth;
            mDotRadius *= factor;
            mDotRadiusSmall *= factor;
            mDotGap *= factor;
            mRequiredWidth = getRequiredWidth(mDotRadius, mDotGap, mDotCount);
        }
        // else if provided width >= required width, we're fine with what we have
        setMeasuredDimension(width, height);

        // Pre-calculate these constants
        mStartX = (width - mRequiredWidth) / 2 + mDotRadius;
        mCenterY = height / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mDotPaint.setColor(mSelectedDotColor);
        int x = mStartX;
        for (int i = 0; i < mDotCount; i++) {
            if (mCurrentItem == i && mIsWarning) {
                mDotPaint.setColor(mWarningDotColor);
            } else if (i == mCurrentItem + 1) {
                mDotPaint.setColor(mMutedDotColor);
            }
            canvas.drawCircle(x, mCenterY, mCurrentItem == i ? mDotRadius : mDotRadiusSmall, mDotPaint);
            x += mDotGap + 2 * mDotRadius;
        }
    }

    public void setDotCount(int count) {
        mDotCount = count;
        mRequiredWidth = getRequiredWidth(mDotRadius, mDotGap, mDotCount);
        requestLayout();
    }

    public void setCurrentItem(int currentItem, boolean isWarning) {
        mCurrentItem = currentItem;
        mIsWarning = isWarning;
        invalidate();
    }

    private static int getRequiredWidth(int dotRadius, int dotGap, int count) {
        return dotRadius * count * 2 + dotGap * (count - 1);
    }
}
