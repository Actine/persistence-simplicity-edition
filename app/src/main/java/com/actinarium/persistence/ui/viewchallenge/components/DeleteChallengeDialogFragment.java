package com.actinarium.persistence.ui.viewchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class DeleteChallengeDialogFragment extends DialogFragment {

    public static final String TAG = "DeleteChallengeDialogFragment";

    public static final String ARG_CHALLENGE_ID = "com.actinarium.persistence.intent.arg.CHALLENGE_ID";

    private Host mHost;
    private Context mContext;

    public static DeleteChallengeDialogFragment newInstance(long challengeId) {
        DeleteChallengeDialogFragment fragment = new DeleteChallengeDialogFragment();
        Bundle args = new Bundle(1);
        args.putLong(ARG_CHALLENGE_ID, challengeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(mContext)
                .setTitle(R.string.dialog_delete_challenge_title)
                .setMessage(R.string.dialog_delete_challenge_message)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onCancelChallengeConfirmed(getArguments().getLong(ARG_CHALLENGE_ID));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {
        void onCancelChallengeConfirmed(long challengeId);
    }

}
