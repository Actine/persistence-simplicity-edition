package com.actinarium.persistence.ui.setupchallenge.wizard;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.actinarium.persistence.common.FragmentStatePagerAdapter;

/**
 * An adapter for a wizard that guides through creating the first challenge. Originally a part of onboarding
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ChallengeWizardAdapter extends FragmentStatePagerAdapter {

    public static final int PAGE_FIRST_CHALLENGE = 0;
    public static final int PAGE_SET_GOALS = 1;
    public static final int PAGE_CALLING_A_DAY = 2;
    public static final int PAGE_REMINDERS = 3;
    public static final int PAGE_STARTING = 4;

    private static final String ARG_PAGE_LOCKS = "com.actinarium.persistence.intent.arg.PAGE_LOCKS";

    private boolean[] mPageLocks = new boolean[]{true, false, false, false, false};
    private PageLockChangeListener mListener;

    private int mCount = 1;

    public ChallengeWizardAdapter(FragmentManager fm, PageLockChangeListener listener) {
        super(fm);
        mListener = listener;
    }

    @Override
    public Parcelable saveState() {
        Bundle bundle = (Bundle) super.saveState();
        bundle.putBooleanArray(ARG_PAGE_LOCKS, mPageLocks);
        return bundle;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        super.restoreState(state, loader);
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            mPageLocks = bundle.getBooleanArray(ARG_PAGE_LOCKS);
            recount();
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PAGE_FIRST_CHALLENGE:
                return new ChallengeTitleFragment();
            case PAGE_SET_GOALS:
                return new SettingAGoalFragment();
            case PAGE_CALLING_A_DAY:
                return new CallingADayFragment();
            case PAGE_REMINDERS:
                return new ChallengeRemindersFragment();
            case PAGE_STARTING:
                return new StartingChallengeFragment();
            default:
                throw new IndexOutOfBoundsException("There's no onboarding page with index " + position);
        }
    }

    public Fragment getExistingItem(int position) {
        return position < mFragments.size() ? mFragments.get(position) : null;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    public void setPageLock(int currentPage, boolean isLocked) {
        mPageLocks[currentPage] = isLocked;
        recount();
    }

    public boolean isLocked(int page) {
        return mPageLocks[page];
    }

    private void recount() {
        int count = PAGE_STARTING + 1;
        for (int i = PAGE_FIRST_CHALLENGE; i <= PAGE_REMINDERS; i++) {
            if (mPageLocks[i]) {
                count = i + 1;
                break;
            }
        }

        if (count != mCount) {
            mCount = count;
            notifyDataSetChanged();
            mListener.onPageLocksChanged();
        }
    }

    public interface PageLockChangeListener {
        void onPageLocksChanged();
    }
}
