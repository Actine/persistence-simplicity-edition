package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class CustomUnitDialogFragment extends DialogFragment {

    public static final String TAG = "CustomUnitDialogFragment";

    private Host mHost;
    private Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        final View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_custom_unit, null);
        final EditText itemType = (EditText) view.findViewById(R.id.custom_item_type);

        builder
                .setTitle(R.string.add_custom_unit)
                .setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onCustomUnitAdded(itemType.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                final Button okButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                if (itemType.getText().toString().trim().isEmpty()) {
                    okButton.setEnabled(false);
                }
                if (itemType.requestFocus()) {
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(itemType, InputMethodManager.SHOW_IMPLICIT);
                }
                itemType.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (s.toString().trim().isEmpty()) {
                            okButton.setEnabled(false);
                        } else {
                            okButton.setEnabled(true);
                        }
                    }
                });
            }
        });
        return alertDialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mHost.onCustomUnitDialogDiscarded();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {

        void onCustomUnitAdded(String unit);
        void onCustomUnitDialogDiscarded();

    }
}
