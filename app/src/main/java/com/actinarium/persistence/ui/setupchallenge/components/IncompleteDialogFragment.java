package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class IncompleteDialogFragment extends DialogFragment {

    public static final String TAG = "IncompleteDialogFragment";

    public static final String ARG_NO_REASON = "com.actinarium.persistence.intent.arg.NO_REASON";
    public static final String ARG_NO_REMINDERS = "com.actinarium.persistence.intent.arg.NO_REMINDERS";

    private Host mHost;
    private Context mContext;

    public static IncompleteDialogFragment newInstance(boolean noReason, boolean noReminders) {
        if (!noReason && !noReminders) {
            return null;
        }
        IncompleteDialogFragment fragment = new IncompleteDialogFragment();
        Bundle args = new Bundle(2);
        args.putBoolean(ARG_NO_REASON, noReason);
        args.putBoolean(ARG_NO_REMINDERS, noReminders);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean noReason = getArguments().getBoolean(ARG_NO_REASON);
        final boolean noReminders = getArguments().getBoolean(ARG_NO_REMINDERS);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        if (noReason && noReminders) {
            String message = getString(
                    R.string.dialog_incomplete_challenge_message_no_reason_and_reminders_joiner,
                    getString(R.string.dialog_incomplete_challenge_message_no_reason),
                    getString(R.string.dialog_incomplete_challenge_message_no_reminders)
            );
            builder
                    .setTitle(R.string.dialog_incomplete_challenge_title_no_reason_and_reminders)
                    .setMessage(message);
        } else if (noReason) {
            builder
                    .setTitle(R.string.dialog_incomplete_challenge_title_no_reason)
                    .setMessage(R.string.dialog_incomplete_challenge_message_no_reason);
        } else {
            builder
                    .setTitle(R.string.dialog_incomplete_challenge_title_no_reminders)
                    .setMessage(R.string.dialog_incomplete_challenge_message_no_reminders);
        }

        builder
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onIncompleteFormConfirmed(noReason, noReminders);
                    }
                })
                .setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {
        void onIncompleteFormConfirmed(boolean noReason, boolean noReminders);
    }

}
