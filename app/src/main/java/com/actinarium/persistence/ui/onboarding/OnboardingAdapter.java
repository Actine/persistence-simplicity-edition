package com.actinarium.persistence.ui.onboarding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.actinarium.persistence.common.FragmentStatePagerAdapter;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class OnboardingAdapter extends FragmentStatePagerAdapter {

    public static final int PAGE_WELCOME = 0;
    public static final int PAGE_ABOUT_PERSISTENCE = 1;
    public static final int PAGE_ANALYTICS = 2;

    public OnboardingAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case PAGE_WELCOME:
                return new WelcomeFragment();
            case PAGE_ABOUT_PERSISTENCE:
                return new AboutPersistenceFragment();
            case PAGE_ANALYTICS:
                return new AnalyticsFragment();
            default:
                throw new IndexOutOfBoundsException("There's no onboarding page with index " + position);
        }
    }

    public Fragment getExistingItem(int position) {
        return position < mFragments.size() ? mFragments.get(position) : null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
