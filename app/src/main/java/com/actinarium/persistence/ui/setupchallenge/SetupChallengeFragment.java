package com.actinarium.persistence.ui.setupchallenge;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.ApplicationContext;
import com.actinarium.persistence.common.BackInterceptor;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.service.PersistenceService;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.settings.PrefUtils;
import com.actinarium.persistence.ui.setupchallenge.components.CustomUnitDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.DatePickerDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.DiscardChangesDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.EditableRemindersController;
import com.actinarium.persistence.ui.setupchallenge.components.IncompleteDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.RuleOverridesController;
import com.actinarium.persistence.ui.setupchallenge.components.SpecialRuleDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.StartChallengeDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.TimePickerDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.UnitSpinnerController;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class SetupChallengeFragment extends Fragment implements RuleOverridesController.Host, EditableRemindersController.Host,
        ApplicationContext.ChallengeSavedListener, BackInterceptor, UnitSpinnerController.Host {

    public static final String TAG = "SetupChallengeFragment";

    // Used to pass a challenge when editing / creating from template
    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    private static final String ARG_ACTION = "com.actinarium.persistence.intent.arg.ACTION";

    private static final String ARG_RULES = "com.actinarium.persistence.intent.arg.RULES";
    private static final String ARG_REMINDERS = "com.actinarium.persistence.intent.arg.REMINDERS";
    private static final String ARG_IS_DIALOG_SHOWN = "com.actinarium.persistence.intent.arg.IS_DIALOG_SHOWN";
    private static final String ARG_IS_TITLE_INVALID = "com.actinarium.persistence.intent.arg.IS_TITLE_INVALID";
    private static final String ARG_OK_WITH_NO_REASON = "com.actinarium.persistence.intent.arg.OK_WITH_NO_REASON";
    private static final String ARG_OK_WITH_NO_REMINDERS = "com.actinarium.persistence.intent.arg.OK_WITH_NO_REMINDERS";
    private static final String ARG_IS_SAVING = "com.actinarium.persistence.intent.arg.IS_SAVING";

    public static final int ACTION_NEW = 0;
    public static final int ACTION_EDIT = 1;
    public static final int ACTION_NEW_FROM_PROTO = 2;

    /** The action that this form is performing at the moment (new challenge, edit challenge, new from prototype) */
    private int mAction;
    /** Proto challenge to fill the form with: the one we're either editing or restarting */
    private Challenge mProtoChallenge;

    private TextView mChallengeNameLabel;
    private EditText mChallengeName;
    private EditText mChallengeDescription;
    private TextView mMeasureUnitsLabel;
    private Spinner mStartingWhen;
    private ImageButton mAddOverrideButton;
    private Button mAddReminderButton;

    private boolean mIsDialogShown;
    private boolean mIsTitleInvalid;

    private boolean mOkWithNoReason;
    private boolean mOkWithNoReminders;
    private boolean mOkWithStartingNow;
    private boolean mIsSaving;

    private UnitSpinnerController mUnitsController;
    private RuleOverridesController mRuleOverridesController;
    private EditableRemindersController mRemindersController;

    private ScrollView mScrollView;
    private View mStartingWhenBlock;
    private View mStartingWhenHairline;
    private View mRootView;


    /**
     * Create a setup challenge fragment for creating a new challenge from empty form state. For editing existing
     * challenge or creating a new one from prototype call {@link #newInstance(Challenge, int)}
     *
     * @return Fragment instance
     */
    public static SetupChallengeFragment newInstance() {
        return new SetupChallengeFragment();
    }

    /**
     * Create a setup challenge fragment for either new challenge, edit existing non-stopped challenge, or restart
     * stopped challenge
     *
     * @param challenge <code>null</code> for creating new challenge. If challenge is provided, then if {@link
     *                  Challenge#isOver()} create a new challenge with all fields pre-initialized from this one,
     *                  otherwise edit provided challenge
     * @return Fragment instance
     */
    public static SetupChallengeFragment newInstance(@NonNull Challenge challenge, @ChallengeAction int action) {
        SetupChallengeFragment fragment = new SetupChallengeFragment();
        Bundle args = new Bundle(2);
        args.putParcelable(ARG_CHALLENGE, challenge);
        args.putInt(ARG_ACTION, action);
        fragment.setArguments(args);
        return fragment;
    }

    public SetupChallengeFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mRuleOverridesController != null) {
            Rule[] rules = mRuleOverridesController.getRules();
            Reminder[] reminders = mRemindersController.getReminders();
            outState.putParcelableArray(ARG_RULES, rules);
            outState.putParcelableArray(ARG_REMINDERS, reminders);
        }
        outState.putBoolean(ARG_IS_TITLE_INVALID, mIsTitleInvalid);
        outState.putBoolean(ARG_OK_WITH_NO_REASON, mOkWithNoReason);
        outState.putBoolean(ARG_OK_WITH_NO_REMINDERS, mOkWithNoReminders);
        outState.putBoolean(ARG_IS_DIALOG_SHOWN, mIsDialogShown);
        outState.putBoolean(ARG_IS_SAVING, mIsSaving);
    }

    @Override
    @SuppressWarnings({"SuspiciousSystemArraycopy", "ConstantConditions"})
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Resolve saved instance state (sans reminders and challenges - those are resolved conditionally later)
        final boolean hasInstanceState = savedInstanceState != null;
        if (hasInstanceState) {
            mIsDialogShown = savedInstanceState.getBoolean(ARG_IS_DIALOG_SHOWN, false);
            mOkWithNoReason = savedInstanceState.getBoolean(ARG_OK_WITH_NO_REASON, false);
            mOkWithNoReminders = savedInstanceState.getBoolean(ARG_OK_WITH_NO_REMINDERS, false);
            mIsTitleInvalid = savedInstanceState.getBoolean(ARG_IS_TITLE_INVALID, false);
            mIsSaving = savedInstanceState.getBoolean(ARG_IS_SAVING, false);
        }

        // Resolve arguments (challenge and action)
        if (getArguments() != null) {
            mProtoChallenge = getArguments().getParcelable(ARG_CHALLENGE);
            mAction = getArguments().getInt(ARG_ACTION, ACTION_NEW);
        } else {
            mAction = ACTION_NEW;
        }

        mRootView = inflater.inflate(R.layout.fragment_setup_challenge, container, false);
        final Activity activity = getActivity();

        // Set up toolbar
        final int title = mAction == ACTION_EDIT ? R.string.edit_challenge : R.string.new_challenge;
        final ActionBar actionBar = ViewUtils.setUpToolbar(activity, mRootView, title, 0);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        actionBar.setHomeActionContentDescription(R.string.accessibility_discard_close);

        // Set up coordination between toolbar and scroll view
        mScrollView = (ScrollView) mRootView.findViewById(R.id.scrollView);
        ViewUtils.wire(actionBar, mScrollView, getResources().getDimension(R.dimen.actionBarElevation));

        // Set up form controls (don't hide anything yet)...
        setupNameAndDescription();
        setupUnitsControl(activity);
        setupRulesControl(activity);
        setupRemindersControl(activity);
        setupStartingControl();

        // ...and populate them
        // If prototype challenge is present, follow one path
        if (mProtoChallenge != null) {
            // Adjust the form (e.g. hide/disable fields if this is editing session)
            adjustFormForChallenge(mProtoChallenge);
            if (!hasInstanceState) {
                // If displaying for the first time and not after config change, fill the form from the proto
                fillFormFromChallenge(mProtoChallenge);
            } else {
                // Restore saved state (assume state is properly saved)
                Parcelable[] rulesRaw = savedInstanceState.getParcelableArray(ARG_RULES);
                Rule[] rules = new Rule[rulesRaw.length];
                System.arraycopy(rulesRaw, 0, rules, 0, rulesRaw.length);

                Parcelable[] remindersRaw = savedInstanceState.getParcelableArray(ARG_REMINDERS);
                Reminder[] reminders = new Reminder[remindersRaw.length];
                System.arraycopy(remindersRaw, 0, reminders, 0, remindersRaw.length);

                mRuleOverridesController.setRules(rules);
                mRemindersController.setReminders(reminders);
            }
        } else {
            // Fill the form with defaults and/or restore on subsequent launches
            if (hasInstanceState && savedInstanceState.containsKey(ARG_RULES)) {
                Parcelable[] rulesRaw = savedInstanceState.getParcelableArray(ARG_RULES);
                Rule[] rules = new Rule[rulesRaw.length];
                System.arraycopy(rulesRaw, 0, rules, 0, rulesRaw.length);
                mRuleOverridesController.setRules(rules);
            } else {
                mRuleOverridesController.setDefaultRule(Rule.getDefaultRule());
            }
            if (hasInstanceState && savedInstanceState.containsKey(ARG_REMINDERS)) {
                Parcelable[] remindersRaw = savedInstanceState.getParcelableArray(ARG_REMINDERS);
                Reminder[] reminders = new Reminder[remindersRaw.length];
                System.arraycopy(remindersRaw, 0, reminders, 0, remindersRaw.length);
                mRemindersController.setReminders(reminders);
            }
        }

        return mRootView;
    }

    /**
     * Will hide certain fields / make inactive based on challenge state.
     * Call this when setting up the form for editing the challenge, each time on configuration change.
     * @param challenge Challenge to adjust this form to
     */
    private void adjustFormForChallenge(@NonNull Challenge challenge) {
        if (mAction == ACTION_EDIT && challenge.isActive()) {
            // Disable measures spinner but keep it visible
            mUnitsController.getSpinner().setVisibility(View.GONE);
            mMeasureUnitsLabel.setVisibility(View.VISIBLE);

            // Hide "starting today/later" block and suppress "starting now?" prompt
            mStartingWhenBlock.setVisibility(View.GONE);
            mStartingWhenHairline.setVisibility(View.GONE);
            mOkWithStartingNow = true;

            // If rules are locked (i.e. < 7 days since last edit), rewire overrides controller to display readonly
            if (!challenge.areRulesEditableOn(Utils.getCurrentEpochDay(getActivity()))) {
                // todo: display message about locked rules and when they'll become editable
                mRuleOverridesController.setItemLayoutId(R.layout.item_challenge_rule_readonly);
                mAddOverrideButton.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Fill form from challenge (title, description, units etc). Call this only once.
     */
    private void fillFormFromChallenge(@NonNull Challenge challenge) {
        mChallengeName.setText(challenge.title);
        mChallengeDescription.setText(challenge.description);

        // Set units. Using this method will select unit if existing, or create a custom unit from this challenge.
        // Also this will automatically set the unit to rules controller
        mUnitsController.onCustomUnitAdded(challenge.units);
        if (mMeasureUnitsLabel.getVisibility() == View.VISIBLE) {
            mMeasureUnitsLabel.setText(challenge.units);
        }

        // Fill the controllers. For rules, make an array of copies, otherwise original objects will be edited
        Rule[] rulesSrc = challenge.getRules();
        final int length = rulesSrc.length;
        Rule[] rulesDst = new Rule[length];
        for (int i = 0; i < length; i++) {
            rulesDst[i] = new Rule(rulesSrc[i]);
        }
        mRuleOverridesController.setRules(rulesDst);
        mRemindersController.setReminders(challenge.getReminders());

        // If we're dealing with pending challenge (not started nor over), set starting now spinner to later
        if (mAction == ACTION_EDIT && !challenge.isActive() && !challenge.isOver()) {
            mStartingWhen.setSelection(Challenge.STARTING_LATER);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_just_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                saveChallenge();
                return true;
            case android.R.id.home:
                // Override "home" transition (since "up/home" is now [X] as in dialog)
                // Also ask if we're discarding modified challenge
                if (!onDiscardChallengeAction()) {
                    getActivity().finish();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        ApplicationContext.get().setChallengeSavedListener(this);
        ((Host) getActivity()).setButtonInterceptor(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ApplicationContext.get().setChallengeSavedListener(null);
        ((Host) getActivity()).setButtonInterceptor(null);
    }

    /**
     * Save new challenge / save edits to existing challenge
     */
    private void saveChallenge() {
        if (mIsSaving) {
            // Suppress duplicate save clicks
            Toast.makeText(getActivity(), R.string.already_saving, Toast.LENGTH_SHORT).show();
            return;
        }

        mIsSaving = true;

        // OK, for the sake of clean code let's assume the best scenario and read all fields, and validate later
        final int today = Utils.getCurrentEpochDay(getContext());

        Challenge challenge = new Challenge();
        challenge.title = mChallengeName.getText().toString().trim();
        challenge.description = mChallengeDescription.getText().toString().trim();
        if (challenge.description.isEmpty()) {
            challenge.description = null;
        }
        // Read rules and reminders later, after validation
        // Read units and starting mode later, only for create mode

        // Validate challenge title. If it's invalid - highlight, scroll, show toast
        if (challenge.title.isEmpty()) {
            if (!mIsTitleInvalid) {
                mIsTitleInvalid = true;
                updateTitleValidationState();
            }
            mScrollView.smoothScrollTo(0, 0);
            Toast.makeText(getActivity(), R.string.invalid_title, Toast.LENGTH_SHORT).show();
            mIsSaving = false;
            return;
        }

        // Now if title is OK, check if any of rules is highlighted as invalid, and scroll there
        View invalidRuleView = mRuleOverridesController.getFirstInvalidView();
        if (invalidRuleView != null) {
            // Scroll the field into view
            if (Build.VERSION.SDK_INT >= 18) {
                invalidRuleView.requestRectangleOnScreen(new Rect(
                        invalidRuleView.getLeft(),
                        invalidRuleView.getTop(),
                        invalidRuleView.getRight(),
                        invalidRuleView.getBottom()
                ));
            } else {
                invalidRuleView.requestFocus();
            }
            Toast.makeText(getActivity(), R.string.invalid_rule, Toast.LENGTH_SHORT).show();
            mIsSaving = false;
            return;
        }

        // OK, rules are valid. Time to read them, and also reminders
        final Rule[] rules = mRuleOverridesController.getRules();
        final Reminder[] reminders = mRemindersController.getReminders();

        // Show alert that either description or reminders is empty, and halt execution for now
        final boolean alertNoReason = !mOkWithNoReason && challenge.description == null;
        final boolean alertNoReminders = !mOkWithNoReminders && reminders.length == 0;
        IncompleteDialogFragment incompleteFormAlert = IncompleteDialogFragment.newInstance(alertNoReason, alertNoReminders);
        if (incompleteFormAlert != null) {
            incompleteFormAlert.show(getFragmentManager(), IncompleteDialogFragment.TAG);
            mIsSaving = false;
            return;
        }

        // Now, those were common things. Onward three scenarios are possible: new challenge, edit inactive, edit active

        if (mAction == ACTION_EDIT && mProtoChallenge.isActive()) {
            // EDIT ACTIVE

            // Need ID for update operation; cannot omit flags
            challenge.id = mProtoChallenge.id;
            challenge.statusFlags = mProtoChallenge.statusFlags;
            // Omitting nullable: units, startedOn, prototypeId, stoppedOn, stopReason (and createdOn, which is not included in update op)

            // Include rules if changed, otherwise leave null and they won't be updated
            if (!Arrays.equals(rules, mProtoChallenge.getRules())
                    && challenge.areRulesEditableOn(Utils.getCurrentEpochDay(getActivity()))) {
                // todo: display alert, just like with starting now etc
                challenge.setRules(rules);
                challenge.rulesSetOn = today;
            } else {
                // must preserve this
                challenge.rulesSetOn = mProtoChallenge.rulesSetOn;
            }

            // Include reminders if changed, otherwise leave null and they won't be updated
            if (!Arrays.equals(reminders, mProtoChallenge.getReminders())) {
                challenge.setReminders(reminders);
            }

            PersistenceService.updateChallenge(getContext(), challenge);
        } else if (mAction == ACTION_EDIT) {
            // EDIT PENDING

            // todo: this is all duplicated in the 'else' block - maybe extract?
            final int startingMode = mStartingWhen.getSelectedItemPosition();

            // Pending challenge may have units changed, and besides we need it for starting now prompt
            challenge.units = String.valueOf(mUnitsController.getSpinner().getSelectedItem());

            if (startingMode == Challenge.STARTING_NOW && !mOkWithStartingNow) {
                StartChallengeDialogFragment startingChallengeAlert = StartChallengeDialogFragment.newInstance(true, challenge);
                startingChallengeAlert.show(getFragmentManager(), StartChallengeDialogFragment.TAG);
                mIsSaving = false;
                return;
            }

            // Need ID for update operation
            challenge.id = mProtoChallenge.id;

            // Update rules and reminders if required
            if (!Arrays.equals(rules, mProtoChallenge.getRules())) {
                challenge.setRules(rules);
            }
            if (!Arrays.equals(reminders, mProtoChallenge.getReminders())) {
                challenge.setReminders(reminders);
            }

            // Again, maybe we're starting challenge in this edit?
            if (startingMode == Challenge.STARTING_NOW) {
                challenge.statusFlags = mProtoChallenge.statusFlags | Challenge.FLAG_ACTIVE;
                challenge.rulesSetOn = today;
                challenge.startedOn = today;
            } else {
                // Cannot omit flags and rulesSetOn even if they're unchanged
                challenge.statusFlags = mProtoChallenge.statusFlags;
                challenge.rulesSetOn = mProtoChallenge.rulesSetOn;
            }

            PersistenceService.updateChallenge(getContext(), challenge);

        } else {
            // CREATE NEW (proto or not - doesn't matter, reading the form here)

            final int startingMode = mStartingWhen.getSelectedItemPosition();
            challenge.units = String.valueOf(mUnitsController.getSpinner().getSelectedItem());

            if (startingMode == Challenge.STARTING_NOW && !mOkWithStartingNow) {
                StartChallengeDialogFragment startingChallengeAlert = StartChallengeDialogFragment.newInstance(true, challenge);
                startingChallengeAlert.show(getFragmentManager(), StartChallengeDialogFragment.TAG);
                mIsSaving = false;
                return;
            }

            challenge.createdOn = today;
            challenge.rulesSetOn = today;
            challenge.setRules(rules);
            challenge.setReminders(reminders);

            if (startingMode == Challenge.STARTING_NOW) {
                challenge.statusFlags = Challenge.FLAG_DISPLAYED | Challenge.FLAG_ACTIVE;
                challenge.startedOn = today;
            } else {
                challenge.statusFlags = Challenge.FLAG_DISPLAYED;
            }

            // Listener is already set (ApplicationContext), so just make sure the status is put in by the service
            PersistenceService.createChallenge(getContext(), challenge, true);
        }
    }

    public boolean onDiscardChallengeAction() {
        final boolean isEdit = mAction == ACTION_EDIT;

        if (hasFormChanged()) {
            DiscardChangesDialogFragment fragment = DiscardChangesDialogFragment.newInstance(isEdit);
            fragment.show(getFragmentManager(), DiscardChangesDialogFragment.TAG);
            return true;
        }

        // If this was "new challenge" (or new from proto) - display toast that "empty challenge discarded"
        if (!isEdit) {
            ApplicationContext.get().setEmptyChallengeDiscarded();
        }

        return false;
    }

    /**
     * Verify if the form was changed. Or, specifically, verifies if there were important user changes to this form,
     * so that discarding it without prompt would be destructive. If {@link #mAction} is {@link #ACTION_EDIT}, will
     * compare this form with {@link #mProtoChallenge}, otherwise ((even if action is {@link #ACTION_NEW_FROM_PROTO}) -
     * with default empty state.
     *
     * @return true if form should not be silently discarded.
     */
    private boolean hasFormChanged() {
        final Rule[] rules = mRuleOverridesController.getRules();
        final Reminder[] reminders = mRemindersController.getReminders();
        if (mAction == ACTION_EDIT && mProtoChallenge != null) {
            boolean equal = mChallengeName.getText().toString().trim().equals(mProtoChallenge.title)
                    && ((mProtoChallenge.description == null && mChallengeDescription.getText().length() == 0)
                        || mChallengeDescription.getText().toString().trim().equals(mProtoChallenge.description))
                    && mProtoChallenge.units.equals(String.valueOf(mUnitsController.getSpinner().getSelectedItem()))
                    && Arrays.equals(mProtoChallenge.getRules(), rules)
                    && Arrays.equals(mProtoChallenge.getReminders(), reminders)
                    && (mProtoChallenge.isActive() || mStartingWhen.getSelectedItemPosition() == Challenge.STARTING_LATER);
            return !equal;
        } else {
            return mChallengeName.length() != 0
                    || mChallengeDescription.length() != 0
                    || mUnitsController.getSpinner().getSelectedItemPosition() != 0
                    || rules.length != 1
                    || rules[0].quantity != Rule.DEFAULT_QUANTITY
                    || reminders.length != 0
                    || mStartingWhen.getSelectedItemPosition() != Challenge.STARTING_NOW;
        }
    }

    /**
     * Call this when title validation state changes. DON'T CALL MULTIPLE TIMES WHEN INVALID
     */
    private void updateTitleValidationState() {
        if (mIsTitleInvalid) {
            // Change label color and title underline tint
            ViewCompat.setBackgroundTintList(
                    mChallengeName,
                    ColorStateList.valueOf(getResources().getColor(R.color.warningOnDark))
            );
            mChallengeNameLabel.setTextColor(getResources().getColor(R.color.warningOnDark));

            // Add text listener
            mChallengeName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().trim().isEmpty()) {
                        mChallengeName.removeTextChangedListener(this);
                        mIsTitleInvalid = false;
                        updateTitleValidationState();
                    }
                }
            });
        } else {
            // Reset title tints to normal
            ViewCompat.setBackgroundTintList(
                    mChallengeName,
                    ColorStateList.valueOf(getResources().getColor(R.color.accentBright))
            );
            mChallengeNameLabel.setTextColor(getResources().getColor(R.color.accentBright));
        }
    }

    @Override
    public boolean isDialogShown() {
        return mIsDialogShown;
    }

    @Override
    public void setIsDialogShown(boolean isShown) {
        this.mIsDialogShown = isShown;
    }

    public void onCustomUnitDialogDiscarded() {
        mUnitsController.resetSpinnerToLastSelection();
        mIsDialogShown = false;
    }

    public void onCustomUnitAdded(String unit) {
        mUnitsController.onCustomUnitAdded(unit);
    }

    @Override
    public void onUnitSelected(String unit) {
        mRuleOverridesController.setUnitsText(unit);
    }

    public void onRuleOverrideAdded(Rule rule) {
        mRuleOverridesController.addRule(rule);
    }

    public void onRuleOverrideEdited(Rule rule) {
        mRuleOverridesController.editRule(rule);
    }

    @Override
    public void requestVerticalScroll(int offset, boolean force) {
        int requiredOffset;
        if (force) {
            requiredOffset = offset;
        } else {
            int remainder = mScrollView.getChildAt(0).getBottom() - mScrollView.getScrollY() - mScrollView.getHeight();
            if (remainder > -offset) {
                requiredOffset = 0;
            } else {
                requiredOffset = offset + remainder;
            }
        }
        if (requiredOffset != 0) {
            mScrollView.smoothScrollBy(0, requiredOffset);
        }
    }

    @Override
    public void openEditRuleDialog(Rule rule) {
        SpecialRuleDialogFragment specialRuleDialogFragment = SpecialRuleDialogFragment.newInstance(rule, true);
        specialRuleDialogFragment.show(getFragmentManager(), SpecialRuleDialogFragment.TAG);
    }

    @Override
    public void requestValidityCheck(boolean knownToBeInvalid) {
        // No-op since validation will be performed when save is clicked
    }

    public void saveDatePickerResult(int oldTime, int newTime) {
        if (oldTime == TimePickerDialogFragment.NO_TIME) {
            mRemindersController.addReminder(newTime);
        } else {
            mRemindersController.updateReminder(oldTime, newTime);
        }
    }

    @Override
    public void onDuplicateReminder() {
        Toast.makeText(getActivity(), R.string.reminder_already_set, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editReminderTime(int oldTime) {
        TimePickerDialogFragment timePickerDialogFragment = TimePickerDialogFragment.newInstance(oldTime);
        timePickerDialogFragment.show(getFragmentManager(), TimePickerDialogFragment.TAG);
    }

    @Override
    public void updateAddReminderButtonText(boolean hasReminders) {
        mAddReminderButton.setText(hasReminders ? R.string.add_reminder : R.string.add_reminder_first);
    }

    public void onIncompleteFormConfirmed(boolean noReason, boolean noReminders) {
        mOkWithNoReason |= noReason;
        mOkWithNoReminders |= noReminders;
        saveChallenge();
    }

    public void onStartChallengeConfirmed() {
        mOkWithStartingNow = true;
        saveChallenge();
    }

    @Override
    public void onChallengeSaved(Challenge challenge) {
        mIsSaving = false;

        // Remember that the user is now educated in how to create a challenge
        PrefUtils.setCreatedChallenge(getContext(), true);

        // !!! previously it launched view challenge screen for created challenge. let's show home now
        // todo: in home, scroll to freshly created challenge
        getActivity().finish();
    }

    @Override
    public boolean onBackPressed() {
        return onDiscardChallengeAction();
    }

    /**
     * An aggregate interface, which assembles all methods that Activity hosting setup challenge dialog must implement
     */
    public interface Host extends CustomUnitDialogFragment.Host, DiscardChangesDialogFragment.Host,
            SpecialRuleDialogFragment.Host, DatePickerDialogFragment.Host, TimePickerDialogFragment.Host,
            IncompleteDialogFragment.Host, StartChallengeDialogFragment.Host {

        void setButtonInterceptor(BackInterceptor interceptor);
    }

    // region Setting up the form internals

    private void setupNameAndDescription() {
        mChallengeNameLabel = (TextView) mRootView.findViewById(R.id.challenge_name_label);
        mChallengeName = (EditText) mRootView.findViewById(R.id.challenge_name);
        mChallengeName.setMaxLines(Integer.MAX_VALUE);
        mChallengeName.setHorizontallyScrolling(false);

        mChallengeDescription = (EditText) mRootView.findViewById(R.id.challenge_description);
        mChallengeDescription.setMaxLines(Integer.MAX_VALUE);
        mChallengeDescription.setHorizontallyScrolling(false);

        updateTitleValidationState();
    }

    private void setupUnitsControl(Context context) {
        mUnitsController = new UnitSpinnerController(context, getFragmentManager(), this, (Spinner) mRootView.findViewById(R.id.challenge_item_type));
        mMeasureUnitsLabel = (TextView) mRootView.findViewById(R.id.measure_units_label);
    }

    private void setupRulesControl(Context context) {
        final LinearLayout overridesContainer = (LinearLayout) mRootView.findViewById(R.id.challenge_goal_rules);

        mRuleOverridesController = new RuleOverridesController(this, context, overridesContainer, R.layout.item_challenge_rule);

        mAddOverrideButton = (ImageButton) mRootView.findViewById(R.id.add_override);
        //noinspection deprecation
        mAddOverrideButton.setAlpha(Utils.DEFAULT_ICON_ALPHA);
        mAddOverrideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpecialRuleDialogFragment specialRuleDialogFragment = SpecialRuleDialogFragment.newInstance(Rule.getDefaultOverrideRule(), false);
                specialRuleDialogFragment.show(getFragmentManager(), SpecialRuleDialogFragment.TAG);
            }
        });
    }

    private void setupRemindersControl(Context context) {
        mAddReminderButton = (Button) mRootView.findViewById(R.id.add_reminder);
        mAddReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timePickerDialogFragment = TimePickerDialogFragment.newInstance(TimePickerDialogFragment.NO_TIME);
                timePickerDialogFragment.show(getFragmentManager(), TimePickerDialogFragment.TAG);
            }
        });

        final LinearLayout remindersContainer = (LinearLayout) mRootView.findViewById(R.id.challenge_reminders);
        mRemindersController = new EditableRemindersController(this, context, remindersContainer);
    }

    private void setupStartingControl() {
        mStartingWhenBlock = mRootView.findViewById(R.id.starting_when_block);
        mStartingWhenHairline = mRootView.findViewById(R.id.pre_starting_when_hairline);
        mStartingWhen = (Spinner) mRootView.findViewById(R.id.challenge_starting_when);
    }

    // endregion

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ACTION_NEW, ACTION_NEW_FROM_PROTO, ACTION_EDIT})
    @interface ChallengeAction {
    }

}
