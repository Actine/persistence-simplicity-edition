package com.actinarium.persistence.ui.home.components;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.actinarium.persistence.R;

/**
* <p></p>
*
* @author Paul Danyliuk
* @version $Id$
*/
public class EmptyStateCardHolder extends RecyclerView.ViewHolder {

    public EmptyStateCardHolder(final View itemView, final Host host, Context context, boolean isWelcome) {
        super(itemView);

        TextView title = (TextView) itemView.findViewById(R.id.title);
        TextView message = (TextView) itemView.findViewById(R.id.message);
        Button addChallenge = (Button) itemView.findViewById(R.id.add_challenge);

        ViewCompat.setBackgroundTintList(addChallenge, ColorStateList.valueOf(context.getResources().getColor(R.color.primaryLight1)));

        if (isWelcome) {
            title.setText(context.getString(R.string.get_started_title));
            message.setText(context.getText(R.string.get_started_message_1));
        } else {
            title.setText(context.getString(R.string.no_challenges_title));
            message.setText(context.getString(R.string.no_challenges_message));
        }

        addChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Let's assume mProgress is initialized at the moment
                host.onAddChallengeClicked();
            }
        });
    }

    public interface Host {
        void onAddChallengeClicked();
    }
}
