package com.actinarium.persistence.ui.common;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.widget.LinearLayout;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.ui.settings.PrefUtils;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 */
public abstract class RemindersController {
    public static final int DAY_IN_MINUTES = 1440;

    protected Context mContext;
    protected LinearLayout mParentLayout;
    protected SparseArray<ReminderHolder> mReminders;
    protected int mSize = 0;
    protected int mDayStart;

    public RemindersController(LinearLayout parentLayout, Context context) {
        mParentLayout = parentLayout;
        mContext = context;
        mDayStart = PrefUtils.getNewDayOffset(context);
        mReminders = new SparseArray<>();
    }

    public Reminder[] getReminders() {
        Reminder[] reminders = new Reminder[mSize];
        for (int i = 0; i < mSize; i++) {
            ReminderHolder holder = mReminders.valueAt(i);
            reminders[i] = new Reminder(holder.getTime(), holder.getType());
        }
        return reminders;
    }

    public void setReminders(Reminder[] reminders) {
        // Clean up everything
        mParentLayout.removeAllViews();
        mReminders.clear();

        mSize = reminders.length;
        onRemindersChanged();

        for (int i = 0; i < mSize; i++) {
            final int time = reminders[i].time;
            ReminderHolder holder = createHolder(time, reminders[i].getFlags());

            // Since we know that reminders are ordered, we can optimize for appending them
            final int key = time < mDayStart ? time + DAY_IN_MINUTES : time;
            mReminders.append(key, holder);
            mParentLayout.addView(holder.getView());
        }
    }

    protected abstract void onRemindersChanged();

    protected abstract ReminderHolder createHolder(int time, int type);

    public interface ReminderHolder {
        int getType();
        int getTime();
        void setTime(int time);
        View getView();
    }
}
