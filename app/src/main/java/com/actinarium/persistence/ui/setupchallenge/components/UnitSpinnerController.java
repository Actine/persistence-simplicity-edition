package com.actinarium.persistence.ui.setupchallenge.components;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.settings.PrefUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class UnitSpinnerController implements AdapterView.OnItemSelectedListener {

    private Host mHost;
    private Context mContext;
    private FragmentManager mFragmentManager;
    private Spinner mSpinner;
    private SharedPreferences mPreferences;

    private int mLastSelectedUnitPosition = 0;
    private ArrayAdapter<String> mUnitSpinnerAdapter;

    public UnitSpinnerController(Context context, FragmentManager fragmentManager, Host host, Spinner spinner) {
        mContext = context;
        mFragmentManager = fragmentManager;
        mHost = host;
        mSpinner = spinner;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        mUnitSpinnerAdapter = new ArrayAdapter<>(
                mContext,
                android.R.layout.simple_spinner_item,
                getAllUnits()
        );
        mUnitSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mUnitSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == parent.getCount() - 1) {
            if (!mHost.isDialogShown()) {
                // todo: it's not OK to keep it here - this is probably host's responsibility to manage additional dialogs
                DialogFragment fragment = new CustomUnitDialogFragment();
                fragment.show(mFragmentManager, CustomUnitDialogFragment.TAG);
                mHost.setIsDialogShown(true);
            }
        } else {
            mHost.onUnitSelected(parent.getAdapter().getItem(position).toString());
            mLastSelectedUnitPosition = position;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    public void onCustomUnitAdded(String unit) {
        String result = unit.replace(PrefUtils.CUSTOM_UNITS_DELIMITER, ' ').trim();
        int position = mUnitSpinnerAdapter.getPosition(result);
        if (position == -1) {
            position = mUnitSpinnerAdapter.getCount() - 1;
            mUnitSpinnerAdapter.insert(result, position);

            String resultingString = getUnitsString();
            if (resultingString == null) {
                resultingString = result;
            } else {
                resultingString += PrefUtils.CUSTOM_UNITS_DELIMITER + result;
            }
            // todo: extract all custom units logic into PrefUtils class or such
            mPreferences
                    .edit()
                    .putString(PrefUtils.SETTING_CUSTOM_UNITS, resultingString)
                    .apply();
        }
        mSpinner.setSelection(position);
        mHost.onUnitSelected(unit);
        mLastSelectedUnitPosition = position;
        mHost.setIsDialogShown(false);
    }

    public Spinner getSpinner() {
        return mSpinner;
    }

    public void resetSpinnerToLastSelection() {
        mSpinner.setSelection(mLastSelectedUnitPosition);
    }

    private List<String> getAllUnits() {
        String[] defaultOptions = mContext.getResources().getStringArray(R.array.challenge_default_units);
        String custom = mContext.getString(R.string.custom);
        String customOptions = getUnitsString();

        Set<String> options = new LinkedHashSet<>();
        options.addAll(Arrays.asList(defaultOptions));
        if (customOptions != null) {
            TextUtils.StringSplitter splitter = new TextUtils.SimpleStringSplitter(PrefUtils.CUSTOM_UNITS_DELIMITER);
            splitter.setString(customOptions);
            for (String s : splitter) {
                options.add(s);
            }
        }
        options.add(custom);
        return new ArrayList<>(options);
    }

    private String getUnitsString() {
        return mPreferences.getString(PrefUtils.SETTING_CUSTOM_UNITS, null);
    }

    public interface Host {
        void onUnitSelected(String unit);
        boolean isDialogShown();
        void setIsDialogShown(boolean isShown);
    }
}
