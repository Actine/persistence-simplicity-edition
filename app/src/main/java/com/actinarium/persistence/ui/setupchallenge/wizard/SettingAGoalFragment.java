package com.actinarium.persistence.ui.setupchallenge.wizard;


import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.ui.setupchallenge.components.RuleOverridesController;
import com.actinarium.persistence.ui.setupchallenge.components.SpecialRuleDialogFragment;
import com.actinarium.persistence.ui.setupchallenge.components.UnitSpinnerController;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingAGoalFragment extends Fragment implements RuleOverridesController.Host, UnitSpinnerController.Host,
        ChallengeWizardFragment.ModifiesChallenge {

    public static final String TAG = "SettingAGoalFragment";

    private static final String ARG_IS_DIALOG_SHOWN = "com.actinarium.persistence.intent.arg.IS_DIALOG_SHOWN";
    private static final String ARG_RULES = "com.actinarium.persistence.intent.arg.RULES";

    private ChallengeWizardFragment.Host mHost;
    private ChallengeWizardAdapter mAdapter;

    private boolean mIsDialogShown;

    private UnitSpinnerController mSpinnerController;
    private RuleOverridesController mRuleOverridesController;
    private ScrollView mScrollView;

    public SettingAGoalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHost = (ChallengeWizardFragment.Host) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ARG_IS_DIALOG_SHOWN, mIsDialogShown);
        outState.putParcelableArray(ARG_RULES, mRuleOverridesController.getRules());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mAdapter = mHost.getWizardAdapter();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cw_setting_goal, container, false);
        mScrollView = (ScrollView) view.findViewById(R.id.scrollView);

        /* ---------------- MEASURE PROGRESS IN ---------------- */

        mSpinnerController = new UnitSpinnerController(getActivity(), getFragmentManager(), this, (Spinner) view.findViewById(R.id.challenge_item_type));

        /* ---------------- SET DAILY GOALS SECTION ---------------- */

        final LinearLayout overridesContainer = (LinearLayout) view.findViewById(R.id.challenge_goal_rules);

        mRuleOverridesController = new RuleOverridesController(this, getActivity(), overridesContainer, R.layout.item_challenge_rule);

        if (savedInstanceState != null) {
            Parcelable[] rulesRaw = savedInstanceState.getParcelableArray(ARG_RULES);
            Rule[] rules = new Rule[rulesRaw.length];
            System.arraycopy(rulesRaw, 0, rules, 0, rulesRaw.length);
            mRuleOverridesController.setRules(rules);
            mIsDialogShown = savedInstanceState.getBoolean(ARG_IS_DIALOG_SHOWN, false);
        } else {
            mRuleOverridesController.setRules(mHost.getChallenge().getRules());
        }

        final ImageButton addOverrideButton = (ImageButton) view.findViewById(R.id.add_override);
        //noinspection deprecation
        addOverrideButton.setAlpha(Utils.DEFAULT_ICON_ALPHA);
        addOverrideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpecialRuleDialogFragment specialRuleDialogFragment = SpecialRuleDialogFragment.newInstance(Rule.getDefaultOverrideRule(), false);
                specialRuleDialogFragment.show(getFragmentManager(), SpecialRuleDialogFragment.TAG);
            }
        });

        return view;
    }

    @Override
    public void writeToChallenge(Challenge challenge) {
        // Save units and rules into challenge
        challenge.units = String.valueOf(mSpinnerController.getSpinner().getSelectedItem());
        challenge.setRules(mRuleOverridesController.getRules());
    }

    @Override
    public void requestVerticalScroll(int offset, boolean force) {
        // todo: externalize this somewhere - duplicate code block
        int requiredOffset;
        if (force) {
            requiredOffset = offset;
        } else {
            int remainder = mScrollView.getChildAt(0).getBottom() - mScrollView.getScrollY() - mScrollView.getHeight();
            if (remainder > -offset) {
                requiredOffset = 0;
            } else {
                requiredOffset = offset + remainder;
            }
        }
        if (requiredOffset != 0) {
            mScrollView.smoothScrollBy(0, requiredOffset);
        }
    }

    @Override
    public void openEditRuleDialog(Rule rule) {
        SpecialRuleDialogFragment specialRuleDialogFragment = SpecialRuleDialogFragment.newInstance(rule, true);
        specialRuleDialogFragment.show(getFragmentManager(), SpecialRuleDialogFragment.TAG);
    }

    @Override
    public void requestValidityCheck(boolean knownToBeInvalid) {
        final boolean isInvalid = knownToBeInvalid || !mRuleOverridesController.isValid();
        mAdapter.setPageLock(ChallengeWizardAdapter.PAGE_SET_GOALS, isInvalid);
    }

    @Override
    public void onUnitSelected(String unit) {
        mRuleOverridesController.setUnitsText(unit);
    }

    @Override
    public boolean isDialogShown() {
        return mIsDialogShown;
    }

    @Override
    public void setIsDialogShown(boolean isShown) {
        this.mIsDialogShown = isShown;
    }

    public void onCustomUnitAdded(String unit) {
        mSpinnerController.onCustomUnitAdded(unit);
    }

    public void onCustomUnitDialogDiscarded() {
        mSpinnerController.resetSpinnerToLastSelection();
        mIsDialogShown = false;
    }

    public void onRuleOverrideAdded(Rule rule) {
        mRuleOverridesController.addRule(rule);
    }

    public void onRuleOverrideEdited(Rule rule) {
        mRuleOverridesController.editRule(rule);
    }
}
