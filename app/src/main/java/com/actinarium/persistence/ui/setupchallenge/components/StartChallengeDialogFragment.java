package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class StartChallengeDialogFragment extends DialogFragment {

    public static final String TAG = "StartChallengeDialogFragment";

    public static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    public static final String ARG_STARTING_FROM_SETUP_CHALLENGE = "com.actinarium.persistence.intent.arg.STARTING_FROM_SETUP_CHALLENGE";

    private Host mHost;
    private Context mContext;

    public static StartChallengeDialogFragment newInstance(boolean startingFromSetupChallenge, @NonNull Challenge challenge) {
        StartChallengeDialogFragment fragment = new StartChallengeDialogFragment();
        Bundle args = new Bundle(2);
        args.putBoolean(ARG_STARTING_FROM_SETUP_CHALLENGE, startingFromSetupChallenge);
        args.putParcelable(ARG_CHALLENGE, challenge);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean startingFromSetupChallenge = getArguments().getBoolean(ARG_STARTING_FROM_SETUP_CHALLENGE, false);
        final Challenge challenge = getArguments().getParcelable(ARG_CHALLENGE);
        if (challenge == null) {
            throw new IllegalArgumentException("Challenge in StartChallengeDialogFragment is null");
        }

        String negativeText = startingFromSetupChallenge ? getString(R.string.keep_editing) : getString(R.string.cancel);

        return new AlertDialog.Builder(mContext)
                .setTitle(R.string.dialog_start_challenge_title)
                .setMessage(getString(R.string.dialog_start_challenge_message, challenge.units))
                .setPositiveButton(R.string.start_challenge, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onStartChallengeConfirmed(challenge);
                    }
                })
                .setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {
        void onStartChallengeConfirmed(Challenge challenge);
    }

}
