package com.actinarium.persistence.ui.support;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.actinarium.persistence.R;

public class SupportActivity extends AppCompatActivity {

    public static void launch(Context context) {
        Intent intent = new Intent(context, SupportActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState == null) {
            // todo: probably just replace with hardcoded fragment in XML?
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_host, new SupportFragment(), SupportFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
