package com.actinarium.persistence.ui.viewchallenge.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.ui.common.RemindersController;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 */
public class StaticRemindersController extends RemindersController {

    private String[] mTypes;

    public StaticRemindersController(Context context, LinearLayout parentLayout) {
        super(parentLayout, context);
        mTypes = context.getResources().getStringArray(R.array.reminder_types_short);
    }

    @Override
    protected void onRemindersChanged() {
        // no-op
    }

    @Override
    protected ReminderHolder createHolder(int time, int type) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_reminder_readonly, mParentLayout, false);
        return new StaticReminderHolder(view, time, type);
    }

    private class StaticReminderHolder implements ReminderHolder {

        View mView;
        int mTime;
        int mType;

        private final TextView mTimeBtn;
        private final TextView mTypeText;

        StaticReminderHolder(View view, int time, int type) {
            mView = view;
            mTime = time;
            mType = type;

            mTimeBtn = (TextView) view.findViewById(R.id.reminder_time);
            mTimeBtn.setText(Utils.timeToString(Utils.extractHour(mTime), Utils.extractMinute(mTime), mContext));

            mTypeText = (TextView) view.findViewById(R.id.reminder_type);
            // Equal to the selection index, since those flags are exactly 0, 1, 2. Just saving some cycles
            mTypeText.setText(mContext.getString(R.string.em_dash_prefix, mTypes[type]));
        }

        public void setTime(int time) {
            mTime = time;
            mTimeBtn.setText(Utils.timeToString(Utils.extractHour(mTime), Utils.extractMinute(mTime), mContext));
        }

        @Override
        public int getType() {
            return mType;
        }

        @Override
        public int getTime() {
            return mTime;
        }

        @Override
        public View getView() {
            return mView;
        }
    }
}
