package com.actinarium.persistence.ui.common;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.Toast;
import com.actinarium.persistence.R;

/**
 * Utility class with misc common UI functionality
 *
 * @author Paul Danyliuk
 */
public final class ViewUtils {

    private ViewUtils() {}

    /**
     * Sets up toolbar as action bar, based on Persistence-specific assumptions
     *
     * @param activity  Activity, will be casted to AppCompatActivity
     * @param rootView  View where to search for <code>+@id/toolbar</code>
     * @param title     String resource, can be 0 to take string resource from activity
     * @param elevation Dimension resource, can be 0 for no elevation
     * @return Decorated action bar, in case any other changes are required
     */
    public static ActionBar setUpToolbar(@NonNull Activity activity, View rootView, @StringRes int title, @DimenRes int elevation) {
        final Toolbar toolbar;
        if (rootView != null) {
            toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        } else {
            toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        }

        AppCompatActivity compatActivity = (AppCompatActivity) activity;
        compatActivity.setSupportActionBar(toolbar);
        final ActionBar actionBar = compatActivity.getSupportActionBar();
        if (actionBar == null) {
            throw new AssertionError("Support ActionBar is null for unknown reason");
        }

        if (title != 0) {
            actionBar.setTitle(title);
        }
        if (elevation != 0) {
            actionBar.setElevation(activity.getResources().getDimension(elevation));
        }
        return actionBar;
    }

    /**
     * Set on long click toast to provided fab
     *
     * @param context Context
     * @param fab     Fab to add long click listener to. Must be wrapped by parent view
     * @param text    Text to display on long click
     */
    public static void setFabTooltip(final Context context, @NonNull final View fab, @StringRes final int text) {
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                final int offset = (int) (16 * context.getResources().getDisplayMetrics().density);
                final View fabWrapper = (View) fab.getParent();
                toast.setGravity(Gravity.TOP | Gravity.START, fabWrapper.getRight() - offset, fabWrapper.getBottom() - offset);
                toast.show();
                return true;
            }
        });
    }

    public static CharSequence hyperlinkifyText(CharSequence text, ClickableSpan clickableSpan) {
        final SpannableStringBuilder strBuilder = new SpannableStringBuilder(text);
        URLSpan[] urls = strBuilder.getSpans(0, text.length(), URLSpan.class);
        if (urls.length > 0) {
            strBuilder.setSpan(
                    clickableSpan,
                    strBuilder.getSpanStart(urls[0]), strBuilder.getSpanEnd(urls[0]), strBuilder.getSpanFlags(urls[0])
            );
            strBuilder.removeSpan(urls[0]);
        }
        return strBuilder;
    }

    public static CharSequence makeBulletList(int bulletRadius, int bulletCenterX, int leadingMargin, CharSequence... items) {
        final SpannableStringBuilder builder = new SpannableStringBuilder();
        int spanStart = 0;
        int spanEnd;
        for (int i = 0, itemsLength = items.length; i < itemsLength; i++) {
            CharSequence item = items[i];
            builder.append(item);
            if (i != itemsLength - 1) {
                builder.append("\n");
            }
            spanEnd = builder.length();
            builder.setSpan(new BulletSpan(bulletRadius, bulletCenterX, leadingMargin), spanStart, spanEnd, 0);
            spanStart = spanEnd;
        }
        return builder;
    }

    public static CharSequence makeNumberedList(int leadingMargin, CharSequence... items) {
        final SpannableStringBuilder builder = new SpannableStringBuilder();
        int spanStart = 0;
        int spanEnd;
        int value = 1;
        for (int i = 0, itemsLength = items.length; i < itemsLength; i++) {
            CharSequence item = items[i];
            builder.append(item);
            if (i != itemsLength - 1) {
                builder.append("\n");
            }
            spanEnd = builder.length();
            builder.setSpan(new NumberSpan(leadingMargin, value++), spanStart, spanEnd, 0);
            spanStart = spanEnd;
        }
        return builder;
    }

    public static CharSequence formatFromHtmlWithMediumFix(String s) {
        if (Build.VERSION.SDK_INT < 21) {
            // Workaround for missing medium typefaces
            s = s.replace("<font face=\"sans-serif-medium\">", "<b>").replace("</font>", "</b>");
        }
        return Html.fromHtml(s);
    }

    /**
     * Make progress bar raise and dunk based on scrollview position
     *
     * @param actionBar action bar to change elevation for
     * @param view      scroll view to observe
     * @param elevation elevation of the view when scroll view is scrolled
     */
    public static void wire(@NonNull final ActionBar actionBar, @NonNull final ScrollView view, final float elevation) {
        view.post(new Runnable() {
            @Override
            public void run() {
                actionBar.setElevation(view.getScrollY() > 0 ? elevation : 0);
            }
        });

        view.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            boolean isScrolled = false;

            @Override
            public void onScrollChanged() {
                if (isScrolled && view.getScrollY() <= 0) {
                    isScrolled = false;
                    ObjectAnimator.ofFloat(actionBar, "elevation", 0f).setDuration(75).start();
                } else if (!isScrolled && view.getScrollY() > 0) {
                    isScrolled = true;
                    ObjectAnimator.ofFloat(actionBar, "elevation", elevation).setDuration(75).start();
                }
            }
        });
    }

    /**
     * Controller implementing UI wiring between home cards and the backplate
     */
    public static class HomeScrollWire extends RecyclerView.OnScrollListener {

        private View mMasterView;
        private int mMasterViewInitialTop;
        private View mParallaxView;
        private ActionBar mRaisingActionBar;
        private Toolbar mExitingToolbar;
        private float mParallaxMultiplier;
        private float mRaisedElevation;
        private boolean mIsExitingToolbar;

        private boolean mIsElevated;
        private boolean mIsDetachedProcessed;
        private int mNegativeToolbarHeight;

        public HomeScrollWire setParallaxView(View parallaxView, float parallaxMultiplier) {
            mParallaxView = parallaxView;
            mParallaxMultiplier = parallaxMultiplier;
            return this;
        }

        public HomeScrollWire setRaisingActionBar(ActionBar raisingActionBar, float raisedElevation) {
            mRaisingActionBar = raisingActionBar;
            mRaisedElevation = raisedElevation;
            mIsElevated = raisingActionBar.getElevation() != 0;
            return this;
        }

        public HomeScrollWire setExitingToolbar(Toolbar exitingToolbar, boolean exits) {
            mExitingToolbar = exitingToolbar;
            mIsExitingToolbar = exits;
            return this;
        }

        /**
         * Use this method whenever master view is attached or detached from screen
         *
         * @param view pass the view when it is attached, null when detached
         */
        public HomeScrollWire setMasterView(View view) {
            mMasterView = view;
            return this;
        }

        /**
         * Use this method to specify the value at which master view is considered to be at zero (and action bar should
         * rest with zero elevation).
         *
         * @param masterViewInitialTop
         */
        public HomeScrollWire setMasterViewInitialTop(int masterViewInitialTop) {
            mMasterViewInitialTop = masterViewInitialTop;
            return this;
        }

        public HomeScrollWire wireTo(RecyclerView recyclerView) {
            recyclerView.addOnScrollListener(this);
            return this;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            final int masterViewOffset;

            // If master view is not present, do nothing. Or, actually, move stuff off-screen once
            if (mMasterView == null) {
                if (!mIsDetachedProcessed) {
                    updateElevation(true);
                    mParallaxView.setVisibility(View.INVISIBLE);
                    mIsDetachedProcessed = true;
                }
                masterViewOffset = Integer.MIN_VALUE;
            } else {
                // Reset the state of "process once if not attached"
                if (mIsDetachedProcessed) {
                    mParallaxView.setVisibility(View.VISIBLE);
                    mIsDetachedProcessed = false;
                }
                masterViewOffset = mMasterView.getTop() - mMasterViewInitialTop;

                // Set translation according to master view position
                mParallaxView.setTranslationY(masterViewOffset * mParallaxMultiplier);
            }

            // If we have exit toolbar, move it as well
            if (mIsExitingToolbar) {
                if (mNegativeToolbarHeight == 0) {
                    mNegativeToolbarHeight = -mExitingToolbar.getHeight();
                }
                float newToolbarTranslation = mExitingToolbar.getTranslationY() - dy;
                if (newToolbarTranslation > 0) {
                    newToolbarTranslation = 0;
                } else if (newToolbarTranslation < mNegativeToolbarHeight) {
                    newToolbarTranslation = mNegativeToolbarHeight;
                }
                mExitingToolbar.setTranslationY(newToolbarTranslation);

                boolean shouldElevate = masterViewOffset < newToolbarTranslation;
                updateElevation(shouldElevate);
            } else {
                final boolean shouldElevate = masterViewOffset != 0;
                updateElevation(shouldElevate);
            }

        }

        private void updateElevation(boolean shouldElevate) {
            if (mIsElevated && !shouldElevate) {
                mIsElevated = false;
                ObjectAnimator.ofFloat(mRaisingActionBar, "elevation", 0f).setDuration(75).start();
            } else if (!mIsElevated && shouldElevate) {
                mIsElevated = true;
                ObjectAnimator.ofFloat(mRaisingActionBar, "elevation", mRaisedElevation).setDuration(75).start();
            }
        }
    }

    public static class WiredAnimator extends DefaultItemAnimator {

        private HomeScrollWire mWire;

        public WiredAnimator(HomeScrollWire wire) {
            mWire = wire;
        }

        @Override
        public void onAnimationStarted(RecyclerView.ViewHolder viewHolder) {
            super.onAnimationStarted(viewHolder);
            mWire.onScrolled(null, 0, 0);
        }

        @Override
        public void onAnimationFinished(RecyclerView.ViewHolder viewHolder) {
            super.onAnimationFinished(viewHolder);
            mWire.onScrolled(null, 0, 0);
        }
    }
}
