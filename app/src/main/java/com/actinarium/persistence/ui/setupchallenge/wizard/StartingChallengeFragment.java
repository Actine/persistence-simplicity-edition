package com.actinarium.persistence.ui.setupchallenge.wizard;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import com.actinarium.aligned.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartingChallengeFragment extends Fragment implements ChallengeWizardFragment.ModifiesChallenge {

    public static final String TAG = "StartingChallengeFragment";

    private ChallengeWizardFragment.Host mHost;
    private Spinner mStartingWhen;

    public StartingChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHost = (ChallengeWizardFragment.Host) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cw_start_challenge, container, false);
        ((TextView) view.findViewById(R.id.ob_starting_text_1))
                .setText(getString(R.string.ob_starting_text_1,
                        getString(R.string.dialog_start_challenge_message, mHost.getChallenge().units)
                ));

        mStartingWhen = (Spinner) view.findViewById(R.id.challenge_starting_when);

        return view;
    }

    @Override
    public void writeToChallenge(Challenge challenge) {
        final int today = Utils.getCurrentEpochDay(getContext());
        challenge.createdOn = today;
        challenge.rulesSetOn = today;

        final int startingMode = mStartingWhen.getSelectedItemPosition();
        if (startingMode == Challenge.STARTING_NOW) {
            challenge.statusFlags = Challenge.FLAG_DISPLAYED | Challenge.FLAG_ACTIVE;
            challenge.startedOn = today;
        } else {
            challenge.statusFlags = Challenge.FLAG_DISPLAYED;
        }
    }
}
