package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.widget.TimePicker;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class TimePickerDialogFragment extends DialogFragment {

    public static final String TAG = "TimePickerDialogFragment";

    private static final String ARG_INIT_TIME = "com.actinarium.persistence.intent.arg.INIT_TIME";

    public static final int NO_TIME = -1;

    private Host mHost;
    private Context mContext;

    public static TimePickerDialogFragment newInstance(int id) {
        TimePickerDialogFragment fragment = new TimePickerDialogFragment();
        Bundle args = new Bundle(1);
        args.putInt(ARG_INIT_TIME, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.Theme_Persistence_Dialog);
        final TimePicker picker = (TimePicker) LayoutInflater.from(builder.getContext()).inflate(R.layout.time_picker, null);
        final int initTime = getInitTime();
        final int minuteOfDay = initTime == NO_TIME ? Utils.getClosestHalfHour() : initTime;
        picker.setCurrentHour(Utils.extractHour(minuteOfDay));
        picker.setCurrentMinute(Utils.extractMinute(minuteOfDay));
        picker.setIs24HourView(DateFormat.is24HourFormat(mContext));

        builder
                .setView(picker)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.saveTimePickerResult(initTime, Utils.getMinuteOfDay(picker.getCurrentHour(), picker.getCurrentMinute()));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    private int getInitTime() {
        return getArguments().getInt(ARG_INIT_TIME);
    }

    public interface Host {
        void saveTimePickerResult(int initTime, int newTime);
    }

}
