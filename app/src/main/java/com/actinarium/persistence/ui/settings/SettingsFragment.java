package com.actinarium.persistence.ui.settings;


import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.design.widget.Snackbar;
import android.view.View;
import com.actinarium.persistence.BuildConfig;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private String mOldCustomItemsString;

    public SettingsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        final PreferenceScreen screen = getPreferenceScreen();
        screen.findPreference(PrefUtils.SETTING_RESET_CUSTOM_UNITS)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        resetCustomItems();
                        showUndoBar();
                        return true;
                    }
                });
        screen.findPreference(PrefUtils.SETTING_VIEW_PRIVACY_POLICY)
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(BuildConfig.PRIVACY_POLICY_URL)));
                        return true;
                    }
                });
        final SharedPreferences preferences = screen.getSharedPreferences();
        onSharedPreferenceChanged(preferences, PrefUtils.SETTING_DAY_STARTS_AT);
        onSharedPreferenceChanged(preferences, PrefUtils.SETTING_ALLOW_ANALYTICS);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PrefUtils.SETTING_DAY_STARTS_AT)) {
            int minutesOfDay = sharedPreferences.getInt(key, PrefUtils.SETTING_DAY_STARTS_AT_DEFAULT);
            int hours = Utils.extractHour(minutesOfDay);
            int minutes = Utils.extractMinute(minutesOfDay);
            findPreference(key).setSummary(Utils.timeToString(hours, minutes, getActivity()));
        }
    }

    private void resetCustomItems() {
        final SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
        mOldCustomItemsString = sharedPreferences.getString(PrefUtils.SETTING_CUSTOM_UNITS, null);
        sharedPreferences.edit().remove(PrefUtils.SETTING_CUSTOM_UNITS).apply();
    }

    private void restoreCustomItems() {
        if (mOldCustomItemsString != null) {
            getPreferenceScreen()
                    .getSharedPreferences()
                    .edit()
                    .putString(PrefUtils.SETTING_CUSTOM_UNITS, mOldCustomItemsString)
                    .apply();
        }
    }

    private void showUndoBar() {
        View view = getView();
        if (view == null) {
            return;
        }
        Snackbar snackbar = Snackbar.make(view, R.string.reset_custom_units_snackbar, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.undo, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restoreCustomItems();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.accentBright));
        snackbar.show();
    }
}
