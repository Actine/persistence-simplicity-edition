package com.actinarium.persistence.ui.setupchallenge.wizard;


import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.ui.setupchallenge.components.EditableRemindersController;
import com.actinarium.persistence.ui.setupchallenge.components.TimePickerDialogFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeRemindersFragment extends Fragment implements EditableRemindersController.Host, ChallengeWizardFragment.ModifiesChallenge {

    public static final String TAG = "ChallengeRemindersFragment";

    private static final String ARG_REMINDERS = "com.actinarium.persistence.intent.arg.REMINDERS";

    private ChallengeWizardFragment.Host mHost;

    private EditableRemindersController mRemindersController;
    private ScrollView mScrollView;

    private Button mAddReminderButton;

    public ChallengeRemindersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHost = (ChallengeWizardFragment.Host) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArray(ARG_REMINDERS, mRemindersController.getReminders());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cw_reminders, container, false);
        mScrollView = (ScrollView) view.findViewById(R.id.scrollView);

        /* ---------------- SET UP REMINDERS SECTION ---------------- */

        mAddReminderButton = (Button) view.findViewById(R.id.add_reminder);
        mAddReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timePickerDialogFragment = TimePickerDialogFragment.newInstance(TimePickerDialogFragment.NO_TIME);
                timePickerDialogFragment.show(getFragmentManager(), TimePickerDialogFragment.TAG);
            }
        });

        final LinearLayout remindersContainer = (LinearLayout) view.findViewById(R.id.challenge_reminders);
        mRemindersController = new EditableRemindersController(this, getActivity(), remindersContainer);

        if (savedInstanceState != null) {
            Parcelable[] remindersRaw = savedInstanceState.getParcelableArray(ARG_REMINDERS);
            Reminder[] reminders = new Reminder[remindersRaw.length];
            System.arraycopy(remindersRaw, 0, reminders, 0, remindersRaw.length);
            mRemindersController.setReminders(reminders);
        } else {
            mRemindersController.setReminders(mHost.getChallenge().getReminders());
        }

        return view;
    }

    @Override
    public void writeToChallenge(Challenge challenge) {
        // Save reminders into challenge
        challenge.setReminders(mRemindersController.getReminders());
    }

    @Override
    public void onDuplicateReminder() {
        Toast.makeText(getActivity(), R.string.reminder_already_set, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void editReminderTime(int oldTime) {
        TimePickerDialogFragment timePickerDialogFragment = TimePickerDialogFragment.newInstance(oldTime);
        timePickerDialogFragment.show(getFragmentManager(), TimePickerDialogFragment.TAG);
    }

    @Override
    public void updateAddReminderButtonText(boolean hasReminders) {
        mAddReminderButton.setText(hasReminders ? R.string.add_reminder : R.string.add_reminder_first);
    }

    public void saveDatePickerResult(int oldTime, int newTime) {
        if (oldTime == TimePickerDialogFragment.NO_TIME) {
            mRemindersController.addReminder(newTime);
        } else {
            mRemindersController.updateReminder(oldTime, newTime);
        }
    }

    @Override
    public void requestVerticalScroll(int offset, boolean force) {
        // todo: externalize this somewhere - duplicate code block
        int requiredOffset;
        if (force) {
            requiredOffset = offset;
        } else {
            int remainder = mScrollView.getChildAt(0).getBottom() - mScrollView.getScrollY() - mScrollView.getHeight();
            if (remainder > -offset) {
                requiredOffset = 0;
            } else {
                requiredOffset = offset + remainder;
            }
        }
        if (requiredOffset != 0) {
            mScrollView.smoothScrollBy(0, requiredOffset);
        }
    }
}
