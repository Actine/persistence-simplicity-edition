package com.actinarium.persistence.ui.viewchallengehistory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;

public class ChallengeHistoryActivity extends AppCompatActivity {

    public static final String EXTRA_CHALLENGE = "com.actinarium.persistence.intent.extra.CHALLENGE";

    public static void launch(Context context, Challenge challenge) {
        Intent intent = new Intent(context, ChallengeHistoryActivity.class);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState == null) {
            final Challenge challenge = getIntent().getParcelableExtra(EXTRA_CHALLENGE);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_host, ChallengeHistoryFragment.newInstance(challenge), ChallengeHistoryFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
