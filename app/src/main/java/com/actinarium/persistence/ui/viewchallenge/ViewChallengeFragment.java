package com.actinarium.persistence.ui.viewchallenge;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.data.PersistenceContract;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Progress;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.ui.common.RemindersController;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.common.charts.SparklineChart;
import com.actinarium.persistence.ui.editprogress.EditProgressActivity;
import com.actinarium.persistence.ui.setupchallenge.SetupChallengeActivity;
import com.actinarium.persistence.ui.setupchallenge.components.RuleOverridesController;
import com.actinarium.persistence.ui.setupchallenge.components.StartChallengeDialogFragment;
import com.actinarium.persistence.ui.stopchallenge.StopChallengeActivity;
import com.actinarium.persistence.ui.viewchallenge.components.DeleteChallengeDialogFragment;
import com.actinarium.persistence.ui.viewchallenge.components.StaticRemindersController;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewChallengeFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = "ViewChallengeFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";

    private Challenge mChallenge;
    private Progress mLastProgress;
    private Cursor mProgressCursor;

    private FrameLayout mFabWrapper;
    private FloatingActionButton mFab;

    private TextView mTitle;
    private TextView mStatus;
    private ProgressBar mProgressBar;
    private View mInProgressInfoBlock;
    private TextView mCompletedQty;
    private TextView mCompletedLabel;
    private TextView mLeftQty;
    private TextView mLeftLabel;
    private TextView mDebtQty;
    private TextView mDebtLabel;
    private TextView mDebtLabelSecondary;
    private TextView mReserveQty;
    private TextView mReserveLabel;
    private TextView mReserveLabelSecondary;

    private View mSinceTheStartHeader;
    private View mSinceTheStartBlock;
    private TextView mTotalPlannedQty;
    private TextView mTotalDoneQty;
    private SparklineChart mSparklineChart;

    private TextView mRemindersTitle;

    private RuleOverridesController mRuleOverridesController;
    private RemindersController mRemindersController;

    public static final int LOADER_CHALLENGE = 0;
    public static final int LOADER_ALL_PROGRESS_FOR_CHALLENGE = 1;

    public static ViewChallengeFragment newInstance(Challenge challenge) {
        ViewChallengeFragment fragment = new ViewChallengeFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(ARG_CHALLENGE, challenge);
        fragment.setArguments(args);
        return fragment;
    }

    public ViewChallengeFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_view_challenge, container, false);
        mChallenge = getArguments().getParcelable(ARG_CHALLENGE);

        // Setting up toolbar
        final FragmentActivity activity = getActivity();
        final ActionBar actionBar = ViewUtils.setUpToolbar(activity, view, 0, R.dimen.actionBarElevation);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Setting the fab
        mFabWrapper = (FrameLayout) view.findViewById(R.id.fab_wrapper);
        mFab = (FloatingActionButton) mFabWrapper.findViewById(R.id.fab);

        // Setting up the form
        mTitle = (TextView) view.findViewById(R.id.challenge_title);
        mStatus = (TextView) view.findViewById(R.id.challenge_status);

        mProgressBar = ((ProgressBar) view.findViewById(R.id.progress_bar));

        mInProgressInfoBlock = view.findViewById(R.id.block_inprogress);
        mCompletedQty = (TextView) mInProgressInfoBlock.findViewById(R.id.progress_completed_qty);
        mCompletedLabel = (TextView) mInProgressInfoBlock.findViewById(R.id.progress_completed_label);
        mLeftQty = (TextView) mInProgressInfoBlock.findViewById(R.id.progress_left_qty);
        mLeftLabel = (TextView) mInProgressInfoBlock.findViewById(R.id.progress_left_label);
        mDebtQty = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_debt);
        mDebtLabel = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_debt_label);
        mDebtLabelSecondary = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_debt_label_secondary);
        mReserveQty = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_reserve);
        mReserveLabel = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_reserve_label);
        mReserveLabelSecondary = (TextView) mInProgressInfoBlock.findViewById(R.id.challenge_reserve_label_secondary);

        mSinceTheStartHeader = view.findViewById(R.id.since_the_start_header);
        mSinceTheStartBlock = view.findViewById(R.id.since_the_start_block);
        mTotalPlannedQty = (TextView) mSinceTheStartBlock.findViewById(R.id.total_planned_qty);
        mTotalDoneQty = (TextView) mSinceTheStartBlock.findViewById(R.id.total_done_qty);
        mSparklineChart = (SparklineChart) view.findViewById(R.id.sparkline_chart);

        // Rules and overrides control
        final LinearLayout overridesContainer = (LinearLayout) view.findViewById(R.id.challenge_goal_rules);
        mRuleOverridesController = new RuleOverridesController(null, activity, overridesContainer, R.layout.item_challenge_rule_readonly);
        final LinearLayout remindersContainer = (LinearLayout) view.findViewById(R.id.challenge_reminders);
        mRemindersController = new StaticRemindersController(activity, remindersContainer);
        mRemindersTitle = (TextView) view.findViewById(R.id.reminders_title);

        // Bind challenge data (no progress data yet available)
        if (savedInstanceState == null) {
            onChallengeSet();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_view_challenge, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit_challenge) {
            SetupChallengeActivity.launchEdit(getActivity(), mChallenge);
            return true;
        } else if (id == R.id.action_delete_challenge) {
            DeleteChallengeDialogFragment dialog = DeleteChallengeDialogFragment.newInstance(mChallenge.id);
            dialog.show(getFragmentManager(), DeleteChallengeDialogFragment.TAG);
            return true;
        } else if (id == R.id.action_stop_challenge) {
            StopChallengeActivity.launch(getActivity(), mChallenge);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(LOADER_CHALLENGE, null, this);
        getLoaderManager().initLoader(LOADER_ALL_PROGRESS_FOR_CHALLENGE, null, this);
    }

    // ------------------- BIND DATA ----------------


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mChallenge.isActive()) {
            menu.findItem(R.id.action_delete_challenge).setVisible(false);
            menu.findItem(R.id.action_stop_challenge).setVisible(true);
            menu.findItem(R.id.action_edit_challenge).setVisible(true);
        } else if (mChallenge.isOver()) {
            menu.findItem(R.id.action_delete_challenge).setVisible(false);
            menu.findItem(R.id.action_stop_challenge).setVisible(false);
            menu.findItem(R.id.action_edit_challenge).setVisible(false);
        } else {
            menu.findItem(R.id.action_delete_challenge).setVisible(true);
            menu.findItem(R.id.action_stop_challenge).setVisible(false);
            menu.findItem(R.id.action_edit_challenge).setVisible(true);
        }
    }

    /**
     * Bind current {@link #mChallenge} to the form. Should be run when form is first loaded and also when anything
     * updates (e.g. challenge is started or stopped)
     */
    private void onChallengeSet() {
        final Context context = getContext();

        // Tickle action menu
        getActivity().invalidateOptionsMenu();

        // First go the common fields
        mTitle.setText(mChallenge.title);
        mRuleOverridesController.setUnitsText(mChallenge.units);
        mRuleOverridesController.setRules(mChallenge.getRules());

        final Reminder[] reminders = mChallenge.getReminders();
        mRemindersTitle.setText(reminders.length != 0 ? getString(R.string.reminders) : getString(R.string.no_reminders));
        mRemindersController.setReminders(reminders);

        // Now, depending on challenge state
        // The challenge lifecycle is described in three states: created but not started; active; over (stopped)
        final int fabIcon;
        final int fabActionText;
        if (mChallenge.isActive()) {
            // Active (started) state
            fabIcon = R.drawable.ic_check_black_24dp;
            fabActionText = R.string.set_progress;
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLastProgress != null) {
                        EditProgressActivity.launch(context, mChallenge, mLastProgress);
                    }
                }
            });

            mProgressBar.setVisibility(View.VISIBLE);
            mInProgressInfoBlock.setVisibility(View.VISIBLE);
            mSinceTheStartHeader.setVisibility(View.VISIBLE);
            mSinceTheStartBlock.setVisibility(View.VISIBLE);
            mSparklineChart.setVisibility(View.VISIBLE);

            // Status will be set in onProgressCursorSet(), but let's put temporary status while loading
            mStatus.setText(Utils.getStatusText(context, mChallenge.status));
        } else if (mChallenge.isOver()) {
            // Completed / cancelled / given up
            fabIcon = R.drawable.ic_refresh_black_24dp;
            fabActionText = R.string.start_over;
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SetupChallengeActivity.launchRestart(context, mChallenge);
                }
            });

            mProgressBar.setVisibility(View.GONE);
            mInProgressInfoBlock.setVisibility(View.GONE);
            mSinceTheStartHeader.setVisibility(View.VISIBLE);
            mSinceTheStartBlock.setVisibility(View.VISIBLE);
            mSparklineChart.setVisibility(View.VISIBLE);

            final int duration = mChallenge.stoppedOn - mChallenge.startedOn + 1;
            mStatus.setText(getString(
                    R.string.stopped_on_lasted,
                    Utils.prettyPrintDate(Utils.createDayFromEpoch(mChallenge.stoppedOn), Utils.DATE_FORMAT_NO_WEEKDAY),
                    getResources().getQuantityString(R.plurals.x_days, duration, duration)
            ));

            // redraw the cursor stats
            onProgressCursorSet();
        } else {
            // Just created
            fabIcon = R.drawable.ic_rocket_black_24dp;
            fabActionText = R.string.start_challenge;
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StartChallengeDialogFragment dialog = StartChallengeDialogFragment.newInstance(false, mChallenge);
                    dialog.show(getFragmentManager(), StartChallengeDialogFragment.TAG);
                }
            });

            mProgressBar.setVisibility(View.GONE);
            mInProgressInfoBlock.setVisibility(View.GONE);
            mSinceTheStartHeader.setVisibility(View.GONE);
            mSinceTheStartBlock.setVisibility(View.GONE);
            mSparklineChart.setVisibility(View.GONE);

            final int dayDiff = Utils.getCurrentEpochDay(context) - mChallenge.createdOn;
            if (dayDiff < 2) {
                mStatus.setText(getString(R.string.added_when, Utils.getDurationAgoString(context, dayDiff)));
            } else {
                mStatus.setText(getString(
                        R.string.added_when_long,
                        Utils.prettyPrintDate(Utils.createDayFromEpoch(mChallenge.createdOn),
                                dayDiff > 7 ? Utils.DATE_FORMAT_NO_WEEKDAY : Utils.DATE_FORMAT_SHORT_WEEKDAY),
                        Utils.getDurationAgoString(context, dayDiff)
                ));
            }
        }
        mFab.setImageResource(fabIcon);
        mFab.setContentDescription(getString(fabActionText));
        ViewUtils.setFabTooltip(getActivity(), mFab, fabActionText);
    }

    /**
     * Bind loaded full progress data to the form. Should be run whenever cursor with progress is (re)loaded, as well
     * as whenever challenge is changed
     */
    private void onProgressCursorSet() {
        // Well, we sorta expect the cursor to be non-null, but leave this for now
        if (mProgressCursor == null || mProgressCursor.getCount() == 0) {
            mSparklineChart.setValues(null, 28, true, 0, 0, 100);
            return;
        }

        // todo: refactor this mess
        final boolean stopped = !mChallenge.isActive();

        // Init temp progress object and arrays for data
        Progress progress = new Progress();
        int value;
        int lengthMinusOne = mProgressCursor.getCount() - 1;
        int[] sparkValues;
        if (stopped) {
            // Include prospected spark as final segment
            sparkValues = new int[lengthMinusOne + 2];
        } else {
            sparkValues = new int[lengthMinusOne + 1];
        }

        // Determine chart min and max here
        // Usually the max value is the last one. Or, if the user is a lazy ass, let his phone suffer
        // Also read the last progress here while we're at it
        mProgressCursor.moveToLast();
        mLastProgress = new Progress().fillFromCursor(mProgressCursor);
        sparkValues[lengthMinusOne] = mLastProgress.getSpark();
        int minValue = sparkValues[lengthMinusOne];
        int maxValue = minValue;
        int prospectedSparkValue = mLastProgress.getProspectedSpark();
        if (stopped) {
            sparkValues[lengthMinusOne + 1] = prospectedSparkValue;
        }

        // Calculate sum of goals and progress for all days INCLUDING TODAY
        int totalPlanned = mLastProgress.dailyGoal;
        int totalDone = mLastProgress.completed;

        // Cursor data is expected to be sorted oldest to newest
        for (int i = 0; i < lengthMinusOne; i++) {
            mProgressCursor.moveToPosition(i);
            progress.fillFromCursor(mProgressCursor);

            value = progress.getSpark();
            if (value > maxValue) {
                maxValue = value;
            } else if (value < minValue) {
                minValue = value;
            }

            sparkValues[i] = progress.getSpark();
            totalPlanned += progress.dailyGoal;
            totalDone += progress.completed;
        }

        // Prospected spark value also controls min/max
        if (prospectedSparkValue > maxValue) {
            maxValue = prospectedSparkValue;
        } else if (prospectedSparkValue < minValue) {
            minValue = prospectedSparkValue;
        }

        // Compensate for ongoing challenge
        int chartSize;
        if (stopped) {
            mSparklineChart.setValues(sparkValues, lengthMinusOne + 1, false, 0, minValue, maxValue);
        } else {
            chartSize = Math.max(lengthMinusOne + 1, 7);
            minValue = (int) (minValue * chartSize / (lengthMinusOne + 1f));
            maxValue = (int) (maxValue * chartSize / (lengthMinusOne + 1f));

            mSparklineChart.setValues(sparkValues, chartSize, true, prospectedSparkValue, minValue, maxValue);
        }

        // now the summary
        mTotalPlannedQty.setText(getString(R.string.qty, totalPlanned, mChallenge.units));
        mTotalDoneQty.setText(getString(R.string.qty, totalDone, mChallenge.units));

        // and the rest of the form if challenge is not yet stopped
        if (stopped) {
            return;
        }

        final Context context = getContext();

        // Status
        final int sparkToday = sparkValues[lengthMinusOne];
        final int spark1dAgo = lengthMinusOne >= 1 ? sparkValues[lengthMinusOne - 1] : 0;
        final int spark2dAgo = lengthMinusOne >= 2 ? sparkValues[lengthMinusOne - 2] : 0;
        mStatus.setText(Utils.getStatusText(context, Utils.getStatus(sparkToday, spark1dAgo, spark2dAgo)));

        mProgressBar.setMax(mLastProgress.dailyGoal + mLastProgress.debt);
        mProgressBar.setProgress(mLastProgress.completed);
        mProgressBar.setSecondaryProgress(mLastProgress.completed + mLastProgress.compensated);

        mCompletedQty.setText(String.valueOf(mLastProgress.completed + mLastProgress.compensated));
        mCompletedLabel.setText(context.getString(R.string.qty_down, mChallenge.units));
        mLeftQty.setText(String.valueOf(mLastProgress.getUnderachievement() - mLastProgress.compensated));
        mLeftLabel.setText(context.getString(R.string.qty_to_go, mChallenge.units));

        mDebtQty.setText(String.valueOf(mLastProgress.debt));
        mDebtLabel.setText(context.getString(R.string.qty_debt, mChallenge.units));
        String debtStatus = mLastProgress.debt == 0
                ? getString(R.string.no_debt_status)
                : mLastProgress.debtMultiplier != 100
                ? getString(R.string.debt_status, Utils.formatDebtMultiplier(mLastProgress.debtMultiplier))
                : getString(R.string.debt_status);
        mDebtLabelSecondary.setText(debtStatus);

        final int todayDoubleReserveDelta = mLastProgress.getDoubleReserveDelta();
        // todo: externalize
        mReserveQty.setText(Utils.formatReserve(mLastProgress.doubleReserveBalance + todayDoubleReserveDelta));
        mReserveLabel.setText(R.string.qty_reserve);
        String reserveStatus = todayDoubleReserveDelta >= 0
                    ? getString(R.string.reserve_collected_status, Utils.formatReserve(todayDoubleReserveDelta))
                    : getString(R.string.reserve_used_status, Utils.formatReserve(-todayDoubleReserveDelta));

        mReserveLabelSecondary.setText(reserveStatus);
    }

    // ------------------- LOADER -------------------

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == LOADER_CHALLENGE) {
            return new CursorLoader(
                    getActivity(),
                    PersistenceContract.ChallengeEntry.CONTENT_URI,
                    null,
                    PersistenceContract.BuildingBlocks.SELECTION_ID,
                    new String[]{Long.toString(mChallenge.id)},
                    null
            );
        } else {
            return new CursorLoader(
                    getActivity(),
                    PersistenceContract.ProgressEntry.CONTENT_URI,
                    null,
                    PersistenceContract.BuildingBlocks.SELECTION_CHALLENGE_ID,
                    new String[]{Long.toString(mChallenge.id)},
                    null
            );
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == LOADER_CHALLENGE) {
            if (!data.moveToFirst()) {
                // No data - maybe the challenge was deleted while we were displaying it. This shouldn't happen, but just in case...
                Log.w(TAG, "Loader returned no data for view challenge - maybe it was deleted?");
                return;
            }
            mChallenge.fillFromCursor(data);
            onChallengeSet();
            // Store for future acquisition
            getArguments().putParcelable(ARG_CHALLENGE, mChallenge);
        } else {
            // Old cursor is managed (closed) implicitly
            mProgressCursor = data;
            onProgressCursorSet();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Old cursor is managed (closed) implicitly
        mProgressCursor = null;
        // No need to remove any data at this point
    }
}
