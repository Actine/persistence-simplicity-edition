package com.actinarium.persistence.ui.onboarding;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actinarium.aligned.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.common.ViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutPersistenceFragment extends Fragment {

    public static final String TAG = "AboutPersistenceFragment";

    public AboutPersistenceFragment() {
        // Required empty public constructor
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ob_about_persistence, container, false);

        // Fill the bullet lists
        final TextView challengeRequirements = (TextView) view.findViewById(R.id.challenge_requirements);
        final int bulletRadius = getResources().getDimensionPixelSize(R.dimen.bulletRadius);
        final int bulletCenterX = getResources().getDimensionPixelSize(R.dimen.bulletCenterX);
        final int leadingMargin = getResources().getDimensionPixelSize(R.dimen.bulletLeadingMargin);
        challengeRequirements.setText(
                ViewUtils.makeBulletList(
                        bulletRadius,
                        bulletCenterX,
                        leadingMargin,
                        getResources().getTextArray(R.array.ob_persistence_challenge_reqs)
                )
        );

        final TextView challengeExamples = (TextView) view.findViewById(R.id.challenge_examples);
        challengeExamples.setText(
                ViewUtils.makeBulletList(
                        bulletRadius,
                        bulletCenterX,
                        leadingMargin,
                        getResources().getTextArray(R.array.ob_persistence_challenge_examples)
                )
        );

        // Fill the numbered list
        final GridLayout rulesGrid = (GridLayout) view.findViewById(R.id.rules_list);
        CharSequence[] items = getResources().getTextArray(R.array.ob_persistence_rules);
        for (int i = 0, itemsLength = items.length; i < itemsLength; ) {
            CharSequence item = items[i];
            TextView index = (TextView) inflater.inflate(R.layout.grid_ol_index, rulesGrid, false);
            TextView text = (TextView) inflater.inflate(R.layout.grid_ol_text, rulesGrid, false);
            index.setText(String.valueOf(++i) + '.');
            text.setText(item);
            rulesGrid.addView(index);
            rulesGrid.addView(text);
        }

        return view;
    }

}
