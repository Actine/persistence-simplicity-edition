package com.actinarium.persistence.ui.home.components;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actinarium.persistence.R;

import java.text.DecimalFormat;

/**
* <p></p>
*
* @author Paul Danyliuk
* @version $Id$
*/
public class HeaderHolder extends RecyclerView.ViewHolder {

    private ProgressBar mProgressBar;
    private View mStatsBox;
    private TextView mGoalsMet;
    private TextView mPercCompleted;
    private TextView mQuoteBox;

    private Context mContext;

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.#%");

    public HeaderHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;

        mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        mProgressBar.setMax(1000);

        mStatsBox = itemView.findViewById(R.id.stats_box);
        mGoalsMet = (TextView) mStatsBox.findViewById(R.id.goals_met);
        mPercCompleted = (TextView) mStatsBox.findViewById(R.id.perc_completed);

        mQuoteBox = (TextView) itemView.findViewById(R.id.quote_box);
    }

    public void refreshStats(double percentCompleted, int goalsMet, int goalsCount) {
        if (goalsCount != 0) {
            mProgressBar.setVisibility(View.VISIBLE);
            mStatsBox.setVisibility(View.VISIBLE);
            mQuoteBox.setVisibility(View.GONE);

            // Primary progress is for fully completed tasks
            mProgressBar.setProgress((int) Math.floor(goalsMet * 1000d / goalsCount));
            // Secondary progress is percentage of progress
            mProgressBar.setSecondaryProgress((int) Math.floor(percentCompleted * 1000));

            mGoalsMet.setText(mContext.getString(R.string.vulgar_fraction, goalsMet, goalsCount));
            mPercCompleted.setText(DECIMAL_FORMAT.format(percentCompleted));
        } else {
            mProgressBar.setVisibility(View.GONE);
            mStatsBox.setVisibility(View.GONE);
            mQuoteBox.setVisibility(View.VISIBLE);
        }
    }
}
