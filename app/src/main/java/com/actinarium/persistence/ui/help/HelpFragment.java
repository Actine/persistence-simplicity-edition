package com.actinarium.persistence.ui.help;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.actinarium.persistence.BuildConfig;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.common.ViewUtils;
import com.actinarium.persistence.ui.home.HomeActivity;
import com.actinarium.persistence.ui.setupchallenge.SetupChallengeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment {

    public static final String TAG = "HelpFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_help_about, container, false);

        // Setting up toolbar
        final ActionBar actionBar = ViewUtils.setUpToolbar(getActivity(), view, 0, R.dimen.actionBarElevation);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView versionInfo = (TextView) view.findViewById(R.id.version_info);
        versionInfo.setText(getString(R.string.version_info, BuildConfig.VERSION_NAME));

        TextView text2 = (TextView) view.findViewById(R.id.getting_help_text_2);
        text2.setMovementMethod(LinkMovementMethod.getInstance());

        TextView oosList = (TextView) view.findViewById(R.id.open_source_list);
        CharSequence[] items = getResources().getTextArray(R.array.oos_libraries_list);
        final int bulletRadius = getResources().getDimensionPixelSize(R.dimen.bulletRadius);
        final int bulletCenterX = getResources().getDimensionPixelSize(R.dimen.bulletCenterX);
        final int leadingMargin = getResources().getDimensionPixelSize(R.dimen.bulletLeadingMargin);
        oosList.setText(ViewUtils.makeBulletList(bulletRadius, bulletCenterX, leadingMargin, items));

        Button restartOnboarding = (Button) view.findViewById(R.id.run_onboarding);
        restartOnboarding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.restartOnboarding(getContext());
            }
        });

        Button restartWizard = (Button) view.findViewById(R.id.run_wizard);
        restartWizard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                SetupChallengeActivity.launchWizard(getContext());
            }
        });

        return view;
    }

}
