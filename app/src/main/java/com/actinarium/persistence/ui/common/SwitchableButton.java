package com.actinarium.persistence.ui.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.widget.Button;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 */
public class SwitchableButton extends Button {

    private OnClickListener mOnDisabledClickListener;

    public SwitchableButton(Context context) {
        super(context);
    }

    public SwitchableButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwitchableButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnDisabledClickListener(OnClickListener onDisabledClickListener) {
        mOnDisabledClickListener = onDisabledClickListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled() && event.getAction() == MotionEvent.ACTION_UP && mOnDisabledClickListener != null) {
            playSoundEffect(SoundEffectConstants.CLICK);
            mOnDisabledClickListener.onClick(this);
        }

        return super.onTouchEvent(event);
    }
}
