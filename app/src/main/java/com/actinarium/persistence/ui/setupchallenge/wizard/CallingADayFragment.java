package com.actinarium.persistence.ui.setupchallenge.wizard;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.settings.PrefUtils;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.ui.common.RadioGroup;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class CallingADayFragment extends Fragment {

    public static final String TAG = "CallingADayFragment";
    private int[] mOptions;

    public CallingADayFragment() {
        // Required empty public constructor
    }

    @SuppressWarnings("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cw_calling_a_day, container, false);
        GridLayout optionsLayout = (GridLayout) view.findViewById(R.id.options);

        final Context context = getContext();
        final int minuteOfDay = PrefUtils.getNewDayOffset(context);
        final int selection = makeDayOptionsAndGetSelectedIndex(minuteOfDay);

        RadioGroup group = new RadioGroup(mOptions.length);

        // Add all the options
        for (int option : mOptions) {
            RadioButton button = (RadioButton) inflater.inflate(R.layout.grid_radio_button, optionsLayout, false);
            TextView label = (TextView) inflater.inflate(R.layout.grid_radio_label, optionsLayout, false);
            if (Build.VERSION.SDK_INT >= 17) {
                int id = View.generateViewId();
                button.setId(id);
                label.setLabelFor(id);
            }
            label.setText(Utils.timeToString(Utils.extractHour(option), Utils.extractMinute(option), context));
            group.addRadioButton(button, label);
            optionsLayout.addView(button);
            optionsLayout.addView(label);
        }

        group.setOnChangeListener(new RadioGroup.OnChangeListener() {
            @Override
            public void onSelectionChanged(int viewId, int index) {
                PrefUtils.setNewDayOffset(context, mOptions[index]);
            }
        });

        group.selectRadio(selection);

        return view;
    }

    private int makeDayOptionsAndGetSelectedIndex(int currentSetting) {
        final int[] dayStartOptions = PrefUtils.DAY_START_OPTIONS;
        int position = Arrays.binarySearch(dayStartOptions, currentSetting);
        if (position < 0) {
            mOptions = new int[dayStartOptions.length + 1];
            position = -position - 1;
            System.arraycopy(dayStartOptions, 0, mOptions, 0, position);
            System.arraycopy(dayStartOptions, position, mOptions, position + 1, dayStartOptions.length - position);
            mOptions[position] = currentSetting;
        } else {
            mOptions = dayStartOptions;
        }
        return position;
    }

}
