package com.actinarium.persistence.ui.onboarding;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.BackInterceptor;
import com.actinarium.persistence.ui.common.PagerIndicator;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class OnboardingFragment extends Fragment implements BackInterceptor {

    public static final String TAG = "OnboardingFragment";

    private Host mHost;
    private OnboardingAdapter mAdapter;
    private ViewPager mPager;

    private ValueAnimator mAnimator;

    private PagerIndicator mIndicator;
    private Button mSkipOrDiscardBtn;
    private Button mNextOrDoneBtn;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHost = (Host) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager_with_controls, container, false);

        // Immediately paint green (first onboarding page)
        repaintStatusBar(true, false);

        // Get controls
        mSkipOrDiscardBtn = (Button) view.findViewById(R.id.skip_or_discard);
        mNextOrDoneBtn = (Button) view.findViewById(R.id.next_or_done);

        // Set up pager
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mAdapter = new OnboardingAdapter(getChildFragmentManager());
        mIndicator = (PagerIndicator) view.findViewById(R.id.pager_indicator);
        mIndicator.setDotCount(3);
        mPager.setAdapter(mAdapter);
        mPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.pager_spacing));

        //noinspection ResourceType
        mIndicator.setCurrentItem(mPager.getCurrentItem(), false);

        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // Set proper text and paint
                if (position == 0) {
                    repaintStatusBar(true, true);
                } else {
                    repaintStatusBar(false, true);

                    if (position == OnboardingAdapter.PAGE_ANALYTICS) {
                        mSkipOrDiscardBtn.setVisibility(View.INVISIBLE);
                        mNextOrDoneBtn.setText(R.string.done);
                    } else {
                        mSkipOrDiscardBtn.setVisibility(View.VISIBLE);
                        mNextOrDoneBtn.setText(R.string.next);
                    }
                }

                mIndicator.setCurrentItem(position, false);
            }
        });

        mSkipOrDiscardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repaintStatusBar(false, false);
                mHost.onSeenOnboarding();
            }
        });

        mNextOrDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = mPager.getCurrentItem();
                if (position == OnboardingAdapter.PAGE_ANALYTICS) {
                    // Done
                    mHost.onSeenOnboarding();
                } else {
                    // Next
                    mPager.setCurrentItem(position + 1);
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Host) getActivity()).setButtonInterceptor(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((Host) getActivity()).setButtonInterceptor(null);
    }

    @Override
    public boolean onBackPressed() {
        final int position = mPager.getCurrentItem();
        if (position > 0) {
            // Go back one page
            mPager.setCurrentItem(position - 1);
            return true;
        } else {
            // todo: probably skip onboarding and go to home? (but only if not called from help)
            return false;
        }
    }

    private void repaintStatusBar(boolean isGreen, final boolean animate) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int currentStatusBarColor = getActivity().getWindow().getStatusBarColor();
            int desiredStatusBarColor = getResources().getColor(isGreen ? R.color.accentDark : R.color.primaryDark);
            if (currentStatusBarColor == desiredStatusBarColor) {
                return;
            }
            if (animate) {
                mAnimator = new ValueAnimator();
                mAnimator.setIntValues(currentStatusBarColor, desiredStatusBarColor);
                mAnimator.setDuration(200);
                mAnimator.setEvaluator(new ArgbEvaluator());
                mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        final FragmentActivity activity = getActivity();
                        if (activity != null) {
                            activity.getWindow().setStatusBarColor((int) animation.getAnimatedValue());
                        }
                    }
                });
                mAnimator.start();
            } else {
                if (mAnimator != null && mAnimator.isRunning()) {
                    mAnimator.cancel();
                }
                getActivity().getWindow().setStatusBarColor(desiredStatusBarColor);
            }
        }
    }

    public interface Host {
        void setButtonInterceptor(BackInterceptor interceptor);
        void onSeenOnboarding();
    }
}
