package com.actinarium.persistence.ui.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class TimePickerPreference extends DialogPreference implements OnClickListener {

    private int mHours;
    private int mMinutes;
    private int mWhichButtonClicked;
    private TimePicker mPicker;

    public TimePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        int value;
        if (restorePersistedValue) {
            value = getPersistedInt(PrefUtils.SETTING_DAY_STARTS_AT_DEFAULT);
            mHours = Utils.extractHour(value);
            mMinutes = Utils.extractMinute(value);
        } else {
            value = (int) defaultValue;
            mHours = Utils.extractHour(value);
            mMinutes = Utils.extractMinute(value);
            persistInt(value);
        }
    }

    @Override
    protected void onBindDialogView(@NonNull View view) {
        super.onBindDialogView(view);
        mPicker = (TimePicker) view;
        mPicker.setCurrentHour(mHours);
        mPicker.setCurrentMinute(mMinutes);
        mPicker.setIs24HourView(DateFormat.is24HourFormat(getContext()));
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInteger(index, PrefUtils.SETTING_DAY_STARTS_AT_DEFAULT);
    }

    @Override
    protected void showDialog(Bundle state) {
        Context context = this.getContext();

        mWhichButtonClicked = DialogInterface.BUTTON_NEGATIVE;

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Theme_Persistence_Dialog)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, this);
        TimePicker picker = (TimePicker) LayoutInflater.from(builder.getContext()).inflate(R.layout.time_picker, null);
        onBindDialogView(picker);
        AlertDialog pickerDialog = builder
                .setView(picker)
                .create();
        pickerDialog.setOnDismissListener(this);

        try {
            PreferenceManager manager = getPreferenceManager();
            final Method method = manager.getClass()
                    .getDeclaredMethod("registerOnActivityDestroyListener", PreferenceManager.OnActivityDestroyListener.class);
            method.setAccessible(true);
            method.invoke(manager, this);

            final Field mDialogField = DialogPreference.class.getDeclaredField("mDialog");
            mDialogField.setAccessible(true);
            mDialogField.set(this, pickerDialog);

            if (state != null) {
                pickerDialog.onRestoreInstanceState(state);
            }
            pickerDialog.show();
        } catch (Exception e) {
            Log.wtf("CompatDialogPreference", "Well shit, reflection failed");
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        mWhichButtonClicked = which;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        try {
            PreferenceManager manager = getPreferenceManager();
            final Method method = manager.getClass()
                    .getDeclaredMethod("unregisterOnActivityDestroyListener", PreferenceManager.OnActivityDestroyListener.class);
            method.setAccessible(true);
            method.invoke(manager, this);

            final Field mDialogField = DialogPreference.class.getDeclaredField("mDialog");
            mDialogField.setAccessible(true);
            mDialogField.set(this, null);
        } catch (Exception e) {
            Log.wtf("CompatDialogPreference", "Well shit, reflection failed");
        }
        this.onDialogClosed(this.mWhichButtonClicked == DialogInterface.BUTTON_POSITIVE);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            mHours = mPicker.getCurrentHour();
            mMinutes = mPicker.getCurrentMinute();
            int value = Utils.getMinuteOfDay(mHours, mMinutes);
            persistInt(value);
        }
    }
}
