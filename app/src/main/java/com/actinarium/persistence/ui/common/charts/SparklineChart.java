package com.actinarium.persistence.ui.common.charts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.actinarium.persistence.R;

/**
 * Simplest chart displaying sparkline for challenge progress. Has two axes
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class SparklineChart extends View {

    private final int DEFAULT_AXIS_WIDTH = 2;    // dp
    private final int DEFAULT_LINE_WIDTH = 2;    // dp
    private final float DEFAULT_SPACING = 0f;  // blank area on top and bottom of the chart, in percent

    private int mAxisWidth;
    private int mLineWidth;
    private RectF mLineBounds;

    private int[] mValues;
    private int mPredictedValue;
    private boolean mShowPredicted;
    private int mDataSize;
    private int mChartSize;
    private int mMinValue;
    private int mMaxValue;

    private float mXAxisPosition;
    private float mTodayLinePosition;
    private float mColumnStep;
    private int mAxisColor;
    private int mLineColor;
    private int mPredictionColor;

    private Paint mAxisPaint;
    private Paint mSparkPaint;
    private Paint mPredictionPaint;
    private Path mSparkLinePath;
    private Path mPredictionPath;

    public SparklineChart(Context context) {
        super(context);
        init(context);
    }

    public SparklineChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SparklineChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        final Resources resources = context.getResources();
        mAxisColor = resources.getColor(R.color.chartAxes);
        mLineColor = resources.getColor(R.color.accent);
        mPredictionColor = resources.getColor(R.color.accentLight);

        final float density = resources.getDisplayMetrics().density;
        mAxisWidth = (int) (DEFAULT_AXIS_WIDTH * density + 0.5f);
        mLineWidth = (int) (DEFAULT_LINE_WIDTH * density + 0.5f);
        final int dotRadius = (int) (DEFAULT_LINE_WIDTH * density / 2 + 0.5f);

        mLineBounds = new RectF();

        mAxisPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mAxisPaint.setColor(mAxisColor);
        mAxisPaint.setStrokeWidth(mAxisWidth);

        mSparkPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSparkPaint.setStyle(Paint.Style.STROKE);
        mSparkPaint.setStrokeJoin(Paint.Join.ROUND);
        mSparkPaint.setStrokeCap(Paint.Cap.ROUND);
        mSparkPaint.setColor(mLineColor);
        mSparkPaint.setStrokeWidth(mLineWidth);

        Path predictionDotsPath = new Path();
        predictionDotsPath.addCircle(0, 0, dotRadius, Path.Direction.CW);
        mPredictionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPredictionPaint.setStyle(Paint.Style.STROKE);
        mPredictionPaint.setColor(mPredictionColor);
        mPredictionPaint.setPathEffect(new PathDashPathEffect(predictionDotsPath, dotRadius * 4, 0, PathDashPathEffect.Style.TRANSLATE));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
//        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        // todo: implement adequately if required
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (!changed) {
            return;
        }

        // Measure active chart rectangle (where path will be drawn)
        final int height = getHeight();
        final float axisCenter = mAxisWidth / 2f;

        mLineBounds.set(axisCenter, height * DEFAULT_SPACING, getWidth() - axisCenter, height * (1 - DEFAULT_SPACING));
        recalculatePath();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // First draw prospected line if any
        if (mPredictionPath != null) {
            canvas.drawPath(mPredictionPath, mPredictionPaint);
        }

        // Then draw path above
        if (mSparkLinePath != null) {
            canvas.drawPath(mSparkLinePath, mSparkPaint);
        }

        // Draw vertical axis
        canvas.drawLine(mLineBounds.left, 0, mLineBounds.left, getHeight(), mAxisPaint);
        // Draw horizontal axis
        //noinspection SuspiciousNameCombination
        canvas.drawLine(mLineBounds.left, mXAxisPosition, mLineBounds.right, mXAxisPosition, mAxisPaint);

        // todo: Draw text
    }

    /**
     * Set data for sparkline chart. Will invoke redraw
     * @param values Sparkline values, starting from the beginning
     * @param chartSize Length of chart, independent of data (e.g. to display future values)
     * @param showPredicted
     */
    public void setValues(@Nullable int[] values, int chartSize, boolean showPredicted, int predictedValue, int minValue, int maxValue) {
        mValues = values;
        mPredictedValue = predictedValue;
        mShowPredicted = showPredicted;
        mChartSize = chartSize;
        mMinValue = minValue;
        mMaxValue = maxValue;
        mDataSize = values == null ? 0 : values.length;

        recalculatePath();
        invalidate();
    }

    private void recalculatePath() {
        if (mDataSize == 0) {
            mSparkLinePath = null;
            return;
        }

        // Entry step is chart width divided by chart size
        mColumnStep = mLineBounds.width() / mChartSize;

        float x = mLineBounds.left;

        mSparkLinePath = new Path();
        mSparkLinePath.moveTo(x, mValues[0]);

        for (int i = 1; i < mDataSize; i++) {
            // Add point to path. Don't bother about min/max now, we'll resize and translate later as necessary
            x += mColumnStep;
            mSparkLinePath.lineTo(x, mValues[i]);
        }

        // Horizontal axis position
        if (mMinValue == -mMaxValue) {
            mXAxisPosition = (int) mLineBounds.centerY();
        } else {
            mXAxisPosition = (int) (mLineBounds.top + mMaxValue / (double)(mMaxValue - mMinValue) * mLineBounds.height());
        }
        if (mAxisWidth % 2 == 1) {
            mXAxisPosition += 0.5f;
        }

        // Transform path: fit vertically to chart bounds, translate if required
        Matrix matrix = new Matrix();
        if (mMaxValue != mMinValue) {
            // Also rotate chart
            matrix.setScale(1f, mLineBounds.height() / (mMinValue - mMaxValue));
        }
        matrix.postTranslate(0, mXAxisPosition);
        mSparkLinePath.transform(matrix);

        // Predicted value
        if (mShowPredicted) {
            mPredictionPath = new Path();
            mPredictionPath.moveTo(x, mValues[mDataSize - 1]);
            x += mColumnStep;
            mPredictionPath.lineTo(x, mPredictedValue);

            mPredictionPath.transform(matrix);
        }
    }
}
