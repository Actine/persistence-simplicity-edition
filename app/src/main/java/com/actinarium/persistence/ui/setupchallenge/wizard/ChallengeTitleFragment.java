package com.actinarium.persistence.ui.setupchallenge.wizard;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import com.actinarium.persistence.R;
import com.actinarium.persistence.dto.Challenge;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeTitleFragment extends Fragment implements ChallengeWizardFragment.ModifiesChallenge {

    public static final String TAG = "ChallengeTitleFragment";

    private static final String ARG_CURRENT_HINT = "com.actinarium.persistence.intent.arg.CURRENT_HINT";
    private static final String ARG_IS_TITLE_VALID = "com.actinarium.persistence.intent.arg.IS_TITLE_VALID";

    private ChallengeWizardFragment.Host mHost;

    private EditText mChallengeName;
    private EditText mChallengeDescription;

    private boolean mIsValid;
    private int mCurrentHint;
    private boolean mIsChallengeNameRtl;
    private ImageButton mNextHintBtn;
    private int mDefaultEndPadding;
    private int mMNextHintButtonWidth;
    private String[] mHints;

    public ChallengeTitleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHost = (ChallengeWizardFragment.Host) activity;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_CURRENT_HINT, mCurrentHint);
        outState.putBoolean(ARG_IS_TITLE_VALID, mIsValid);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ChallengeWizardAdapter adapter = mHost.getWizardAdapter();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cw_first_challenge, container, false);

        mChallengeName = (EditText) view.findViewById(R.id.challenge_name);
        mChallengeName.setMaxLines(Integer.MAX_VALUE);
        mChallengeName.setHorizontallyScrolling(false);

        mChallengeDescription = (EditText) view.findViewById(R.id.challenge_description);
        mChallengeDescription.setMaxLines(Integer.MAX_VALUE);
        mChallengeDescription.setHorizontallyScrolling(false);

        mNextHintBtn = (ImageButton) view.findViewById(R.id.next_hint);
        //noinspection deprecation
        mNextHintBtn.setAlpha(80);

        // Relevant to increasing padding when refresh button is shown
        mIsChallengeNameRtl = Build.VERSION.SDK_INT > 16
                && mChallengeName.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
        mDefaultEndPadding = mIsChallengeNameRtl ? mChallengeName.getPaddingLeft() : mChallengeName.getPaddingRight();
        mMNextHintButtonWidth = getResources().getDimensionPixelSize(R.dimen.next_hint_btn_width);

        mHints = getResources().getStringArray(R.array.ob_first_challenge_suggestions);

        if (savedInstanceState == null) {
            final Challenge challenge = mHost.getChallenge();
            mChallengeName.setText(challenge.title);
            mChallengeDescription.setText(challenge.description);
            mIsValid = challenge.title != null && !challenge.title.isEmpty();
//            insetChallengeName(!mIsValid);
            mCurrentHint = 0;
        } else {
            mCurrentHint = savedInstanceState.getInt(ARG_CURRENT_HINT, 0);
            mIsValid = savedInstanceState.getBoolean(ARG_IS_TITLE_VALID, false);
        }
        updateHint();

        mNextHintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentHint++;
                updateHint();
            }
        });

        mChallengeName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                final String text = s.toString();
                if (mCurrentHint < mHints.length - 1) {
                    final boolean isHintShown = text.isEmpty();
                    mNextHintBtn.setVisibility(isHintShown ? View.VISIBLE : View.INVISIBLE);
                    insetChallengeName(isHintShown);
                }

                final boolean isEmpty = text.trim().isEmpty();
                if (isEmpty && mIsValid) {
                    mIsValid = false;
                    adapter.setPageLock(ChallengeWizardAdapter.PAGE_FIRST_CHALLENGE, true);
                } else if (!isEmpty && !mIsValid) {
                    mIsValid = true;
                    adapter.setPageLock(ChallengeWizardAdapter.PAGE_FIRST_CHALLENGE, false);
                }
            }
        });

        return view;
    }

    @Override
    public void writeToChallenge(Challenge challenge) {
        // Save input into challenge
        challenge.title = mChallengeName.getText().toString().trim();
        final String description = mChallengeDescription.getText().toString().trim();
        challenge.description = description.isEmpty() ? null : description;
    }

    private void updateHint() {
        if (mCurrentHint < mHints.length) {
            mChallengeName.setHint(mHints[mCurrentHint]);
        }

        if (mCurrentHint == mHints.length - 1) {
            mNextHintBtn.setVisibility(View.INVISIBLE);
            insetChallengeName(false);
        }

        mChallengeName.requestLayout();
    }

    private void insetChallengeName(boolean hasIcon) {
        if (mIsChallengeNameRtl) {
            mChallengeName.setPadding(
                    hasIcon ? mDefaultEndPadding + mMNextHintButtonWidth : mDefaultEndPadding,
                    mChallengeName.getPaddingTop(),
                    mChallengeName.getPaddingRight(),
                    mChallengeName.getPaddingBottom()
            );
        } else {
            mChallengeName.setPadding(
                    mChallengeName.getPaddingLeft(),
                    mChallengeName.getPaddingTop(),
                    hasIcon ? mDefaultEndPadding + mMNextHintButtonWidth : mDefaultEndPadding,
                    mChallengeName.getPaddingBottom()
            );
        }
    }
}
