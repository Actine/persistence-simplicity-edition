package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class DiscardChangesDialogFragment extends DialogFragment {

    public static final String TAG = "DiscardChangesDialogFragment";

    public static final String ARG_IS_EDIT = "com.actinarium.persistence.intent.arg.IS_EDIT";

    private Host mHost;
    private Context mContext;

    public static DiscardChangesDialogFragment newInstance(boolean isEdit) {
        DiscardChangesDialogFragment fragment = new DiscardChangesDialogFragment();
        Bundle args = new Bundle(1);
        args.putBoolean(ARG_IS_EDIT, isEdit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean isEdit = getArguments().getBoolean(ARG_IS_EDIT, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        if (isEdit) {
            builder.setMessage(R.string.dialog_discard_changes_message);
        } else {
            builder.setMessage(R.string.dialog_discard_challenge_message);
        }
        builder
                .setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onChallengeDiscarded();
                    }
                })
                .setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {
        void onChallengeDiscarded();
    }
}
