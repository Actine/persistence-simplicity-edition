package com.actinarium.persistence.ui.common;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.ArrayList;

/**
 * Radio group for mutually exclusive radio items that may reside anywhere in view hierarchy
 *
 * @author Paul Danyliuk
 */
public class RadioGroup {

    private ArrayList<RadioButton> mItems;
    private OnChangeListener mOnChangeListener;
    private CompoundButton.OnCheckedChangeListener mRadioListener;

    public RadioGroup(int initialCapacity) {
        mItems = new ArrayList<>(initialCapacity);
        mRadioListener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // prevent recursion by not processing un-checks
                if (!isChecked) {
                    return;
                }

                int size = mItems.size();
                int index = -1;
                for (int i = 0; i < size; i++) {
                    RadioButton button = mItems.get(i);
                    if (!button.equals(buttonView)) {
                        button.setChecked(false);
                    } else {
                        index = i;
                    }
                }

                if (mOnChangeListener != null) {
                    mOnChangeListener.onSelectionChanged(buttonView.getId(), index);
                }
            }
        };
    }

    public void setOnChangeListener(OnChangeListener listener) {
        mOnChangeListener = listener;
    }

    public void addRadioButton(final RadioButton button, @Nullable View label) {
        button.setOnCheckedChangeListener(mRadioListener);
        if (label != null) {
            label.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    button.setChecked(true);
                }
            });
        }
        mItems.add(button);
    }

    public void selectRadio(int index) {
        if (index >= 0 && index < mItems.size()) {
            mItems.get(index).setChecked(true);
        }
    }

    public interface OnChangeListener {
        void onSelectionChanged(int viewId, int index);
    }


}
