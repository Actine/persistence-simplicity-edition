package com.actinarium.persistence.ui.support;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actinarium.persistence.R;
import com.actinarium.persistence.ui.common.ViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {

    public static final String TAG = "SupportFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_support, container, false);

        // todo: think of how to best present toolbar. Probably not pinned
        final ActionBar actionBar = ViewUtils.setUpToolbar(getActivity(), view, 0, 0);
        actionBar.setDisplayHomeAsUpEnabled(true);

        return view;
    }

}
