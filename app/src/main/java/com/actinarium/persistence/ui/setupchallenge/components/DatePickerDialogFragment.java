package com.actinarium.persistence.ui.setupchallenge.components;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.DatePicker;
import com.actinarium.persistence.R;

import java.util.Calendar;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class DatePickerDialogFragment extends DialogFragment {

    public static final String TAG = "DatePickerDialogFragment";

    private static final String ARG_ID = "com.actinarium.persistence.intent.arg.ID";

    private Host mHost;
    private Context mContext;

    public static DatePickerDialogFragment newInstance(int id) {
        DatePickerDialogFragment fragment = new DatePickerDialogFragment();
        Bundle args = new Bundle(1);
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.Theme_Persistence_Dialog);
        final DatePicker picker = (DatePicker) LayoutInflater.from(builder.getContext()).inflate(R.layout.date_picker, null);
        final int pickerId = getPickerId();
        final Calendar calendar = mHost.getCalendarForDatePicker(pickerId);
        picker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        builder
                .setView(picker)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        calendar.set(picker.getYear(), picker.getMonth(), picker.getDayOfMonth());
                        mHost.saveDatePickerResult(pickerId, calendar);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

    private int getPickerId() {
        return getArguments().getInt(ARG_ID);
    }

    public interface Host {
        Calendar getCalendarForDatePicker(int id);
        void saveDatePickerResult(int id, Calendar calendar);
    }

}
