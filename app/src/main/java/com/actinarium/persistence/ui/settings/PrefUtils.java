package com.actinarium.persistence.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.actinarium.persistence.dto.Challenge;

/**
 * Preferences
 *
 * @author Paul Danyliuk
 */
public final class PrefUtils {

    private PrefUtils() {}

    /**
     * Minutes since midnight on a normal (non-DST) day, default is 5:00 AM
     */
    static final String SETTING_DAY_STARTS_AT = "com.actinarium.persistence.settings.DAY_STARTS_AT";
    static final int SETTING_DAY_STARTS_AT_DEFAULT = 300;

    /**
     * For the first few versions of the app, there will be these pre-defined options (well, until I make a custom control)
     */
    public static final int[] DAY_START_OPTIONS = {0, 3*60, 5*60, 8*60, 10*60, 12*60, 15*60};

    public static final String SETTING_CUSTOM_UNITS = "com.actinarium.persistence.settings.CUSTOM_UNITS";
    public static final char CUSTOM_UNITS_DELIMITER = '\u2026';

    static final String SETTING_ALLOW_ANALYTICS = "com.actinarium.persistence.settings.ALLOW_ANALYTICS";

    static final String SETTING_RESET_CUSTOM_UNITS = "com.actinarium.persistence.settings.RESET_CUSTOM_UNITS";
    static final String SETTING_VIEW_PRIVACY_POLICY = "com.actinarium.persistence.settings.VIEW_PRIVACY_POLICY";

    private static final String SETTING_SEEN_ONBOARDING = "com.actinarium.persistence.settings.SEEN_ONBOARDING";
    private static final String SETTING_CREATED_CHALLENGE = "com.actinarium.persistence.settings.PROCEEDED_TO_CREATE_CHALLENGE";
    private static final String SETTING_FIRST_CHALLENGE_START_DAY = "com.actinarium.persistence.settings.FIRST_CHALLENGE_START_DAY";

    public static int getNewDayOffset(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(SETTING_DAY_STARTS_AT, SETTING_DAY_STARTS_AT_DEFAULT);
    }

    public static void setNewDayOffset(Context context, int minuteOfDay) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putInt(SETTING_DAY_STARTS_AT, minuteOfDay)
                .apply();
    }

    public static boolean hasSeenOnboarding(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTING_SEEN_ONBOARDING, false);
    }

    public static void setSeenOnboarding(Context context, boolean hasSeen) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(SETTING_SEEN_ONBOARDING, hasSeen)
                .apply();
    }

    public static boolean hasCreatedChallenge(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTING_CREATED_CHALLENGE, false);
    }

    public static void setCreatedChallenge(Context context, boolean hasProceeded) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(SETTING_CREATED_CHALLENGE, hasProceeded)
                .apply();
    }

    public static boolean isAnalyticsAllowed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTING_ALLOW_ANALYTICS, true);
    }

    public static void setAnalyticsAllowed(Context context, boolean isAllowed) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(SETTING_ALLOW_ANALYTICS, isAllowed)
                .apply();
    }

    /**
     * Store {@link Challenge#startedOn} <b>once</b> when the first challenge is started, to mark a reference point for
     * donation nag
     *
     * @param context
     * @param epochDay
     */
    public static void trySetFirstChallengeStartDay(Context context, int epochDay) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!preferences.contains(SETTING_FIRST_CHALLENGE_START_DAY)) {
            preferences.edit().putInt(SETTING_FIRST_CHALLENGE_START_DAY, epochDay).apply();
        }
    }

    public static boolean hasDonated(Context context) {
        return false;
    }
}
