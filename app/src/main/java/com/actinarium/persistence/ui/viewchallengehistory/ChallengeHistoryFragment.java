package com.actinarium.persistence.ui.viewchallengehistory;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actinarium.persistence.R;
import com.actinarium.persistence.data.PersistenceContract;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.ui.common.ViewUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeHistoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = "ChallengeHistoryFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";

    private Challenge mChallenge;
    private Cursor mProgressCursor;

    public static ChallengeHistoryFragment newInstance(Challenge challenge) {
        ChallengeHistoryFragment fragment = new ChallengeHistoryFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(ARG_CHALLENGE, challenge);
        fragment.setArguments(args);
        return fragment;
    }

    public ChallengeHistoryFragment() {
        // todo: re-enable options menu (i.e. sorting) when added. Actually history view is not crucial for 0.7a release
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        final View view = inflater.inflate(R.layout.fragment_challenge_history, container, false);
        mChallenge = getArguments().getParcelable(ARG_CHALLENGE);

        final ActionBar actionBar = ViewUtils.setUpToolbar(activity, view, 0, R.dimen.actionBarElevation);
        actionBar.setDisplayHomeAsUpEnabled(true);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(0, null, this);
    }

    // ------------------- BIND DATA ----------------

    private void onCursorLoaded(Cursor cursor) {
        mProgressCursor = cursor;
        // todo: set cursor to history recycler view adapter
    }

    // ------------------- LOADER -------------------

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                getActivity(),
                PersistenceContract.ProgressEntry.CONTENT_URI,
                null,
                PersistenceContract.BuildingBlocks.SELECTION_CHALLENGE_ID,
                new String[]{Long.toString(mChallenge.id)},
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Old cursor is managed (closed) implicitly
        onCursorLoaded(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // Old cursor is managed (closed) implicitly
        onCursorLoaded(null);
    }


}
