package com.actinarium.persistence.ui.setupchallenge.wizard;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.actinarium.persistence.R;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class RunWizardDialogFragment extends DialogFragment {

    public static final String TAG = "RunWizardDialogFragment";

    private Host mHost;
    private Context mContext;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mHost = (Host) activity;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(mContext)
                .setTitle(R.string.dialog_run_wizard_title)
                .setMessage(R.string.dialog_run_wizard_message)
                .setPositiveButton(R.string.dialog_run_wizard_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onWizardConfirmed();
                    }
                })
                .setNegativeButton(R.string.dialog_run_wizard_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mHost.onWizardRejected();
                    }
                })
                .create();
    }

    /**
     * The activity that manages this dialog must implement this callback interface
     */
    public interface Host {
        void onWizardConfirmed();
        void onWizardRejected();
    }

}
