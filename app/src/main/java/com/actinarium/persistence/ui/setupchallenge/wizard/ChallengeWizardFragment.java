package com.actinarium.persistence.ui.setupchallenge.wizard;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.ApplicationContext;
import com.actinarium.persistence.common.BackInterceptor;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.dto.Reminder;
import com.actinarium.persistence.dto.Rule;
import com.actinarium.persistence.service.PersistenceService;
import com.actinarium.persistence.ui.common.PagerIndicator;
import com.actinarium.persistence.ui.common.SwitchableButton;
import com.actinarium.persistence.ui.settings.PrefUtils;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ChallengeWizardFragment extends Fragment implements BackInterceptor, ChallengeWizardAdapter.PageLockChangeListener, ApplicationContext.ChallengeSavedListener {

    public static final String TAG = "ChallengeWizardFragment";

    private static final String ARG_CHALLENGE = "com.actinarium.persistence.intent.arg.CHALLENGE";
    private static final String ARG_IS_SAVING = "com.actinarium.persistence.intent.arg.IS_SAVING";

    private Host mHost;
    private ChallengeWizardAdapter mAdapter;
    private SwitchableButton mNextOrDoneBtn;
    private ViewPager mPager;

    private Challenge mChallenge;
    private ValueAnimator mAnimator;

    private boolean mIsSaving;
    private PagerIndicator mIndicator;
    private Button mBackOrCancelBtn;
    private String mValidationMessage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mHost = (Host) context;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ARG_CHALLENGE, mChallenge);
        outState.putBoolean(ARG_IS_SAVING, mIsSaving);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager_with_controls, container, false);

        if (savedInstanceState == null) {
            mChallenge = makeDefaultOnboardingChallenge();
        } else {
            mChallenge = savedInstanceState.getParcelable(ARG_CHALLENGE);
            mIsSaving = savedInstanceState.getBoolean(ARG_IS_SAVING);
        }

        // Get controls
        mBackOrCancelBtn = (Button) view.findViewById(R.id.skip_or_discard);
        mNextOrDoneBtn = (SwitchableButton) view.findViewById(R.id.next_or_done);

        // Set up pager
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mAdapter = new ChallengeWizardAdapter(getChildFragmentManager(), this);
        mIndicator = (PagerIndicator) view.findViewById(R.id.pager_indicator);
        mIndicator.setDotCount(5);
        mPager.setAdapter(mAdapter);
        mPager.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.pager_spacing));

        final int[] prevPosition = {mPager.getCurrentItem()};
        final ViewPager.SimpleOnPageChangeListener listener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // If previous page stores info to the challenge, invoke it
                Fragment previousPage = mAdapter.getExistingItem(prevPosition[0]);
                if (previousPage instanceof ModifiesChallenge) {
                    ((ModifiesChallenge) previousPage).writeToChallenge(mChallenge);
                }
                prevPosition[0] = position;

                // Set proper text and paint
                if (position == ChallengeWizardAdapter.PAGE_STARTING) {
                    mBackOrCancelBtn.setText(R.string.back);
                    mNextOrDoneBtn.setText(R.string.ob_lets_do_this);
                } else if (position == ChallengeWizardAdapter.PAGE_FIRST_CHALLENGE) {
                    mBackOrCancelBtn.setText(R.string.cancel);
                    mNextOrDoneBtn.setText(R.string.next);
                } else {
                    mBackOrCancelBtn.setText(R.string.back);
                    mNextOrDoneBtn.setText(R.string.next);
                }

                // Enable or disable "next"/"done" button, redraw indicator
                onNextPageLockCheck(position);
            }
        };
        mPager.addOnPageChangeListener(listener);
        if (prevPosition[0] == 0) {
            listener.onPageSelected(0);
        }

        mBackOrCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int page = mPager.getCurrentItem();
                if (page == ChallengeWizardAdapter.PAGE_FIRST_CHALLENGE) {
                    mHost.onCancelChallengeWizard();
                } else {
                    onBackPressed();
                }
            }
        });

        mNextOrDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = mPager.getCurrentItem();
                if (position == ChallengeWizardAdapter.PAGE_STARTING) {
                    // Done
                    if (mIsSaving) {
                        // Suppress duplicate done clicks
                        return;
                    }

                    Fragment page = mAdapter.getExistingItem(position);
                    if (page instanceof ModifiesChallenge) {
                        ((ModifiesChallenge) page).writeToChallenge(mChallenge);
                    }

                    // Send request to save challenge - and close onboarding in onChallengeSaved
                    PersistenceService.createChallenge(getActivity(), mChallenge, true);
                } else {
                    // Next
                    mPager.setCurrentItem(position + 1);
                }
            }
        });

        mNextOrDoneBtn.setOnDisabledClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), mValidationMessage, Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Host) getActivity()).setButtonInterceptor(this);
        ApplicationContext.get().setChallengeSavedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((Host) getActivity()).setButtonInterceptor(null);
        ApplicationContext.get().setChallengeSavedListener(null);
    }

    public Challenge getChallenge() {
        return mChallenge;
    }

    public ChallengeWizardAdapter getWizardAdapter() {
        return mAdapter;
    }

    @Override
    public boolean onBackPressed() {
        final int position = mPager.getCurrentItem();
        if (position > 0) {
            // Go back one page
            mPager.setCurrentItem(position - 1);
            return true;
        } else {
            // todo: probably skip onboarding and go to home? (but only if not called from help)
            return false;
        }
    }

    @Override
    public void onPageLocksChanged() {
        onNextPageLockCheck(mPager.getCurrentItem());
    }

    private void onNextPageLockCheck(int currentPosition) {
        final int lockedPage = mAdapter.getCount() - 1;
        final boolean isLocked = currentPosition == lockedPage && currentPosition != ChallengeWizardAdapter.PAGE_STARTING;
        mNextOrDoneBtn.setEnabled(!isLocked);
        mIndicator.setCurrentItem(currentPosition, isLocked);
        if (lockedPage == ChallengeWizardAdapter.PAGE_FIRST_CHALLENGE) {
            mValidationMessage = getString(R.string.invalid_title);
        } else {
            mValidationMessage = getString(R.string.invalid_rule);
        }
    }

    private Challenge makeDefaultOnboardingChallenge() {
        final int today = Utils.getCurrentEpochDay(getActivity());
        Challenge challenge = new Challenge();

        Rule[] rules = new Rule[2];
        rules[0] = new Rule(25, Rule.FLAG_ALL, null, null);
        rules[1] = new Rule(60, Rule.FLAG_SATURDAY | Rule.FLAG_SUNDAY, null, null);
        challenge.setRules(rules);

        Reminder[] reminders = new Reminder[1];
        // Remind 8 hours before new day offset
        int reminderTime = (PrefUtils.getNewDayOffset(getActivity()) + Utils.MINUTES_IN_16_H) % Utils.MINUTES_IN_24_H;
        reminders[0] = new Reminder(reminderTime, Reminder.FLAG_FIRE_ALWAYS);
        challenge.setReminders(reminders);

        return challenge;
    }

    @Override
    public void onChallengeSaved(Challenge challenge) {
        // Remember that the user is now educated in how to create a challenge
        PrefUtils.setCreatedChallenge(getContext(), true);

        // Exit onboarding
        mHost.onWizardChallengeSaved(challenge);
    }

    public interface Host {
        void setButtonInterceptor(BackInterceptor interceptor);
        void onWizardChallengeSaved(Challenge challenge);
        void onCancelChallengeWizard();
        Challenge getChallenge();
        ChallengeWizardAdapter getWizardAdapter();
    }

    public interface ModifiesChallenge {
        void writeToChallenge(Challenge challenge);
    }
}
