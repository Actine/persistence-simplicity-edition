package com.actinarium.persistence.ui.viewchallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import com.actinarium.persistence.R;
import com.actinarium.persistence.common.Utils;
import com.actinarium.persistence.dto.Challenge;
import com.actinarium.persistence.service.PersistenceService;
import com.actinarium.persistence.ui.home.HomeFragment;
import com.actinarium.persistence.ui.setupchallenge.components.StartChallengeDialogFragment;
import com.actinarium.persistence.ui.viewchallenge.components.DeleteChallengeDialogFragment;

public class ViewChallengeActivity extends AppCompatActivity
        implements StartChallengeDialogFragment.Host,DeleteChallengeDialogFragment.Host {

    public static final String EXTRA_CHALLENGE = "com.actinarium.persistence.intent.extra.CHALLENGE";

    public static void launch(Context context, Challenge challenge) {
        Intent intent = new Intent(context, ViewChallengeActivity.class);
        intent.putExtra(EXTRA_CHALLENGE, challenge);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment_host);

        if (savedInstanceState == null) {
            final Challenge challenge = getIntent().getParcelableExtra(EXTRA_CHALLENGE);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_host, ViewChallengeFragment.newInstance(challenge), HomeFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartChallengeConfirmed(Challenge challenge) {
        final int today = Utils.getCurrentEpochDay(this);
        challenge.startedOn = today;
        challenge.statusFlags |= Challenge.FLAG_ACTIVE;
        challenge.rulesSetOn = today;
        PersistenceService.startChallenge(this, challenge);
    }

    @Override
    public void onCancelChallengeConfirmed(long challengeId) {
        finish();
        PersistenceService.deleteChallenge(this, challengeId);
    }
}
