package com.actinarium.persistence.ui.common;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Parcel;
import android.text.Layout;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.style.LeadingMarginSpan;

/**
 * Bullet list span, copied from SDK but made more customizable
 *
 * @author Paul Danyliuk
 */
public class NumberSpan implements LeadingMarginSpan {
    private final int mLeadingMargin;
    private final int mValue;

    private static Path sBulletPath = null;
    private Layout mLayout;

    public NumberSpan(int leadingMargin, int value) {
        mLeadingMargin = leadingMargin;
        mValue = value;
    }

    public NumberSpan(Parcel src) {
        mLeadingMargin = src.readInt();
        mValue = src.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mLeadingMargin);
        dest.writeInt(mValue);
    }

    public int getLeadingMargin(boolean first) {
        return mLeadingMargin;
    }

    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir,
                                  int top, int baseline, int bottom,
                                  CharSequence text, int start, int end,
                                  boolean first, Layout l) {

        if (((Spanned) text).getSpanStart(this) == start) {
            if (mLayout == null) {
                mLayout = new StaticLayout(String.valueOf(mValue) + '.', l.getPaint(), mLeadingMargin,
                        Layout.Alignment.ALIGN_NORMAL, l.getSpacingMultiplier(), l.getSpacingAdd(), false);
            }

            c.save();
            c.translate(x, top);
            mLayout.draw(c);
            c.restore();
        }
    }
}
