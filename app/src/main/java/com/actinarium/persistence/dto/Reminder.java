package com.actinarium.persistence.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Reminder implements Parcelable {

    public static final int FLAG_FIRE_ALWAYS = 0b00;
    public static final int FLAG_FIRE_IF_GOAL_NOT_MET = 0b01;
    public static final int FLAG_FIRE_IF_NO_PROGRESS = 0b10;

    public static final int FLAG_FIRE_TYPE_MASK = 0b11;

    public Integer time;
    public int flags;

    public Reminder() {}

    public Reminder(@Nullable Integer time, int flags) {
        this.time = time;
        this.flags = flags;
    }

    public int getFlags() {
        return flags & FLAG_FIRE_TYPE_MASK;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Reminder reminder = (Reminder) o;

        if (flags != reminder.flags) { return false; }
        return !(time != null ? !time.equals(reminder.time) : reminder.time != null);
    }

    @Override
    public int hashCode() {
        int result = time != null ? time.hashCode() : 0;
        result = 31 * result + flags;
        return result;
    }

    // Parcelable stuff

    private Reminder(Parcel in) {
        time = in.readInt();
        if (time < 0) {
            time = null;
        }
        flags = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(time == null ? -1 : time);
        dest.writeInt(this.flags);
    }

    public static final Parcelable.Creator<Reminder> CREATOR = new Parcelable.Creator<Reminder>() {
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };
}
