package com.actinarium.persistence.dto;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import com.actinarium.persistence.common.CursorReaderUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Scanner;

import static com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Challenge implements Parcelable {

    /**
     * If set, challenge is visible in the list
     */
    public static final int FLAG_DISPLAYED = 1;
    /**
     * If set, record is tracked every day and notifications appear
     */
    public static final int FLAG_ACTIVE = 2;
    /**
     * If set, penalties don't grow progressively (i.e. underachievement is not multiplied by 1.5). Reserved
     */
    public static final int FLAG_HIATUS = 4;

    public static final int PENDING_NAG_AFTER_DAYS = 2;
    public static final int RULES_EDIT_LOCK_DAYS = 7;

    public static final int STARTING_NOW = 0;
    public static final int STARTING_LATER = 1;

    public static final int STATUS_ON_TRACK = 0;
    public static final int STATUS_BACK_ON_TRACK = 1;
    public static final int STATUS_IN_THE_ZONE = 2;
    public static final int STATUS_HICCUPS = 3;
    public static final int STATUS_NOT_GOOD = 4;
    public static final int STATUS_CRITICAL = 5;

    public long id;
    public String uuid;
    public String title;
    public String description;
    public int displayPriority;
    public int statusFlags;
    public int createdOn;
    public Integer startedOn;
    public int rulesSetOn;
    public Long prototypeId;
    public String units;
    public Integer stoppedOn;
    public String stopReason;

    @ChallengeStatus
    public int status;

    private String rulesRaw;
    private String remindersRaw;

    private boolean rulesChanged = false;
    private boolean remindersChanged = false;

    // Array of decoded rules, always sorted from less important to more important
    private Rule[] rules;
    // Array of decoded reminder rules
    private Reminder[] reminders;

    public Challenge() {}

    public ContentValues toContentValuesOnCreate() {
        if (rulesChanged) {
            encodeRules();
        }
        if (remindersChanged) {
            encodeReminders();
        }

        ContentValues values = new ContentValues(12);
        values.put(ChallengeEntry.COL_UUID, uuid);
        values.put(ChallengeEntry.COL_TITLE, title);
        values.put(ChallengeEntry.COL_DESCRIPTION, description);
        values.put(ChallengeEntry.COL_DISPLAY_PRIORITY, displayPriority);
        values.put(ChallengeEntry.COL_STATUS_FLAGS, statusFlags);
        values.put(ChallengeEntry.COL_CREATED_ON, createdOn);
        values.put(ChallengeEntry.COL_STARTED_ON, startedOn);
        values.put(ChallengeEntry.COL_RULES_SET_ON, rulesSetOn);
        values.put(ChallengeEntry.COL_UNITS, units);
        values.put(ChallengeEntry.COL_PROTOTYPE_ID, prototypeId);
        values.put(ChallengeEntry.COL_RULES, rulesRaw);
        values.put(ChallengeEntry.COL_REMINDERS, remindersRaw);
        return values;
    }

    public ContentValues toContentValuesOnUpdate() {
        if (rulesChanged) {
            encodeRules();
        }
        if (remindersChanged) {
            encodeReminders();
        }

        ContentValues values = new ContentValues(10);
        values.put(ChallengeEntry.COL_TITLE, title);
        values.put(ChallengeEntry.COL_DESCRIPTION, description);
        values.put(ChallengeEntry.COL_STATUS_FLAGS, statusFlags);
        values.put(ChallengeEntry.COL_RULES_SET_ON, rulesSetOn);
        if (startedOn != null) {
            values.put(ChallengeEntry.COL_STARTED_ON, startedOn);
        }
        if (units != null) {
            values.put(ChallengeEntry.COL_UNITS, units);
        }
        if (stoppedOn != null) {
            values.put(ChallengeEntry.COL_STOPPED_ON, stoppedOn);
        }
        if (stopReason != null) {
            values.put(ChallengeEntry.COL_STOP_REASON, stopReason);
        }
        if (rulesRaw != null) {
            values.put(ChallengeEntry.COL_RULES, rulesRaw);
        }
        if (remindersRaw != null) {
            values.put(ChallengeEntry.COL_REMINDERS, remindersRaw);
        }
        return values;
    }

    public Challenge fillFromCursor(Cursor cursor) {
        id = CursorReaderUtils.getId(cursor, ChallengeEntry.TABLE_NAME);

        uuid = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_UUID);
        title = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_TITLE);
        description = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_DESCRIPTION);
        displayPriority = CursorReaderUtils.getInt(cursor, ChallengeEntry.COL_DISPLAY_PRIORITY);
        statusFlags = CursorReaderUtils.getInt(cursor, ChallengeEntry.COL_STATUS_FLAGS);
        createdOn = CursorReaderUtils.getInt(cursor, ChallengeEntry.COL_CREATED_ON);
        startedOn = CursorReaderUtils.getNullableInt(cursor, ChallengeEntry.COL_STARTED_ON);
        stoppedOn = CursorReaderUtils.getNullableInt(cursor, ChallengeEntry.COL_STOPPED_ON);
        rulesSetOn = CursorReaderUtils.getInt(cursor, ChallengeEntry.COL_RULES_SET_ON);
        units = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_UNITS);
        prototypeId = CursorReaderUtils.getNullableLong(cursor, ChallengeEntry.COL_PROTOTYPE_ID);
        stopReason = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_STOP_REASON);
        rulesRaw = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_RULES);
        remindersRaw = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_REMINDERS);

        // Reset cached arrays
        rules = null;
        reminders = null;

        return this;
    }

    public Rule[] getRules() {
        if (rules == null && rulesRaw != null) {
            decodeRules();
        }
        return rules;
    }

    public Reminder[] getReminders() {
        if (reminders == null && remindersRaw != null) {
            decodeReminders();
        }
        return reminders;
    }

    public Challenge setRules(Rule[] rules) {
        this.rules = rules;
        rulesChanged = true;
        return this;
    }

    public Challenge setReminders(Reminder[] reminders) {
        this.reminders = reminders;
        remindersChanged = true;
        return this;
    }

    public boolean isActive() {
        return (statusFlags & FLAG_ACTIVE) != 0;
    }

    public boolean isOver() {
        return (statusFlags & (FLAG_ACTIVE | FLAG_DISPLAYED)) == 0;
    }

    public void setIsOverFlags() {
        statusFlags &= ~(FLAG_ACTIVE | FLAG_DISPLAYED);
    }

    public boolean areRulesEditableOn(int day) {
        return day >= getEditableSince();
    }

    public int getEditableSince() {
        return rulesSetOn + RULES_EDIT_LOCK_DAYS;
    }

    private void decodeRules() {
        // todo: replace with String.split; would be more effective
        Scanner scanner = new Scanner(rulesRaw);
        final int length = scanner.nextInt();
        int quantity;
        int weekdayFlags;
        String startDay;
        String endDay;
        rules = new Rule[length];
        for (int i = 0; i < length; i++) {
            quantity = Integer.parseInt(scanner.next());
            weekdayFlags = Integer.parseInt(scanner.next());
            startDay = scanner.next();
            endDay = scanner.next();
            rules[i] = new Rule(
                    quantity,
                    weekdayFlags,
                    Rule.DB_NULL.equals(startDay) ? null : Integer.valueOf(startDay),
                    Rule.DB_NULL.equals(endDay) ? null : Integer.valueOf(endDay)
            );
            rules[i].priority = i;
        }
        scanner.close();
    }

    private void decodeReminders() {
        Scanner scanner = new Scanner(remindersRaw);
        final int length = scanner.nextInt();
        reminders = new Reminder[length];
        for (int i = 0; i < length; i++) {
            reminders[i] = new Reminder(
                    Integer.parseInt(scanner.next()),
                    Byte.parseByte(scanner.next())
            );
        }
        scanner.close();
    }

    private void encodeRules() {
        if (rules == null) {
            rulesRaw = null;
            return;
        }

        final int length = rules.length;
        // Statistically, we should have length of 3 + items*(3+2+5+5+4spaces) = 3 + items*19
        StringBuilder builder = new StringBuilder(3 + length * 19);
        builder.append(length);

        for (Rule rule : rules) {
            builder
                    .append(' ').append(rule.quantity)
                    .append(' ').append(rule.weekdayFlags)
                    .append(' ').append(rule.startDay == null ? Rule.DB_NULL : rule.startDay)
                    .append(' ').append(rule.endDay == null ? Rule.DB_NULL : rule.endDay);
        }
        rulesRaw = builder.toString();
    }

    private void encodeReminders() {
        if (reminders == null) {
            remindersRaw = null;
            return;
        }

        final int length = reminders.length;
        // Statistically, we should have length of 2 + items*(4+2+2spaces) = 2 + items*8
        StringBuilder builder = new StringBuilder(2 + length * 8);
        builder.append(length);

        for (Reminder reminder : reminders) {
            builder
                    .append(' ').append(reminder.time)
                    .append(' ').append(reminder.flags);
        }
        remindersRaw = builder.toString();
    }

    // Parcelable stuff

    private Challenge(Parcel in) {
        id = in.readLong();
        uuid = in.readString();
        title = in.readString();
        description = in.readString();
        displayPriority = in.readInt();
        statusFlags = in.readInt();
        units = in.readString();
        createdOn = in.readInt();
        rulesSetOn = in.readInt();

        if (in.readInt() != 0) {
            startedOn = in.readInt();
        }
        if (in.readInt() != 0) {
            prototypeId = in.readLong();
        }
        if (in.readInt() != 0) {
            stoppedOn = in.readInt();
        }

        stopReason = in.readString();
        rulesRaw = in.readString();
        remindersRaw = in.readString();

        //noinspection ResourceType
        status = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(uuid);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(displayPriority);
        dest.writeInt(statusFlags);
        dest.writeString(units);
        dest.writeInt(createdOn);
        dest.writeInt(rulesSetOn);

        dest.writeInt(startedOn != null ? 1 : 0);
        if (startedOn != null) {
            dest.writeInt(startedOn);
        }
        dest.writeInt(prototypeId != null ? 1 : 0);
        if (prototypeId != null) {
            dest.writeLong(prototypeId);
        }
        dest.writeInt(stoppedOn != null ? 1 : 0);
        if (stoppedOn != null) {
            dest.writeInt(stoppedOn);
        }

        dest.writeString(stopReason);

        if (rulesChanged) {
            encodeRules();
        }
        if (remindersChanged) {
            encodeReminders();
        }
        dest.writeString(rulesRaw);
        dest.writeString(remindersRaw);

        dest.writeInt(status);
    }

    public static final Parcelable.Creator<Challenge> CREATOR = new Parcelable.Creator<Challenge>() {
        public Challenge createFromParcel(Parcel in) {
            return new Challenge(in);
        }

        public Challenge[] newArray(int size) {
            return new Challenge[size];
        }
    };

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATUS_ON_TRACK, STATUS_BACK_ON_TRACK, STATUS_IN_THE_ZONE, STATUS_HICCUPS, STATUS_NOT_GOOD, STATUS_CRITICAL})
    public @interface ChallengeStatus {
    }
}
