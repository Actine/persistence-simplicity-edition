package com.actinarium.persistence.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Rule implements Parcelable {

    public static final int FLAG_SUNDAY = 1;
    public static final int FLAG_MONDAY = 2;
    public static final int FLAG_TUESDAY = 4;
    public static final int FLAG_WEDNESDAY = 8;
    public static final int FLAG_THURSDAY = 16;
    public static final int FLAG_FRIDAY = 32;
    public static final int FLAG_SATURDAY = 64;
    public static final int FLAG_ALL = 127;

    public static final String DB_NULL = "N";

    public static final int DEFAULT_QUANTITY = 25;
    public static final int NO_QUANTITY = -1;

    public int quantity;
    public int weekdayFlags;
    public Integer startDay;
    public Integer endDay;
    public int priority;
    volatile public boolean isTemporary;

    public static Rule getDefaultOverrideRule() {
        return new Rule(Rule.NO_QUANTITY, Rule.FLAG_SATURDAY | Rule.FLAG_SUNDAY, null, null);
    }

    public static Rule getDefaultRule() {
        return new Rule(Rule.DEFAULT_QUANTITY, Rule.FLAG_ALL, null, null);
    }

    public Rule() {}

    public Rule(int quantity, int weekdayFlags, Integer startDay, Integer endDay) {
        this.quantity = quantity;
        this.weekdayFlags = weekdayFlags;
        this.startDay = startDay;
        this.endDay = endDay;
        this.isTemporary = startDay != null || endDay != null;
    }

    public Rule(Rule source) {
        this.quantity = source.quantity;
        this.weekdayFlags = source.weekdayFlags;
        this.startDay = source.startDay;
        this.endDay = source.endDay;
        this.isTemporary = source.isTemporary;
        this.priority = source.priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        Rule rule = (Rule) o;

        if (quantity != rule.quantity) { return false; }
        if (weekdayFlags != rule.weekdayFlags) { return false; }
        if (priority != rule.priority) { return false; }
        if (startDay != null ? !startDay.equals(rule.startDay) : rule.startDay != null) { return false; }
        return !(endDay != null ? !endDay.equals(rule.endDay) : rule.endDay != null);
    }

    @Override
    public int hashCode() {
        int result = quantity;
        result = 31 * result + weekdayFlags;
        result = 31 * result + (startDay != null ? startDay.hashCode() : 0);
        result = 31 * result + (endDay != null ? endDay.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }

    // Parcelable stuff

    private Rule(Parcel in) {
        quantity = in.readInt();
        weekdayFlags = in.readInt();
        priority = in.readInt();
        boolean[] temp = new boolean[2];
        in.readBooleanArray(temp);
        if (temp[0]) {
            startDay = in.readInt();
        }
        if (temp[1]) {
            endDay = in.readInt();
        }
        isTemporary = startDay != null || endDay != null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(quantity);
        dest.writeInt(weekdayFlags);
        dest.writeInt(priority);
        dest.writeBooleanArray(new boolean[]{startDay != null, endDay != null});
        if (startDay != null) {
            dest.writeInt(startDay);
        }
        if (endDay != null) {
            dest.writeInt(endDay);
        }
    }

    public static final Parcelable.Creator<Rule> CREATOR = new Parcelable.Creator<Rule>() {
        public Rule createFromParcel(Parcel in) {
            return new Rule(in);
        }

        public Rule[] newArray(int size) {
            return new Rule[size];
        }
    };
}
