package com.actinarium.persistence.dto;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.data.PersistenceContract.ProgressEntry;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class Progress implements Parcelable {

    public static final int DEFAULT_DEBT_MULTIPLIER = 150;

    public long id;
    public long challengeId;
    public int day;
    public int dailyGoal;
    /**
     * Base debt as of the beginning of the day, i.e. without regard to this day's user's progress. To get prospected
     * debt for tomorrow in case no more progress is added use {@link #getProspectedDebt()}
     */
    public int debt;
    public int debtMultiplier;
    public int completed;
    public int compensated;
    /**
     * Base reserve balance as of the beginning of the day, i.e. not accounting current day's used compensation or, on
     * the contrary, overachievement stored. To get the delta introduced this day, use {@link #getDoubleReserveDelta()}
     */
    public int doubleReserveBalance;
    public String note;

    public Progress() {}

    public Progress(Progress source) {
        this.id = source.id;
        this.challengeId = source.challengeId;
        this.day = source.day;
        this.dailyGoal = source.dailyGoal;
        this.debt = source.debt;
        this.debtMultiplier = source.debtMultiplier;
        this.completed = source.completed;
        this.compensated = source.compensated;
        this.doubleReserveBalance = source.doubleReserveBalance;
        this.note = source.note;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(9);
        values.put(ProgressEntry.COL_CHALLENGE_ID, challengeId);
        values.put(ProgressEntry.COL_DAY, day);
        values.put(ProgressEntry.COL_DAILY_GOAL, dailyGoal);
        values.put(ProgressEntry.COL_DEBT, debt);
        values.put(ProgressEntry.COL_DEBT_MULTIPLIER, debtMultiplier);
        values.put(ProgressEntry.COL_COMPLETED, completed);
        values.put(ProgressEntry.COL_COMPENSATED, compensated);
        values.put(ProgressEntry.COL_2X_RESERVE, doubleReserveBalance);
        values.put(ProgressEntry.COL_NOTE, note);
        return values;
    }

    public Progress fillFromCursor(Cursor cursor) {
        id = CursorReaderUtils.getId(cursor, ProgressEntry.TABLE_NAME);

        challengeId = CursorReaderUtils.getLong(cursor, ProgressEntry.COL_CHALLENGE_ID);
        day = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_DAY);
        dailyGoal = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_DAILY_GOAL);
        debt = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_DEBT);
        debtMultiplier = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_DEBT_MULTIPLIER);
        completed = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_COMPLETED);
        compensated = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_COMPENSATED);
        doubleReserveBalance = CursorReaderUtils.getInt(cursor, ProgressEntry.COL_2X_RESERVE);
        note = CursorReaderUtils.getString(cursor, ProgressEntry.COL_NOTE);

        return this;
    }

    /**
     * Get the quantity of <b>actual completed</b> progress exceeding the goal. Actually you don't need to account for
     * compensation, since it must be 0 if actual progress is enough to cover the goal and debt.
     * @return quantity of exceeding the goal this day
     */
    public int getOverachievement() {
        return Math.max(completed - dailyGoal - debt, 0);
    }

    /**
     * Get the uncompleted quantity <b>without compensation</b> (since compensation is not technically achievement). To
     * obtain debt-liable amount you can safely subtract {@link #compensated} from the result of this function, since
     * the latter is constrained to [0, underachievement] range.
     * @return quantity of not meeting the goal with real progress this day
     */
    public int getUnderachievement() {
        return Math.max(dailyGoal + debt - completed, 0);
    }

    /**
     * @return Delta to {@link #doubleReserveBalance} caused by today's overachievement or compensation
     */
    public int getDoubleReserveDelta() {
        return getOverachievement() - compensated * 2;
    }

    /**
     * @return Tomorrow's debt, as of currently logged progress
     */
    public int getProspectedDebt() {
        return (int) Math.ceil((getUnderachievement() - compensated) * debtMultiplier / 100.0);
    }

    /**
     * @return spark metric as of beginning of the day. To get prospected tomorrow's spark, use {@link
     * #getProspectedSpark()}
     */
    public int getSpark() {
        return calculateSpark(doubleReserveBalance, debt);
    }

    /**
     * @return Prospected spark for tomorrow, as of currently logged progress
     */
    public int getProspectedSpark() {
        return doubleReserveBalance + getDoubleReserveDelta() - 2 * getProspectedDebt();
    }

    /**
     * Static utility method to quickly calculate spark by two given significant values
     *
     * @param doubleReserveBalance 2x the reserve amount (or 1x of unused overachievement)
     * @param debt                 Current debt value
     * @return Spark value
     */
    public static int calculateSpark(int doubleReserveBalance, int debt) {
        return doubleReserveBalance - 2 * debt;
    }

    // Parcelable stuff

    private Progress(Parcel in) {
        this.id = in.readLong();
        this.challengeId = in.readLong();
        this.day = in.readInt();
        this.dailyGoal = in.readInt();
        this.debt = in.readInt();
        this.debtMultiplier = in.readInt();
        this.completed = in.readInt();
        this.compensated = in.readInt();
        this.doubleReserveBalance = in.readInt();
        this.note = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(challengeId);
        dest.writeInt(day);
        dest.writeInt(dailyGoal);
        dest.writeInt(debt);
        dest.writeInt(debtMultiplier);
        dest.writeInt(completed);
        dest.writeInt(compensated);
        dest.writeInt(doubleReserveBalance);
        dest.writeString(note);
    }

    public static final Parcelable.Creator<Progress> CREATOR = new Parcelable.Creator<Progress>() {
        public Progress createFromParcel(Parcel in) {
            return new Progress(in);
        }

        public Progress[] newArray(int size) {
            return new Progress[size];
        }
    };
}
