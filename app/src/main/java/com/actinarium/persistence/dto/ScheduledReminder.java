package com.actinarium.persistence.dto;

import android.content.ContentValues;
import android.database.Cursor;
import com.actinarium.persistence.common.CursorReaderUtils;
import com.actinarium.persistence.data.PersistenceContract.ChallengeEntry;
import com.actinarium.persistence.data.PersistenceContract.ReminderEntry;
import com.actinarium.persistence.data.PersistenceContract.SyntheticEntry;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ScheduledReminder extends Reminder {

    public static final int FLAG_IS_SNOOZED = 256;

    public static final int SNOOZE_MINUTES = 30;

    public long id;
    public long challengeId;
    public long scheduledAt;

    // Coming from joined tables
    public String challengeTitle;
    public int totalGoal;
    public int totalProgress;

    public ScheduledReminder() {}

    public ScheduledReminder(Reminder reminder, long challengeId, long scheduledAt) {
        this.time = reminder.time;
        this.flags = reminder.flags;
        this.challengeId = challengeId;
        this.scheduledAt = scheduledAt;
    }

    public ScheduledReminder(ScheduledReminder reminder, long newScheduledAt) {
        this.time = reminder.time;
        this.flags = reminder.flags;
        this.challengeId = reminder.challengeId;
        this.challengeTitle = reminder.challengeTitle;
        this.scheduledAt = newScheduledAt;
    }

    public ContentValues toContentValuesOnCreate() {
        ContentValues values = new ContentValues(4);

        values.put(ReminderEntry.COL_CHALLENGE_ID, challengeId);
        values.put(ReminderEntry.COL_SCHEDULED_AT, scheduledAt);
        values.put(ReminderEntry.COL_REMINDER_TIME, time);
        values.put(ReminderEntry.COL_REMINDER_FLAGS, flags);

        return values;
    }

    public ContentValues toContentValuesOnUpdate() {
        ContentValues values = new ContentValues(1);
        values.put(ReminderEntry.COL_SCHEDULED_AT, scheduledAt);
        return values;
    }

    public ScheduledReminder fillFromCursor(Cursor cursor) {
        id = CursorReaderUtils.getId(cursor, ReminderEntry.TABLE_NAME);

        challengeId = CursorReaderUtils.getLong(cursor, ReminderEntry.COL_CHALLENGE_ID);
        scheduledAt = CursorReaderUtils.getLong(cursor, ReminderEntry.COL_SCHEDULED_AT);
        time = CursorReaderUtils.getNullableInt(cursor, ReminderEntry.COL_REMINDER_TIME);
        flags = CursorReaderUtils.getInt(cursor, ReminderEntry.COL_REMINDER_FLAGS);

        challengeTitle = CursorReaderUtils.getString(cursor, ChallengeEntry.COL_TITLE);
        totalGoal = CursorReaderUtils.getInt(cursor, SyntheticEntry.PROGRESS_TOTAL_GOAL);
        totalProgress = CursorReaderUtils.getInt(cursor, SyntheticEntry.PROGRESS_TOTAL_PROGRESS);

        return this;
    }

}
