package com.actinarium.persistence;

import android.app.Application;
import com.actinarium.persistence.service.ReminderService;
import com.facebook.stetho.Stetho;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class DebugApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize stetho
        Stetho.initializeWithDefaults(this);

        // initialize notifications
        ReminderService.fireAlarm(this);
    }
}
